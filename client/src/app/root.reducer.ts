import { LayoutState, layoutReducer } from "./core/reducers";
import { ActionReducerMap } from "@ngrx/store";
import { RouterReducerState, routerReducer } from "@ngrx/router-store";

export interface State {
    layout: LayoutState;
    router: '';
}

export const rootReducer: ActionReducerMap<State> = {
    layout: layoutReducer,
    router: () => ''
}
