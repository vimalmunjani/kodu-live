import { Component } from '@angular/core';

@Component({
  selector: 'kodu-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'kodu-live';
}
