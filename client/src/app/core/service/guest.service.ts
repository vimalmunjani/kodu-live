import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GuestService {

  constructor(private http: HttpClient) { }

  public sendQuery(guestQuery: {
    message: string,
    email: string,
    name: string
  }): Observable<{ status: string }> {
    return this.http.post<{ status: string }>(Constants.GUEST_QUERY_URL, {
      guestQuery
    });
  }

}
