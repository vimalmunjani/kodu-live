import { Injectable } from '@angular/core';


@Injectable()
export class JwtService {

    getToken(): string {
        return window.localStorage.authToken;
    }

    saveToken(token: string): void {
        window.localStorage.authToken = token;
    }

    destroyToken(): void {
        window.localStorage.removeItem('authToken');
    }

}
