import { Injectable } from '@angular/core';
import {
    MatSnackBar,
    MatSnackBarHorizontalPosition,
    MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { SnackBarComponent } from '../components/snack-bar/snack-bar.component';
import { SNACK_TYPE } from '../models/snack-type.enum';



@Injectable({
    providedIn: 'root'
})
export class SnackService {

    private _horizontalPosition: MatSnackBarHorizontalPosition = 'end';
    private _verticalPosition: MatSnackBarVerticalPosition = 'bottom';


    constructor(private _snackBar: MatSnackBar) { }

    public openSnackBar(message: string): void {
        this._snackBar.open(message, '', {
            duration: 1500,
            horizontalPosition: this._horizontalPosition,
            verticalPosition: this._verticalPosition
        });
    }

    public success(message: string): void {
        this._snackBar.openFromComponent(SnackBarComponent, {
            duration: 1500,
            panelClass: ['snack-success'],
            data: {
                message,
                type: SNACK_TYPE.SUCCESS,
                prefix: 'done'
            },
            horizontalPosition: this._horizontalPosition,
            verticalPosition: this._verticalPosition
        });

    }

    public error(message: string): void {
        this._snackBar.openFromComponent(SnackBarComponent, {
            duration: 1500,
            panelClass: ['snack-error'],
            data: {
                message,
                type: SNACK_TYPE.ERROR,
                prefix: 'error'
            },
            horizontalPosition: this._horizontalPosition,
            verticalPosition: this._verticalPosition
        });

    }

    public dismiss(value: string): void {
        this._snackBar.dismiss();
    }

}
