import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { IFaqPageState } from '../reducers';
import { IFaq } from '../../user/models/faq.model';
import { getFaqs } from '../selectors/faq-page.selector';
import { FaqPageActions } from '../actions';

export class FaqPageDataSource implements DataSource<IFaq> {

    private _faqs: Array<IFaq>;
    constructor(private _store$: Store<IFaqPageState>) {
        this._faqs = [];
    }


    connect(collectionViewer: CollectionViewer): Observable<IFaq[]> {
        return this._store$.select(getFaqs).pipe(
            tap((faqs: Array<IFaq>) => this._faqs = faqs)
        );
    }

    disconnect(collectionViewer: CollectionViewer): void {
    }

    loadFaqs(sortDirection?: string, pageIndex?: number, pageSize?: number): void {
        pageIndex = pageIndex || 0;
        pageSize = pageSize || 10;
        this._store$.dispatch(FaqPageActions.fetchFaqs({ pageIndex, pageSize }));
    }

    public get data(): Array<IFaq> {
        return this._faqs;
    }

}
