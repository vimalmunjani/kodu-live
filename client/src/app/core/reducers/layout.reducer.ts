import { createReducer, on } from '@ngrx/store';
import { LayoutActions } from '../actions';

export interface LayoutState {
    isModalOpen: boolean;
}

const initialLayoutState: LayoutState = {
    isModalOpen: false
};

export const layoutReducer = createReducer(
    initialLayoutState,
    on(LayoutActions.openModal, (state) => ({ ...state, isModalOpen: true })),
    on(LayoutActions.closeModal, (state) => ({ ...state, isModalOpen: false })),
);
