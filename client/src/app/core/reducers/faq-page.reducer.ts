import { createReducer, on } from '@ngrx/store';
import { FaqPageActions } from '../actions';
import { IError } from '../../core/models/error.model';
import { IFaq } from '../../user/models/faq.model';


export interface IFaqPageState {
    error: IError | null;
    pending: boolean;
    faqs: Array<IFaq> | [];
    faqPagination: {
        totalCount: number,
        pageIndex: number,
        pageSize: number
    };
    faq: IFaq | null;
}

const initialUserState: IFaqPageState = {
    error: null,
    pending: false,
    faqs: [],
    faqPagination: {
        totalCount: 0,
        pageIndex: 1,
        pageSize: 10
    },
    faq: null,
};

export const faqPageReducer = createReducer(
    initialUserState,
    on(FaqPageActions.fetchFaqs, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(FaqPageActions.fetchFaqsSuccess, (state, { paginatedFaqs }) => ({
        ...state,
        error: null,
        pending: false,
        faqs: paginatedFaqs.data,
        faqPaginationPagination: {
            totalCount: paginatedFaqs.totalCount,
            pageIndex: paginatedFaqs.pageIndex,
            pageSize: paginatedFaqs.pageSize
        }
    })),
    on(FaqPageActions.fetchFaqsFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false,
        meetings: []
    }))
);

export const getError = (state: IFaqPageState) => state.error;
export const getPending = (state: IFaqPageState) => state.pending;

