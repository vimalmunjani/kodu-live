export enum SNACK_TYPE {
    SUCCESS,
    ERROR,
    WARNING
}
