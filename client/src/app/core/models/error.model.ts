export interface IError {
    status: string | number;
    error: string;
}
