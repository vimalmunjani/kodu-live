import { createSelector } from '@ngrx/store';
import { IFaqPageState } from '../reducers';

export const selectCore = (state: any) => state.core;
export const selectFaqs = (state: IFaqPageState) => state.faqs;
export const selectFaqPagination = (state: IFaqPageState) => state.faqPagination;
export const selectFaq = (state: IFaqPageState) => state.faq;


export const getFaqs = createSelector(selectCore, selectFaqs);
export const getFaq = createSelector(selectCore, selectFaq);
export const getFaqPagination = createSelector(selectCore, selectFaqPagination);
