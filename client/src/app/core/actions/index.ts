import * as LayoutActions from './layout.actions';
import * as FaqPageActions from './faq-page.actions';

export { LayoutActions, FaqPageActions };
