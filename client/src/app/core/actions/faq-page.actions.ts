import { createAction, props } from '@ngrx/store';
import { IFaq } from '../../user/models/faq.model';
import { IError } from '../models/error.model';

export const fetchFaqs = createAction(
    '[Support Page] Fetch Faqs',
    props<{ pageIndex: number, pageSize: number }>()
);

export const fetchFaqsSuccess = createAction(
    '[Support Page] Fetch Faqs Success',
    props<{
        paginatedFaqs: {
            data: Array<IFaq>, totalCount: number,
            pageIndex: number,
            pageSize: number
        }
    }>()
);

export const fetchFaqsFailure = createAction(
    '[Support Page] Fetch Faqs Failure',
    props<{ error: IError }>()
);