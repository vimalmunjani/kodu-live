import { createAction, props } from '@ngrx/store';

export const openModal = createAction('[Layout] Open Modal');
export const closeModal = createAction('[Layout] Close Modal');
