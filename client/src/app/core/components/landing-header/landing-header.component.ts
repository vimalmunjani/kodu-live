import { Component, OnInit, Input } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { selectLoggedIn } from '../../../auth/reducers';

@Component({
  selector: 'kodu-landing-header',
  templateUrl: './landing-header.component.html',
  styleUrls: ['./landing-header.component.scss']
})
export class LandingHeaderComponent implements OnInit {

  @Input()
  public currentSection: string;

  public isHandset$: Observable<boolean>;
  public loggedIn$: Observable<boolean>;
  public navs: Array<any> = [{
    title: 'Home',
    link: 'home'
  }, {
    title: 'Features',
    link: 'features'
  }, {
    title: 'Plans & Pricing',
    link: 'pricing'
  }, {
    title: 'Contact',
    link: 'contact'
  }];

  constructor(private breakpointObserver: BreakpointObserver, private _store$: Store<any>) {
    this.isHandset$ = this.breakpointObserver.observe(Breakpoints.Handset)
      .pipe(
        map(result => result.matches)
      );
    // this.loggedIn$ = of(false);
    this.loggedIn$ = this._store$.pipe(select(selectLoggedIn));
    this.currentSection = 'home';
  }

  ngOnInit(): void {
    const logo_nav = document.getElementById('logo_nav') as HTMLVideoElement;
    if (logo_nav) {
      logo_nav.oncanplaythrough = () => {
        logo_nav.muted = true;
        logo_nav.play();
      };
    }
  }

  public scrollTo(section: string): void {
    if (document) {
      document.querySelector('#' + section)?.scrollIntoView();
    }
  }


}
