import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormGroupDirective } from '@angular/forms';
import { GuestService } from '../../service/guest.service';
import { SnackService } from '../../service/snack.service';
import { EMAIL_PATTERN } from 'src/app/auth/utils/constants';

@Component({
  selector: 'kodu-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  public contactForm: FormGroup;
  public submitted: boolean;
  public loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private _snackBar: SnackService,
    private _guestService: GuestService) {
    this.submitted = false;
    this.loading = false;
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
      message: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  public get controls(): { [key: string]: AbstractControl } {
    return this.contactForm.controls;
  }

  public onSubmit(formDirective: FormGroupDirective): void {
    if (this.contactForm.valid) {
      this.loading = true;
      const query = this.contactForm.value;
      this._guestService.sendQuery(query).subscribe(() => {
        this._snackBar.success('Query submmited. Thanks.');
        this.loading = false;
        this.contactForm.reset();
        formDirective?.resetForm();
      }, () => {
        this.loading = false;
      });
    }

  }

}
