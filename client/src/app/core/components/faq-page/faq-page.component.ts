import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FaqPageDataSource } from '../../service/faq-page.datasource';
import { IFaq } from 'src/app/user/models/faq.model';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { IFaqPageState } from '../../reducers';
import { getFaqPagination } from '../../selectors/faq-page.selector';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'kodu-faq-page',
  templateUrl: './faq-page.component.html',
  styleUrls: ['./faq-page.component.scss']
})
export class FaqPageComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['question'];
  dataSource: FaqPageDataSource;
  faqs: Array<IFaq> = [];
  selection = new SelectionModel<IFaq>(true, []);
  isScheduled: boolean;
  startMeet: boolean;
  loading: boolean;
  expandedFaq: IFaq;
  totalFaqs: number;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  constructor(
    private _store$: Store<IFaqPageState>, private route: ActivatedRoute) {
    this.dataSource = new FaqPageDataSource(_store$);
    this.isScheduled = false;
    this.startMeet = false;
    this.loading = false;
    this.expandedFaq = {
      id: '-1',
      question: '',
      answer: '',
      faqCategory: {
        name: '',
        disabled: false
      }
    };
    this.totalFaqs = 0;
  }


  ngOnInit(): void {
    this._store$.select(getFaqPagination).pipe().subscribe((pagination) => {
      if (pagination) {
        this.loading = false;
        this.paginator.pageIndex = pagination.pageIndex;
        this.paginator.pageSize = pagination.pageSize;
        this.totalFaqs = pagination.totalCount;
      }
    });
    this.dataSource.loadFaqs();
  }


  ngAfterViewInit(): void {

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadPage())
      )
      .subscribe();
  }

  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  public checkboxLabel(row?: IFaq): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }


  public loadPage(): void {
    this.dataSource.loadFaqs(
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

}
