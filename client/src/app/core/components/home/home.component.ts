import { Component, OnInit, HostListener, ElementRef, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { selectLoggedIn } from 'src/app/auth/reducers';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'kodu-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public isCompactLayout: boolean;
  public currentSection: string;
  public loggedIn$: Observable<boolean>;
  @ViewChild('wrapper') wrapper: ElementRef<HTMLElement> | undefined;

  constructor(private _store$: Store<any>) {
    this.isCompactLayout = false;
    this.currentSection = 'home';
    this.loggedIn$ = of(false);
  }

  @HostListener('document:scroll', ['$event'])
  private onScroll($event: Event): void {
    //   this.isCompactLayout = !!this.wrapper && ((window.scrollY + window.innerHeight) > this.wrapper?.nativeElement?.offsetHeight);
  }

  ngOnInit(): void {
    const logo_home = document.getElementById('logo_home') as HTMLVideoElement;
    if (logo_home) {
      logo_home.oncanplaythrough = () => {
        logo_home.muted = true;
        logo_home.play();
      };
    }
    this.loggedIn$ = this._store$.pipe(select(selectLoggedIn));
  }


  onSectionChange(sectionId: string): void {
    this.currentSection = sectionId;
  }

  public scrollTo(section: string): void {
    if (document) {
      document.querySelector('#' + section)?.scrollIntoView();
    }
  }



}
