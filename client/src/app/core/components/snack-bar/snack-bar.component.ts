import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { SNACK_TYPE } from '../../models/snack-type.enum';

@Component({
  selector: 'kodu-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss']
})
export class SnackBarComponent implements OnInit {

  public message: string;
  public prefix: string;
  public type: SNACK_TYPE;

  constructor(
    public snackBarRef: MatSnackBarRef<SnackBarComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any
  ) {
    this.message = '';
    this.type = SNACK_TYPE.SUCCESS;
    this.prefix = 'done';
  }

  ngOnInit(): void {
    this.message = this.data.message;
    this.type = this.data.type;
    this.prefix = this.data.prefix;
  }

}
