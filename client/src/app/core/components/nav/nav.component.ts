import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'kodu-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public hasSolidBackground: boolean;
  public navs: Array<string>;

  constructor() {
    this.hasSolidBackground = false;
    this.navs = ['Home', 'Features', 'Pricing', 'Clients', 'Contact'];
  }

  @HostListener('document:scroll', ['$event'])
  private onScroll($event: Event): void {
    const element = $event?.srcElement as any;
    const scrollTop = element?.scrollingElement?.scrollTop;
    this.hasSolidBackground = scrollTop > 65;
  }

  ngOnInit(): void {
  }

}
