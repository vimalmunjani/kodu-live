import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'kodu-office-contact',
  templateUrl: './office-contact.component.html',
  styleUrls: ['./office-contact.component.scss']
})
export class OfficeContactComponent implements OnInit {
  // labelOrigin = new google.maps.Point(10, 10);
  labelOptions = {
    color: '#F44336',
    fontFamily: '',
    fontWeight: 'bold',
    fontSize: '14px',
    zIndex: 1,
    text: 'Ekodus Technologies Pvt Ltd',

  };
  locations = [{
    title: 'Ekodus Technologies Pvt. Ltd.',
    subTitle: 'Guwahati, India',
    latitude: 26.1442905,
    longitude: 91.784465,
    address: 'Aashi Grande 1(A), 1(C) & 1(D),Near Ganesh Mandir, Kahilipara Road,Ganeshguri, Guwahati - 781006,Assam, India',
    labelOptions: {
      ...this.labelOptions,
      text: 'Ekodus Technologies Pvt Ltd'
    }
  },
  {
    title: 'Ekodus Digital',
    subTitle: 'Noida UP, India',
    latitude: 22.1662208,
    longitude: 71.4856171,
    address: `Suite 6, 2nd Floor,D- 77, Sector 63 Noida,UP - 201301, India`,
    labelOptions: {
      ...this.labelOptions,
      text: 'Ekodus Digital'
    }
  },
  {
    title: 'Ekodus Inc.',
    subTitle: 'NJ 08512, United States',
    latitude: 40.344657,
    longitude: -74.500013,
    address: '2525 US-130, Cranbury Township, NJ 08512, United States',
    labelOptions: {
      ...this.labelOptions,
      text: 'Ekodus Inc.'
    }
  },
  {
    title: 'Ekodus Technologies Canada Inc.',
    subTitle: 'Toronto, Canada',
    latitude: 43.6399228,
    longitude: -79.4244245,
    address: 'Liberty Village 60 Atlantic Avenue Suite 200 Toronto M6K 1X9',
    labelOptions: {
      ...this.labelOptions,
      text: 'Ekodus Inc.'
    }
  }];


  constructor() { }

  ngOnInit(): void {
  }


  locationChange(event: any, location: any): void {
    location.latitude = event.coords.lat;
    location.longitude = event.coords.lng;
  }

}
