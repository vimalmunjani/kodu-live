import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';

import { MaterialModule } from '../material';
import {
  HomeComponent,
  PageNotFoundComponent,
  ContactComponent,
  FeaturesComponent,
  LandingHeaderComponent,
  PricingComponent,
  FooterComponent,
  PrivacyPolicyComponent,
  AboutUsComponent,
  FaqPageComponent,
  OfficeContactComponent
} from './components';
import { ScrollSpyDirective } from './directives/scroll-spy.directive';
import { RouterModule } from '@angular/router';
import { JwtService } from './service/jwt.service';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { faqPageReducer } from './reducers';
import { FaqPageEffects } from './effects/faq-page.effects';

export const COMPONENTS = [
  HomeComponent,
  PageNotFoundComponent,
  SnackBarComponent,
  ContactComponent,
  FeaturesComponent,
  LandingHeaderComponent,
  ScrollSpyDirective,
  PricingComponent,
  FooterComponent,
  PrivacyPolicyComponent,
  AboutUsComponent,
  FaqPageComponent,
  OfficeContactComponent
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule,
    StoreModule.forFeature('core', faqPageReducer),
    EffectsModule.forFeature([FaqPageEffects]),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCgb4VgW_IXx-r6CSFd5rgVAZ7cTiMW-AM'
    })
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [JwtService],
  entryComponents: [SnackBarComponent]
})
export class CoreModule { }
