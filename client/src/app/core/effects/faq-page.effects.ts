import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { FaqPageActions } from '../actions';
import { IFaqPageState } from '../reducers';
import { FaqService } from '../../user/services/faq.service';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { IFaq } from '../../user/models/faq.model';

@Injectable()
export class FaqPageEffects {

    constructor(
        private actions$: Actions,
        private faqService: FaqService,
        private _store$: Store<IFaqPageState>
    ) { }

    fetch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FaqPageActions.fetchFaqs),
            switchMap((pagination) => {
                return this.faqService.fetch(pagination)
                    .pipe(
                        map((paginatedFaqs: {
                            data: Array<IFaq>,
                            totalCount: number,
                            pageIndex: number,
                            pageSize: number
                        }) => FaqPageActions.fetchFaqsSuccess({ paginatedFaqs })),
                        catchError((error) => of(FaqPageActions.fetchFaqsFailure({ error })))
                    );
            })
        )
    );


}
