import { createAction, props } from '@ngrx/store';
import { IMeeting } from '../models/meeting.model';
import { IError } from '../../core/models/error.model';


export const fetchMeetings = createAction(
    '[User] Fetch Meetings',
    props<{ pageIndex: number, pageSize: number }>()
);

export const fetchMeetingsSuccess = createAction(
    '[User] Fetch Meetings Success',
    props<{
        paginatedMeetings: {
            data: Array<IMeeting>, totalCount: number,
            pageIndex: number,
            pageSize: number
        }
    }>()
);

export const fetchMeetingsFailure = createAction(
    '[User] Fetch Meetings Failure',
    props<{ error: IError }>()
);

export const startMeeting = createAction(
    '[User] Start Meeting',
    props<{ meetingMetaData: { title: string, meetingDate: Date, startTime: string, endTime: string, guests: Array<string> } }>()
);

export const startMeetingSuccess = createAction(
    '[User] Start Meeting Success',
    props<{ meeting: IMeeting }>()
);

export const startMeetingFailure = createAction(
    '[User] Start Meeting Failure',
    props<{ error: IError }>()
);

export const joinMeeting = createAction(
    '[User] Join Meeting',
    props<{ meetingCode: string }>()
);

export const joinMeetingSuccess = createAction(
    '[User] Join Meeting Success',
    props<{ meeting: IMeeting }>()
);

export const joinMeetingFailure = createAction(
    '[User] Join Meeting Failure',
    props<{ error: IError }>()
);

export const cancelMeeting = createAction(
    '[User] Cancel Meeting',
    props<{ meeting: IMeeting }>()
);

export const cancelMeetingSuccess = createAction(
    '[User] Cancel Meeting Success',
    props<{ meeting: IMeeting }>()
);

export const cancelMeetingFailure = createAction(
    '[User] Cancel Meeting Failure',
    props<{ error: IError }>()
);

export const updateMeeting = createAction(
    '[User] Update Meeting',
    props<{ meetingMetaData: { title: string, meetingDate: Date, startTime: string, endTime: string, guests: Array<string> } }>()
);

export const updateMeetingSuccess = createAction(
    '[User] Update Meeting Success',
    props<{ meeting: IMeeting }>()
);

export const updateMeetingFailure = createAction(
    '[User] Update Meeting Failure',
    props<{ error: IError }>()
);


