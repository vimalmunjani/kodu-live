import { createAction, props } from '@ngrx/store';
import { IFaq } from '../models/faq.model';
import { IError } from '../../core/models/error.model';


export const fetchFaqs = createAction(
    '[Support] Fetch Faqs',
    props<{ pageIndex: number, pageSize: number }>()
);

export const fetchFaqsSuccess = createAction(
    '[Support] Fetch Faqs Success',
    props<{
        paginatedFaqs: {
            data: Array<IFaq>, totalCount: number,
            pageIndex: number,
            pageSize: number
        }
    }>()
);

export const fetchFaqsFailure = createAction(
    '[Support] Fetch Faqs Failure',
    props<{ error: IError }>()
);

export const createFaq = createAction(
    '[Support] Create Faq',
    props<{ faqMetaData: {} }>()
);

export const createFaqSuccess = createAction(
    '[Support] Create Faq Success',
    props<{ faq: IFaq }>()
);

export const createFaqFailure = createAction(
    '[Support] Create Faq Failure',
    props<{ error: IError }>()
);


export const deleteFaq = createAction(
    '[Support] Delete Faq',
    props<{ faq: IFaq }>()
);

export const cancelFaqSuccess = createAction(
    '[Support] Cancel Faq Success',
    props<{ faq: IFaq }>()
);

export const cancelFaqFailure = createAction(
    '[Support] Cancel Faq Failure',
    props<{ error: IError }>()
);

export const updateFaq = createAction(
    '[Support] Update Faq',
    props<{ faq: IFaq }>()
);

export const updateFaqSuccess = createAction(
    '[Support] Update Faq Success',
    props<{ meeting: IFaq }>()
);

export const updateFaqFailure = createAction(
    '[Support] Update Faq Failure',
    props<{ error: IError }>()
);


