import { createAction, props } from '@ngrx/store';
import { IProfile } from '../models/profile.model';
import { IError } from '../../core/models/error.model';

export const fetchProfile = createAction(
    '[User] Fetch Profile',
);

export const fetchProfileSuccess = createAction(
    '[User] Fetch Profile Success',
    props<{ profile: IProfile }>()
);

export const fetchProfileFailure = createAction(
    '[User] Fetch Profile Failure',
    props<{ error: IError }>()
);

export const updateProfile = createAction(
    '[User] Update Profile',
    props<{ profile: IProfile }>()
);

export const updateProfileSuccess = createAction(
    '[User] Update Profile Success',
    props<{ profile: IProfile }>()
);

export const updateProfileFailure = createAction(
    '[User] Update Profile Failure',
    props<{ error: IError }>()
);
export const updateAvatar = createAction(
    '[User] Update Avatar',
    props<{ avatar: File }>()
);

export const updateAvatarSuccess = createAction(
    '[User] Update Avatar Success',
    props<{ photoUrl: string }>()
);

export const updateAvatarFailure = createAction(
    '[User] Update Avatar Failure',
    props<{ error: IError }>()
);



