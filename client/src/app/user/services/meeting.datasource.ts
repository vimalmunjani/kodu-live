import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { IMeeting } from '../models/meeting.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { IUserState } from '../reducers';
import { Store } from '@ngrx/store';
import { getMeetings } from '../selectors/meeting.selector';
import { MeetingActions } from '../actions';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { tap } from 'rxjs/operators';

export class MeetingDataSource implements DataSource<IMeeting> {

    private _meetings: Array<IMeeting>;
    constructor(private _store$: Store<IUserState>) {
        this._meetings = [];
    }


    connect(collectionViewer: CollectionViewer): Observable<IMeeting[]> {
        return this._store$.select(getMeetings).pipe(
            tap((meetings: Array<IMeeting>) => this._meetings = meetings)
        );
    }

    disconnect(collectionViewer: CollectionViewer): void {
    }

    loadMeetings(sortDirection?: string, pageIndex?: number, pageSize?: number): void {
        pageIndex = pageIndex || 0;
        pageSize = pageSize || 10;
        this._store$.dispatch(MeetingActions.fetchMeetings({ pageIndex, pageSize }));
    }

    public get data(): Array<IMeeting> {
        return this._meetings;
    }

}
