import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Constants } from '../utils';
import { Observable } from 'rxjs';
import { IProfile } from '../models/profile.model';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    constructor(private http: HttpClient) { }

    public fetch(): Observable<IProfile> {
        return this.http.get<IProfile>(Constants.PROFILE_URL);
    }

    public update(profile: IProfile): Observable<IProfile> {
        return this.http.put<IProfile>(Constants.PROFILE_URL, { profile });
    }

    public updateAvatar(avatar: File): Observable<string> {
        const postData = new FormData();
        const params = new HttpParams();

        const options = {
            params,
            reportProgress: true,
        };

        postData.append('image', avatar, 'avatar');
        return this.http.post<string>(Constants.AVATAR_URL, postData, options);
    }


}
