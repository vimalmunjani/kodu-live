import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils';
import { Observable } from 'rxjs';
import { IFaq } from '../models/faq.model';

@Injectable({
    providedIn: 'root'
})
export class FaqService {

    constructor(private http: HttpClient) { }

    public fetch(pagination: { pageIndex: number, pageSize: number }): Observable<{
        data: Array<IFaq>, totalCount: number,
        pageIndex: number,
        pageSize: number
    }> {
        const { pageIndex, pageSize } = pagination;
        return this.http.get<{
            data: Array<IFaq>, totalCount: number,
            pageIndex: number,
            pageSize: number
        }>(`${Constants.FETCH_FAQS_URL}?pageIndex=${pageIndex}&pageSize=${pageSize}`);
    }


    public update(meeting: IFaq): Observable<IFaq> {
        return this.http.put<IFaq>('', meeting);
    }

    public cancel(meeting: IFaq): Observable<IFaq> {
        return this.http.put<IFaq>('', meeting);
    }


}
