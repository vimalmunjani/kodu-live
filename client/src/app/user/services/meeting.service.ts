import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils';
import { Observable } from 'rxjs';
import { IMeeting } from '../models/meeting.model';

@Injectable({
    providedIn: 'root'
})
export class MeetingService {

    constructor(private http: HttpClient) { }

    public fetch(pagination: { pageIndex: number, pageSize: number }): Observable<{
        data: Array<IMeeting>, totalCount: number,
        pageIndex: number,
        pageSize: number
    }> {
        const { pageIndex, pageSize } = pagination;
        return this.http.get<{
            data: Array<IMeeting>, totalCount: number,
            pageIndex: number,
            pageSize: number
        }>(`${Constants.FETCH_MEETING_URL}?pageIndex=${pageIndex}&pageSize=${pageSize}`);
    }

    public start(meetingMetaData: {
        title: string,
        meetingDate: Date,
        startTime: string,
        endTime: string,
        guests: Array<string>
    }): Observable<IMeeting> {
        return this.http.post<IMeeting>(Constants.START_MEETING_URL, {
            ...meetingMetaData,
            startTime: meetingMetaData.startTime.valueOf(),
            endTime: meetingMetaData.endTime.valueOf()
        });
    }
    public join(meetingCode: string): Observable<IMeeting> {
        return this.http.post<IMeeting>(Constants.JOIN_MEETING_URL, {
            meetingCode
        });
    }

    public update(meetingMetaData: {
        title: string,
        meetingDate: Date,
        startTime: string,
        endTime: string,
        guests: Array<string>
    }): Observable<IMeeting> {
        return this.http.put<IMeeting>(Constants.UPDATE_MEETING_URL, {
            ...meetingMetaData,
            startTime: meetingMetaData.startTime.valueOf(),
            endTime: meetingMetaData.endTime.valueOf()
        });
    }

    public cancel(meeting: IMeeting): Observable<IMeeting> {
        return this.http.put<IMeeting>(Constants.CANCEL_MEETING_URL, meeting);
    }


}
