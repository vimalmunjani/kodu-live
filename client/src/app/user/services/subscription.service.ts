import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UPDATE_PLAN_URL, CANCEL_PLAN_URL, REACTIVATE_PLAN_URL, RESET_PLAN_URL } from '../utils/constants';

@Injectable({
    providedIn: 'root'
})
export class SubscriptionService {

    constructor(private http: HttpClient) { }

    public updatePlan(subscriptionId: string) {
        return this.http.get(`${UPDATE_PLAN_URL}/${subscriptionId}`);
    }

    public cancelPlan(subscriptionId: string) {
        return this.http.get(`${CANCEL_PLAN_URL}/${subscriptionId}`);
    }

    public reactivatePlan(subscriptionId: string) {
        return this.http.get(`${REACTIVATE_PLAN_URL}/${subscriptionId}`);
    }

    public resetPlan(subscriptionId: string) {
        return this.http.get(`${RESET_PLAN_URL}/${subscriptionId}`);
    }

}
