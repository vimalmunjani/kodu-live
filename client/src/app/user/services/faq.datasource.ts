import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { IUserState } from '../reducers';
import { Store } from '@ngrx/store';
import { getFaqs } from '../selectors/faq.selector';
import { FaqActions } from '../actions';
import { tap } from 'rxjs/operators';
import { IFaq } from '../models/faq.model';

export class FaqDataSource implements DataSource<IFaq> {

    private _faqs: Array<IFaq>;
    constructor(private _store$: Store<IUserState>) {
        this._faqs = [];
    }


    connect(collectionViewer: CollectionViewer): Observable<IFaq[]> {
        return this._store$.select(getFaqs).pipe(
            tap((faqs: Array<IFaq>) => this._faqs = faqs)
        );
    }

    disconnect(collectionViewer: CollectionViewer): void {
    }

    loadFaqs(sortDirection?: string, pageIndex?: number, pageSize?: number): void {
        pageIndex = pageIndex || 0;
        pageSize = pageSize || 10;
        this._store$.dispatch(FaqActions.fetchFaqs({ pageIndex, pageSize }));
    }

    public get data(): Array<IFaq> {
        return this._faqs;
    }

}
