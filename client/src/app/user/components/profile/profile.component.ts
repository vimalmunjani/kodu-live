import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PersonalInfoComponent } from '../personal-info/personal-info.component';
import { Store } from '@ngrx/store';
import { ProfileActions } from '../../actions';
import { IUserState } from '../../reducers';
import { IProfile } from '../../models/profile.model';
import { getProfile } from '../../selectors/profile.selector';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { mimeType } from '../../../core/mime-type.validator';
import { SubscriptionService } from '../../services/subscription.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { take } from 'rxjs/operators';

@Component({
  selector: 'kodu-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public profile: IProfile | null;


  public profileImageUrl: string | ArrayBuffer;
  public coverImageUrl: string | ArrayBuffer;
  private dialogRef!: MatDialogRef<PersonalInfoComponent | ChangePasswordComponent>;
  public imageForm: FormGroup;
  public isSubscriptionValidToday: boolean;

  constructor(
    public dialog: MatDialog,
    private _store$: Store<IUserState>,
    protected subscriptionService: SubscriptionService,
    protected router: Router,
    private route: ActivatedRoute,
    protected datePipe: DatePipe) {
    this.profile = null;
    this.isSubscriptionValidToday = false;
    this.profileImageUrl = 'assets/images/user.svg';
    this.coverImageUrl = '';
    this.imageForm = new FormGroup({
      image: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType]
      })
    });
  }

  ngOnInit(): void {
    this._store$.select(getProfile).subscribe((profile: IProfile | null) => {
      this.profile = profile;
      if (this.profile && this.profile.email) {
        // tslint:disable-next-line:no-any
        this.route.queryParams.pipe(take(1)).subscribe((params: any) => {
          const password = params && params.password;
          const email = params && params.email;

          if (email && password) {
            this.router.navigate([], {
              queryParams: {
                password: null,
                email: null,
              },
              queryParamsHandling: 'merge'
            });
            this.changePassword();

          }
        });
      }

      if (profile && profile.subscription && profile.subscription.nextBillingDate) {
        if (new Date() > new Date(profile.subscription.nextBillingDate)) {
          this.isSubscriptionValidToday = false;
          this.subscriptionService.resetPlan(profile.subscription.subscriptionID).subscribe(() => {
            this.router.navigate(['user', 'profile']);
          });
        } else {
          this.isSubscriptionValidToday = true;
        }
      }
      this.profileImageUrl = profile && profile.photoUrl || 'assets/images/user.svg';
    });

  }

  public onProfileImageChange(event: Event): void {
    const reader = new FileReader();
    const target = event.target as HTMLInputElement;
    if (target && target.files && target.files[0]) {
      this.imageForm.patchValue({ image: target.files[0] });
      this.imageForm?.get('image')?.updateValueAndValidity();
      reader.readAsDataURL(target.files[0]);
      reader.onload = (e) => {
        this.profileImageUrl = reader.result as string;
        this.updateAvatar();
      };
    }
  }

  public onCoverImageChange(event: Event): void {
    const reader = new FileReader();
    const target = event.target as HTMLInputElement;
    if (target && target.files && target.files[0]) {
      reader.readAsDataURL(target.files[0]);
      reader.onload = (e) => {
        this.coverImageUrl = reader.result as string;
      };
    }
  }

  public updateAvatar(): void {
    this._store$.dispatch(
      ProfileActions.updateAvatar({
        avatar: this.imageForm.get('image')?.value
      })
    );

  }

  public edit(): void {
    this.dialogRef = this.dialog.open(PersonalInfoComponent, {
      width: '350px',
      height: 'auto',
      disableClose: true,
      data: this.profile,
    });
  }

  public changePassword(): void {
    this.dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '350px',
      height: 'auto',
      disableClose: true,
      data: this.profile,
    });
  }

  cancelSubscription(subscriptionID: string): void {
    this.subscriptionService.cancelPlan(subscriptionID).subscribe(() => {
      this.router.navigate(['user', 'profile']);
    });
  }

  reactivateSubscription(subscriptionID: string): void {
    this.router.navigate(['subscription']);
  }

}
