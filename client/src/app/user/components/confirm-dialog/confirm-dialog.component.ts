import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';

@Component({
  selector: 'kodu-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {


  public message: string;
  public loading: boolean;
  public confirm$: Subject<boolean>;
  public subject: string | undefined;
  constructor(
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { message: string },
    private _snackBar: MatSnackBar) {
    this.message = 'Cancelled meeting cannot be recoverd. Are you sure ?';
    this.subject = '';
    this.loading = false;
    this.confirm$ = new Subject();
  }

  ngOnInit(): void {
    this.message = this.data.message ? this.data.message : 'Cancelled meeting cannot be recoverd. Are you sure ?';
  }



  public onSubmit(): void {
    this.loading = true;
    this.confirm$.next(true);
    setTimeout(() => { this.loading = false; }, 2000);
  }

  public onCancel(): void {
    this.dialogRef.close();
  }
}
