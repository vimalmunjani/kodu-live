import { Component, OnInit, Inject } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { IUserState, getPending } from '../../reducers';
import { FaqActions } from '../../actions';
import { IFaq } from '../../models/faq.model';

@Component({
  selector: 'kodu-faq-form',
  templateUrl: './faq-form.component.html',
  styleUrls: ['./faq-form.component.scss']
})
export class FaqFormComponent implements OnInit {

  public faqForm: FormGroup;
  public submitted: boolean;
  public loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<FaqFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IFaq,
    private _store$: Store<IUserState>,
    private _snackBar: MatSnackBar) {
    this.submitted = false;
    this.loading = false;
    this.faqForm = this.formBuilder.group({
      question: ['', Validators.required],
      answer: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this._store$.select(getPending)
      .pipe()
      .subscribe((loading: boolean) => this.loading = !!loading);
    if (this.data) {
      this.faqForm = this.formBuilder.group({
        question: [this.data.question, Validators.required],
        answer: [this.data.answer, Validators.required],
      });
    }
  }

  public get controls(): { [key: string]: AbstractControl } {
    return this.faqForm.controls;
  }

  public onSubmit(): void {
    if (this.faqForm.valid) {
      this.loading = true;
      const faqMetaData = {
        ...this.faqForm?.value,
        faqCategory: 1
      };
      this._store$.dispatch(FaqActions.createFaq({ faqMetaData }));
    }

  }

  public onCancel(): void {
    this.dialogRef.close();
  }
}
