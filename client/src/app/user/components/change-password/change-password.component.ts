import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ICredentials } from '../../../auth/models';
import { SnackService } from '../../../core/service/snack.service';
import { AuthService } from '../../../auth/services';
import { confirmPasswordValidator } from '../../../auth/utils/confirm-password.validator';
import { EMAIL_PATTERN } from '../../../auth/utils/constants';
import { IProfile } from '../../models/profile.model';

@Component({
  selector: 'kodu-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  public updatePasswordForm: FormGroup;
  public loading: boolean;
  public hidePassword: boolean;
  public emailField: FormControl;
  public confirmPasswordField: FormControl;
  public passwordField: FormControl;

  constructor(private dialogRef: MatDialogRef<ChangePasswordComponent>,
    private fb: FormBuilder,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) private data: IProfile,
    private snackService: SnackService) {
    this.hidePassword = true;
    this.loading = false;
    this.emailField = new FormControl(this.data.email, [Validators.required, Validators.pattern(EMAIL_PATTERN)]);
    this.passwordField = new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(16),
    ]));
    this.confirmPasswordField = new FormControl('', Validators.required);
    this.updatePasswordForm = this.fb.group({
      email: this.emailField,
      password: this.passwordField,
      confirmPassword: this.confirmPasswordField
    }, {
      validator: confirmPasswordValidator('password', 'confirmPassword')
    });
  }

  ngOnInit(): void {
    // this.updatePasswordForm.patchValue({
    //   email: this.data.email
    // })
  }

  onSubmit(): void {
    if (this.updatePasswordForm.valid) {
      this.loading = true;
      this.authService.updatePassword(<ICredentials>{
        email: this.updatePasswordForm.value.email,
        password: this.updatePasswordForm.value.password
      }).subscribe((res) => {
        this.snackService.success('Password Updated Successfully');
        setTimeout(() => {
          this.loading = false
        }, 2000);
      }, (err) => {
        this.snackService.success('Error Updating Password');
        this.loading = false
      })
      this.onCancel();
    }
  }

  public get controls(): { [key: string]: AbstractControl } {
    return this.updatePasswordForm.controls;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
