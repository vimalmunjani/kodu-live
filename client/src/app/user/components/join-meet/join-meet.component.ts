import { Component, OnInit, Inject } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { IUserState, getPending } from '../../reducers';
import { IMeeting } from '../../models/meeting.model';
import { MeetingActions } from '../../actions';
import { getMeeting } from '../../selectors/meeting.selector';
import { take } from 'rxjs/operators';
import { getProfile } from '../../selectors/profile.selector';
import { IProfile } from '../../models/profile.model';

@Component({
  selector: 'kodu-join-meet',
  templateUrl: './join-meet.component.html',
  styleUrls: ['./join-meet.component.scss']
})
export class JoinMeetComponent implements OnInit {

  public joinMeetForm: FormGroup;
  public submitted: boolean;
  public loading: boolean;
  public joinMeet: boolean;
  public profile: IProfile | null;
  public displayName: string;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<JoinMeetComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IMeeting,
    private _store$: Store<IUserState>,
    private _snackBar: MatSnackBar) {
    this.submitted = false;
    this.loading = false;
    this.joinMeet = false;
    this.displayName = 'Kodu';
    this.profile = null;
    this.joinMeetForm = this.formBuilder.group({
      meetingCode: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this._store$.select(getProfile).subscribe((profile: IProfile | null) => {
      this.profile = profile;
      this.displayName = profile && profile.name && profile.name.split(' ')[0] || '';
    });
    this.joinMeetForm.patchValue({
      meetingCode: this.data?.meetingCode,
    });

    this._store$.select(getPending)
      .pipe()
      .subscribe((loading: boolean) => this.loading = !!loading);

    this._store$.select(getMeeting).pipe().subscribe((meeting: IMeeting | null) => {
      const isOrganizer = meeting?.organizer?.email === this.profile?.email;
      if (meeting && meeting.meetingCode && this.joinMeet && !isOrganizer) {
        this.joinMeet = false;
        window.open(`https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${meeting.token}#userInfo.displayName="${this.displayName}"`);
        this.dialogRef.close();
      }
      if (meeting && isOrganizer && meeting.meetingCode && this.joinMeet) {
        this.joinMeet = false;
        const meetingTitleParam = meeting.title.split(' ').join('-');
        window.open(`https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${meeting.token}#config.subject="${meetingTitleParam}"`);
        this.dialogRef.close();
      }
    });


  }

  public get controls(): { [key: string]: AbstractControl } {
    return this.joinMeetForm.controls;
  }

  public reset(): void {
    this.joinMeetForm.reset();
  }

  public onSubmit(): void {
    if (this.joinMeetForm.valid) {
      this.joinMeet = true;
      const meetingCode = this.joinMeetForm.value.meetingCode;
      this._store$.dispatch(MeetingActions.joinMeeting({ meetingCode }));
    }

  }

  public onCancel(): void {
    this.dialogRef.close();
  }
}
