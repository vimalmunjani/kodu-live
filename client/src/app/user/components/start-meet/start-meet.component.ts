import { Component, OnInit, Inject } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { IUserState, getPending } from '../../reducers';
import { MeetingActions } from '../../actions';
import { Subject } from 'rxjs';
import { duplicateGuestValidator } from '../../utils/duplicate-guest.validator';
import { dateRangeValidator } from '../../utils/date-range.validator';
import { EMAIL_PATTERN } from 'src/app/auth/utils/constants';
import { IProfile } from '../../models/profile.model';
import { getProfile } from '../../selectors/profile.selector';
import { IMeeting } from '../../models/meeting.model';
import * as moment from 'moment';

@Component({
  selector: 'kodu-start-meet',
  templateUrl: './start-meet.component.html',
  styleUrls: ['./start-meet.component.scss']
})
export class StartMeetComponent implements OnInit {

  public startMeetForm: FormGroup;
  public guestForm: FormGroup;
  public submitted: boolean;

  public isScheduled: Subject<boolean>;
  public loading: boolean;
  public isEditMode: boolean;
  public displayName: string;
  public profile: IProfile | null;
  public isDateRangeValidator: boolean;
  public guests: Array<{ email: string, bgColor: string, color: string }>;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<StartMeetComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { meeting: IMeeting, isEditMode: boolean },
    private _store$: Store<IUserState>,
    private _snackBar: MatSnackBar) {
    this.submitted = false;
    this.loading = false;
    this.isEditMode = false;
    this.isDateRangeValidator = false;
    this.guests = [];
    this.displayName = 'Me';
    this.profile = null;
    this.isScheduled = new Subject();
    this.startMeetForm = this.formBuilder.group({
      title: ['', Validators.required],
      meetingDate: [new Date(), Validators.required],
      startTime: [moment(), Validators.required],
      endTime: [moment().add(5, 'minute'), Validators.required]
    }, {
      validator: dateRangeValidator('startTime', 'endTime')
    });
    this.guestForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(EMAIL_PATTERN)]]
    }, {
      validator: duplicateGuestValidator('email', this.guests)
    });
  }

  ngOnInit(): void {
    this.isEditMode = this.data.isEditMode;
    const meeting = this.data.meeting as any;
    if (this.isEditMode && meeting) {
      this.startMeetForm = this.formBuilder.group({
        title: [meeting.title, Validators.required],
        meetingDate: [meeting.meetingDate, Validators.required],
        startTime: [moment(Number(meeting.startTime)), Validators.required],
        endTime: [moment(Number(meeting.endTime)), Validators.required]
      }, {
        validator: dateRangeValidator('startTime', 'endTime')
      });
      if (meeting.guestEmailsString) {
        const guestList = meeting.guestEmailsString.split(',');
        if (guestList && guestList.length) {
          guestList.forEach((guestEmail: string) => {
            const bgColor = this.getBgColor();
            const color = this.getColor(bgColor);
            this.guests.push({ email: guestEmail, bgColor, color });
          });
        }
      }
    }
    this._store$.select(getPending)
      .pipe()
      .subscribe((loading: boolean) => this.loading = !!loading);
    this._store$.select(getProfile).subscribe((profile: IProfile | null) => {
      this.profile = profile;
      this.displayName = profile && profile.name || '';
    });
  }

  public getBgColor(): string {
    // tslint:disable-next-line:no-bitwise
    return '#' + ((1 << 24) * Math.random() | 0).toString(16);
  }
  public getColor(hex: string): string {
    hex = hex.replace('#', '');
    // tslint:disable-next-line:no-bitwise
    return (Number(`0x1${hex}`) ^ 0xFFFFFF).toString(16).substr(1);
  }

  public addGuest(): void {
    if (this.guestForm.valid) {
      const bgColor = this.getBgColor();
      const color = this.getColor(bgColor);
      this.guests.push({ email: this.guestForm.value.email, bgColor, color });
      const guestEmail = this.guestForm.get('email');
      guestEmail?.reset();
    }
  }

  public removeGuest(guestEmail: string): void {
    this.guests = this.guests.filter((guest) => guest.email !== guestEmail);
    const guestEmailControl = this.guestForm.get('email');
    guestEmailControl?.reset();
  }

  public get controls(): { [key: string]: AbstractControl } {
    return this.startMeetForm.controls;
  }

  public onSubmit(): void {
    if (this.startMeetForm.valid) {
      this.loading = true;
      const meetingMetaData = {
        ...this.startMeetForm?.value,
        guests: this.guests.map((guest) => guest.email)
      };
      this._store$.dispatch(MeetingActions.startMeeting({ meetingMetaData }));
    }

  }

  public update(): void {
    if (this.startMeetForm.valid) {
      this.loading = true;
      const meetingMetaData = {
        ...this.startMeetForm?.value,
        guests: this.guests.map((guest) => guest.email),
        id: this.data?.meeting?.id
      };
      this._store$.dispatch(MeetingActions.updateMeeting({ meetingMetaData }));
      this.dialogRef.close();
    }

  }

  public schedule(): void {
    if (this.startMeetForm.valid) {
      this.isScheduled.next(true);
      const meetingMetaData = {
        ...this.startMeetForm?.value,
        guests: this.guests.map((guest) => guest.email)
      };
      this._store$.dispatch(MeetingActions.startMeeting({ meetingMetaData }));
    }

  }

  public onCancel(): void {
    this.dialogRef.close();
  }
}
