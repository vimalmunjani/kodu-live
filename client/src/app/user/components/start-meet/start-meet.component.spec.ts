import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartMeetComponent } from './start-meet.component';

describe('SignInComponent', () => {
  let component: StartMeetComponent;
  let fixture: ComponentFixture<StartMeetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StartMeetComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartMeetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
