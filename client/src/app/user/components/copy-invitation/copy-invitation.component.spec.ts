import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyInvitationComponent } from './copy-invitation.component';

describe('CopyInvitationComponent', () => {
  let component: CopyInvitationComponent;
  let fixture: ComponentFixture<CopyInvitationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyInvitationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyInvitationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
