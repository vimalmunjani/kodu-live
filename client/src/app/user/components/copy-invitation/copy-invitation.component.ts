import { Component, OnInit, Inject } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { IUserState, getPending } from '../../reducers';
import { IMeeting } from '../../models/meeting.model';
import { getProfile } from '../../selectors/profile.selector';
import { IProfile } from '../../models/profile.model';

@Component({
  selector: 'kodu-copy-invitation',
  templateUrl: './copy-invitation.component.html',
  styleUrls: ['./copy-invitation.component.scss']
})
export class CopyInvitationComponent implements OnInit {

  public copyInvitationForm: FormGroup;
  public submitted: boolean;
  public loading: boolean;
  public joinMeet: boolean;
  public profile: IProfile | null;
  public displayName: string;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CopyInvitationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IMeeting,
    private _store$: Store<IUserState>,
    private _snackBar: MatSnackBar) {
    this.submitted = false;
    this.loading = false;
    this.joinMeet = false;
    this.displayName = 'Kodu';
    this.profile = null;
    this.copyInvitationForm = this.formBuilder.group({
      meetingInvitation: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this._store$.select(getProfile).subscribe((profile: IProfile | null) => {
      this.profile = profile;
      this.displayName = profile && profile.name && profile.name.split(' ')[0] || '';
    });
    const meetingLink = `https://meeting.kodulive.com/${this.data?.meetingCode}`;
    const options = {
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      month: 'short',
      timeZoneName: 'short',
      weekday: 'short',
      year: 'numeric'
    };
    const invitation = `
    Kodulive Meet
    Let’s make Life Better Together!.

    You have been invited to a meeting.
    Meeting Title: ${this.data?.title}

    When (India/Kolkata) : ${new Date(parseInt(this.data.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'Asia/Kolkata' })}
    When (America/New York) : ${new Date(parseInt(this.data.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'America/New_York' })}

    Join meeting using following link :
     ${meetingLink}
    `;
    this.copyInvitationForm.patchValue({
      meetingInvitation: invitation,
    });

    this._store$.select(getPending)
      .pipe()
      .subscribe((loading: boolean) => this.loading = !!loading);

  }

  public get controls(): { [key: string]: AbstractControl } {
    return this.copyInvitationForm.controls;
  }

  public reset(): void {
    this.copyInvitationForm.reset();
  }

  public onSubmit(textarea: any): void {
    if (this.copyInvitationForm.valid) {
      textarea.select();
      document.execCommand('copy');
      textarea.blur();
      this._snackBar.open('Meeting invitation copied', '', { duration: 1000 });
      const meetingInvitation = this.copyInvitationForm.value.meetingInvitation;
    }

  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public openGmail(): void {
    const meetingLink = `https://meeting.kodulive.com/${this.data?.meetingCode}`;
    const options = {
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      month: 'short',
      timeZoneName: 'short',
      weekday: 'short',
      year: 'numeric'
    };
    const invitation = `
    Kodulive Meet%0a
    Let’s make Life Better Together!.%0a%0a

    You have been invited to a meeting.%0a
    Meeting Title: ${this.data?.title}%0a%0a

    When (India/Kolkata) : ${new Date(parseInt(this.data.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'Asia/Kolkata' })}%0a
    When (America/New York) : ${new Date(parseInt(this.data.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'America/New_York' })}%0a%0a
    Join meeting using following link :%0a
     ${meetingLink}
    `;
    window.open(`https://mail.google.com/mail/u/0/?view=cm&fs=1&su=Kodulive Meeting&body=${invitation}&tf=1`);
  }

  public openOutlook(): void {
    const meetingLink = `https://meeting.kodulive.com/${this.data?.meetingCode}`;
    const options = {
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      month: 'short',
      timeZoneName: 'short',
      weekday: 'short',
      year: 'numeric'
    };
    const invitation = `
    Kodulive Meet%0a
    Let’s make Life Better Together!.%0a%0a

    You have been invited to a meeting.%0a
    Meeting Title: ${this.data?.title}%0a%0a

    When (India/Kolkata) : ${new Date(parseInt(this.data.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'Asia/Kolkata' })}%0a
    When (America/New York) : ${new Date(parseInt(this.data.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'America/New_York' })}%0a%0a
    Join meeting using following link :%0a
     ${meetingLink}%0a
    `;
    window.open(`mailto:?subject=Kodulive Meeting&body=${invitation}`);
  }
}


