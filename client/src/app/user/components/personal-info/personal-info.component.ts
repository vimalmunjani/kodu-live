import { Component, OnInit, Inject } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { ProfileActions } from '../../actions';
import { IProfile } from '../../models/profile.model';
import { IUserState, getPending, getError } from '../../reducers';
import { EMAIL_PATTERN } from 'src/app/auth/utils/constants';

@Component({
  selector: 'kodu-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnInit {

  public personalInfoForm: FormGroup;
  public submitted: boolean;
  public loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<PersonalInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IProfile,
    private _store$: Store<IUserState>,
    private _snackBar: MatSnackBar) {
    this.submitted = false;
    this.loading = false;
    this.personalInfoForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
      contact: ['', [Validators.pattern('^[0-9]*$'),
      Validators.minLength(10), Validators.maxLength(12)]]
    });
  }

  ngOnInit(): void {
    this.personalInfoForm.patchValue({
      name: this.data.name,
      email: this.data.email,
      contact: this.data.contact
    });

    this._store$.select(getPending)
      .pipe()
      .subscribe((loading: boolean) => this.loading = !!loading);


  }

  public get controls(): { [key: string]: AbstractControl } {
    return this.personalInfoForm.controls;
  }

  public onSubmit(): void {
    if (this.personalInfoForm.valid) {
      const profile: IProfile = {
        ...this.personalInfoForm.value
      };
      this._store$.dispatch(ProfileActions.updateProfile({ profile }));
      this.onCancel();
    }

  }

  public onCancel(): void {
    this.dialogRef.close();
  }
}
