import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { JoinMeetComponent } from '../join-meet/join-meet.component';
import { MeetingActions } from '../../actions';
import { IUserState } from '../../reducers';
import { Store } from '@ngrx/store';
import { IMeeting } from '../../models/meeting.model';
import { MeetingDataSource } from '../../services/meeting.datasource';
import { merge } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import { StartMeetComponent } from '../start-meet/start-meet.component';
import { getMeeting, getMeetingPagination } from '../../selectors/meeting.selector';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getProfile } from '../../selectors/profile.selector';
import { IProfile } from '../../models/profile.model';
import { CopyInvitationComponent } from '../copy-invitation/copy-invitation.component';

@Component({
  selector: 'kodu-meetings',
  templateUrl: './meetings.component.html',
  styleUrls: ['./meetings.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('50ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class MeetingsComponent implements OnInit, AfterViewInit {

  private joinMeetingDialogRef!: MatDialogRef<JoinMeetComponent>;
  private copyInvitationDialogRef!: MatDialogRef<CopyInvitationComponent>;
  private deleteMeetingDialogRef!: MatDialogRef<ConfirmDialogComponent>;
  private startMeetingDialogRef!: MatDialogRef<StartMeetComponent>;
  public meetingForm: FormGroup;
  public displayName: string;
  public profile: IProfile | null;



  displayedColumns: string[] = ['title', 'meetingCode', 'meetingDate', 'startTime', 'endTime', 'guests', 'join'];
  dataSource: MeetingDataSource;
  meetings: Array<IMeeting> = [];
  selection = new SelectionModel<IMeeting>(true, []);
  isScheduled: boolean;
  startMeet: boolean;
  loading: boolean;
  expandedMeeting: IMeeting;
  meetingCodeFromParam: string;
  totalMeetings: number;



  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog, private _store$: Store<IUserState>, private route: ActivatedRoute) {
    this.dataSource = new MeetingDataSource(_store$);
    this.isScheduled = false;
    this.startMeet = false;
    this.loading = false;
    this.displayName = 'Me';
    this.profile = null;
    this.expandedMeeting = {
      id: '-1',
      title: '',
      endTime: '00',
      startTime: '00',
      guests: [],
      meetingCode: 'meeting-code',
      meetingDate: new Date(),
    };
    this.totalMeetings = 0;
    this.meetingCodeFromParam = '';
    this.meetingForm = this.formBuilder.group({
      title: ['', [Validators.required]]
    });
  }

  public onSubmit(): void {
    if (this.meetingForm.valid) {
      // this.loading = true;
      const title = this.meetingForm.value.title;
      const meeting = { ...this.expandedMeeting, title };
      this.updateMeeting(meeting);
      // this._store$.dispatch(MeetingActions.updateMeeting({ meeting }));
    }

  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.meetingCodeFromParam = params && params.meetingCode;
    });
    this._store$.select(getProfile).subscribe((profile: IProfile | null) => {
      this.profile = profile;
      this.displayName = profile && profile.name || '';
    });
    this._store$.select(getMeetingPagination).pipe().subscribe((pagination) => {
      if (pagination) {
        this.loading = false;
        this.paginator.pageIndex = pagination.pageIndex;
        this.paginator.pageSize = pagination.pageSize;
        this.totalMeetings = pagination.totalCount;
      }
    });
    this.dataSource.loadMeetings();
    this._store$.select(getMeeting).pipe().subscribe((meeting: IMeeting | null) => {
      if (meeting && meeting.meetingCode && this.startMeetingDialogRef && this.startMeet) {
        this.startMeet = false;
        this.startMeetingDialogRef.close();
        if (!this.isScheduled) {
          const meetingTitleParam = meeting.title.split(' ').join('-');
          window.open(`https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${meeting.token}#config.subject="${meetingTitleParam}"`);
        }
        this.loadPage();
      }
    });
  }
  setTitle(row: IMeeting): void {
    this.meetingForm.patchValue({
      title: row.title
    });
  }

  ngAfterViewInit(): void {

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadPage())
      )
      .subscribe();
  }

  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  public checkboxLabel(row?: IMeeting): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

  public joinMeeting(meeting?: IMeeting): void {
    this.joinMeetingDialogRef = this.dialog.open(JoinMeetComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: meeting
    });
  }

  public copyInvitation(meeting?: IMeeting): void {
    this.copyInvitationDialogRef = this.dialog.open(CopyInvitationComponent, {
      width: '700px',
      height: 'auto',
      disableClose: true,
      data: meeting
    });
  }

  public joinMeetingAsParticipant(meeting?: IMeeting): void {
    this.joinMeetingDialogRef = this.dialog.open(JoinMeetComponent, {
      width: '500px',
      height: 'auto',
      disableClose: true,
      data: { meetingCode: this.meetingCodeFromParam }
    });
  }
  public deleteMeeting(meeting?: IMeeting): void {
    this.deleteMeetingDialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      height: 'auto',
      disableClose: true,
      data: meeting
    });
    this.deleteMeetingDialogRef.componentInstance.subject = meeting?.title;
    this.deleteMeetingDialogRef.componentInstance.confirm$.subscribe((isComfrimed: boolean) => {
      if (isComfrimed && meeting) {
        this._store$.dispatch(MeetingActions.cancelMeeting({ meeting }));
        this.deleteMeetingDialogRef.close();
      }

    });
  }

  public startMeeting(): void {
    this.startMeet = true;
    this.startMeetingDialogRef = this.dialog.open(StartMeetComponent, {
      width: '600px',
      height: 'auto',
      disableClose: true,
      data: 'meeting-code-aplha',
    });
    this.startMeetingDialogRef.componentInstance.isScheduled.subscribe((isScheduled: boolean) => {
      this.isScheduled = isScheduled;
    });
  }

  public updateMeeting(meeting: IMeeting): void {
    this.startMeet = true;
    this.startMeetingDialogRef = this.dialog.open(StartMeetComponent, {
      width: '600px',
      height: 'auto',
      disableClose: true,
      data: { meeting, isEditMode: true },
    });
  }

  public loadPage(): void {
    this.dataSource.loadMeetings(
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  // public copyMeetingCode(meetingCode: string): void {
  //   const textAreaForCopy = document.createElement('textarea');
  //   textAreaForCopy.style.position = 'fixed';
  //   textAreaForCopy.style.left = '0';
  //   textAreaForCopy.style.top = '0';
  //   textAreaForCopy.style.opacity = '0';
  //   textAreaForCopy.value = meetingCode;
  //   document.body.appendChild(textAreaForCopy);
  //   textAreaForCopy.focus();
  //   textAreaForCopy.select();
  //   document.execCommand('copy');
  //   document.body.removeChild(textAreaForCopy);
  // }
  public openGmail(meeting: IMeeting): void {
    const meetingLink = `https://meeting.kodulive.com/${meeting?.meetingCode}`;
    const options = {
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      month: 'short',
      timeZoneName: 'short',
      weekday: 'short',
      year: 'numeric'
    };
    const invitation = `
    Kodulive Meet%0a
    Let’s make Life Better Together!.%0a%0a

    You have been invited to a meeting.%0a
    Meeting Title: ${meeting?.title}%0a%0a

    When (India/Kolkata) : ${new Date(parseInt(meeting?.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'Asia/Kolkata' })}%0a
    When (America/New York) : ${new Date(parseInt(meeting?.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'America/New_York' })}%0a%0a
    Join meeting using following link :%0a
     ${meetingLink}
    `;
    window.open(`https://mail.google.com/mail/u/0/?view=cm&fs=1&su=Kodulive Meeting&body=${invitation}&tf=1`);
  }

  public openOutlook(meeting: IMeeting): void {
    const meetingLink = `https://meeting.kodulive.com/${meeting?.meetingCode}`;
    const options = {
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      month: 'short',
      timeZoneName: 'short',
      weekday: 'short',
      year: 'numeric'
    };
    const invitation = `
    Kodulive Meet%0a
    Let’s make Life Better Together!.%0a%0a

    You have been invited to a meeting.%0a
    Meeting Title: ${meeting?.title}%0a%0a

    When (India/Kolkata) : ${new Date(parseInt(meeting?.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'Asia/Kolkata' })}%0a
    When (America/New York) : ${new Date(parseInt(meeting?.startTime, 10)).toLocaleString('en-Us', { ...options, timeZone: 'America/New_York' })}%0a%0a
    Join meeting using following link :%0a
     ${meetingLink}%0a%0a

    `;
    window.open(`mailto:?subject=Kodulive Meeting&body=${invitation}`);
  }

}
