import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProfileActions } from '../../actions';
import { IUserState } from '../../reducers';
import { IProfile } from '../../models/profile.model';
import { getProfile } from '../../selectors/profile.selector';
import { JwtService } from 'src/app/core/service/jwt.service';
import { Router } from '@angular/router';
import { AuthActions } from 'src/app/auth/actions';

@Component({
  selector: 'kodu-user-base',
  templateUrl: './user-base.component.html',
  styleUrls: ['./user-base.component.scss']
})
export class UserBaseComponent implements OnInit {
  public profile: IProfile | null;
  public profileImageUrl: string;
  public menus = [
    {
      name: 'Profile',
      icon: 'person_outline',
      description: 'Your profile details',
      route: 'profile',
      class: 'active'
    },
    {
      name: 'Meetings',
      icon: 'videocam',
      description: 'Upcoming meetings',
      route: 'meeting',
      class: ''
    },
    {
      name: 'Faqs',
      icon: 'contact_support',
      description: 'Frequently Asked Questions',
      route: 'faq',
      class: ''
    }
  ];


  constructor(private _store$: Store<IUserState>, private _jwtService: JwtService, private router: Router) {
    this.profile = null;
    this.profileImageUrl = 'assets/images/user.svg';
  }

  ngOnInit(): void {
    this._store$.dispatch(ProfileActions.fetchProfile());
    this._store$.select(getProfile).subscribe((profile: IProfile | null) => {
      this.profile = profile;
      if (this.profile?.role === 'SUPPORTADMIN') {
        this.menus = [
          {
            name: 'Profile',
            icon: 'person_outline',
            description: 'Your profile details',
            route: 'profile',
            class: 'active'
          },
          {
            name: 'Meetings',
            icon: 'videocam',
            description: 'Upcoming meetings',
            route: 'meeting',
            class: ''
          },
          {
            name: 'Faqs',
            icon: 'contact_support',
            description: 'Frequently Asked Questions',
            route: 'faq',
            class: ''
          }
        ];
      } else {
        this.menus = [
          {
            name: 'Profile',
            icon: 'person_outline',
            description: 'Your profile details',
            route: 'profile',
            class: 'active'
          },
          {
            name: 'Meetings',
            icon: 'videocam',
            description: 'Upcoming meetings',
            route: 'meeting',
            class: ''
          }
        ];
      }
      this.profileImageUrl = profile && profile.photoUrl || 'assets/images/user.svg';
    });

  }

  logout(): void {
    this._jwtService.destroyToken();
    this._store$.dispatch(AuthActions.logoutConfirmation());

  }


}
