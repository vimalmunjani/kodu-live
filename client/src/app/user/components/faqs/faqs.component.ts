import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { JoinMeetComponent } from '../join-meet/join-meet.component';
import { FaqActions } from '../../actions';
import { IUserState } from '../../reducers';
import { Store } from '@ngrx/store';
import { IFaq } from '../../models/faq.model';
import { FaqDataSource } from '../../services/faq.datasource';
import { merge } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import { StartMeetComponent } from '../start-meet/start-meet.component';
import { getFaq, getFaqPagination } from '../../selectors/faq.selector';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FaqFormComponent } from '../faq-form/faq-form.component';

@Component({
  selector: 'kodu-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('50ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class FaqsComponent implements OnInit, AfterViewInit {

  private deleteMeetingDialogRef!: MatDialogRef<ConfirmDialogComponent>;
  private faqFormDialogRef!: MatDialogRef<FaqFormComponent>;


  displayedColumns: string[] = ['question'];
  dataSource: FaqDataSource;
  faqs: Array<IFaq> = [];
  selection = new SelectionModel<IFaq>(true, []);
  isScheduled: boolean;
  startMeet: boolean;
  loading: boolean;
  expandedFaq: IFaq;
  totalFaqs: number;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog, private _store$: Store<IUserState>, private route: ActivatedRoute) {
    this.dataSource = new FaqDataSource(_store$);
    this.isScheduled = false;
    this.startMeet = false;
    this.loading = false;
    this.expandedFaq = {
      id: '-1',
      question: '',
      answer: '',
      faqCategory: {
        name: '',
        disabled: false
      }
    };
    this.totalFaqs = 0;
  }


  ngOnInit(): void {
    this._store$.select(getFaqPagination).pipe().subscribe((pagination) => {
      if (pagination) {
        this.loading = false;
        this.paginator.pageIndex = pagination.pageIndex;
        this.paginator.pageSize = pagination.pageSize;
        this.totalFaqs = pagination.totalCount;
      }
    });
    this.dataSource.loadFaqs();
  }


  ngAfterViewInit(): void {

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadPage())
      )
      .subscribe();
  }

  public isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  public checkboxLabel(row?: IFaq): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }


  public loadPage(): void {
    this.dataSource.loadFaqs(
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  public editFaq(faq: IFaq): void {
    this.faqFormDialogRef = this.dialog.open(FaqFormComponent, {
      width: '600px',
      height: 'auto',
      disableClose: true,
      data: faq
    });
  }
  public addFaq(): void {
    this.startMeet = true;
    this.faqFormDialogRef = this.dialog.open(FaqFormComponent, {
      width: '600px',
      height: 'auto',
      disableClose: true,
      data: null,
    });
  }

}
