export interface IProfile {
    id: number;
    name: string;
    email: string;
    contact: string;
    address: string;
    photoUrl: string;
    subscription?: any;
    role?: string;
}
