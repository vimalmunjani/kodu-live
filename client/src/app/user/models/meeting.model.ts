import { IProfile } from './profile.model';

export interface IMeeting {
    id: string;
    title: string;
    meetingCode: string;
    startTime: string;
    endTime: string;
    meetingDate: Date;
    organizer?: IProfile;
    guests: Array<IProfile>;
    token?: string;
}

export class Meeting {
    constructor() {

    }
}

