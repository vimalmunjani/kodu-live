import { IFaqCategory } from './faq-category.model';

export interface IFaq {
    id?: string;
    question: string;
    answer: string;
    faqCategory: IFaqCategory;
}



