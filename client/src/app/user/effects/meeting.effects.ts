import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { switchMap, map, catchError, tap, withLatestFrom } from 'rxjs/operators';
import { MeetingActions } from '../actions';
import { MeetingService } from '../services/meeting.service';
import { of } from 'rxjs';
import { IMeeting } from '../models/meeting.model';
import { IUserState } from '../reducers/user.reducer';
import { Store } from '@ngrx/store';
import { getMeetingPagination } from '../selectors/meeting.selector';

@Injectable()
export class MeetingEffects {

    constructor(
        private actions$: Actions,
        private meetingService: MeetingService,
        private _store$: Store<IUserState>
    ) { }

    fetch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MeetingActions.fetchMeetings),
            switchMap((pagination: { pageIndex: number, pageSize: number }) => {
                return this.meetingService.fetch(pagination)
                    .pipe(
                        map((paginatedMeetings: {
                            data: Array<IMeeting>,
                            totalCount: number,
                            pageIndex: number,
                            pageSize: number
                        }) => MeetingActions.fetchMeetingsSuccess({ paginatedMeetings })),
                        catchError((error) => of(MeetingActions.fetchMeetingsFailure({ error })))
                    );
            })
        )
    );

    start$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MeetingActions.startMeeting),
            map((action) => action.meetingMetaData),
            switchMap((meetingMetaData: {
                title: string,
                meetingDate: Date,
                startTime: string,
                endTime: string,
                guests: Array<string>
            }) => {
                return this.meetingService.start(meetingMetaData)
                    .pipe(
                        map((meeting: IMeeting) => MeetingActions.startMeetingSuccess({ meeting })),
                        tap(() => MeetingActions.fetchMeetings({
                            pageIndex: 0,
                            pageSize: 10
                        })),
                        catchError((error) => of(MeetingActions.startMeetingFailure({ error })))
                    );
            })
        )
    );

    join$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MeetingActions.joinMeeting),
            map((action) => action.meetingCode),
            switchMap((meetingCode: string) => {
                return this.meetingService.join(meetingCode)
                    .pipe(
                        map((meeting: IMeeting) => MeetingActions.joinMeetingSuccess({ meeting })),
                        tap(() => MeetingActions.fetchMeetings({
                            pageIndex: 0,
                            pageSize: 10
                        })),
                        catchError((error) => of(MeetingActions.joinMeetingFailure({ error })))
                    );
            })
        )
    );

    update$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MeetingActions.updateMeeting),
            map((action) => action.meetingMetaData),
            switchMap((meetingMetaData: {
                title: string,
                meetingDate: Date,
                startTime: string,
                endTime: string,
                guests: Array<string>
            }) => {
                return this.meetingService.update(meetingMetaData)
                    .pipe(
                        map((updatedMeeting: IMeeting) => MeetingActions.updateMeetingSuccess({ meeting: updatedMeeting })),
                        catchError((error) => of(MeetingActions.updateMeetingFailure({ error })))
                    );
            })
        )
    );

    postUpdateSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MeetingActions.updateMeetingSuccess),
            withLatestFrom(this._store$.select(getMeetingPagination)),
            map(([action, meetingPagination]) => meetingPagination),
            switchMap(({ pageIndex, pageSize }) => of(MeetingActions.fetchMeetings({
                pageIndex,
                pageSize
            })))
        ),
    );


    cancel$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MeetingActions.cancelMeeting),
            map((action) => action.meeting),
            switchMap((meeting: IMeeting) => {
                return this.meetingService.cancel(meeting)
                    .pipe(
                        map((updatedMeeting: IMeeting) => MeetingActions.cancelMeetingSuccess({ meeting: updatedMeeting })),
                        catchError((error) => of(MeetingActions.cancelMeetingFailure({ error })))
                    );
            })
        )
    );

    postCancelSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MeetingActions.cancelMeetingSuccess),
            withLatestFrom(this._store$.select(getMeetingPagination)),
            map(([action, meetingPagination]) => {
                const isLastMeetingOnPage = (
                    (meetingPagination.pageIndex) * meetingPagination.pageSize
                ) === (meetingPagination.totalCount - 1);
                if (isLastMeetingOnPage) {
                    return { ...meetingPagination, pageIndex: meetingPagination.pageIndex ? meetingPagination.pageIndex - 1 : 0 };
                } else {
                    return meetingPagination;
                }
            }),
            switchMap(({ pageIndex, pageSize }) => of(MeetingActions.fetchMeetings({
                pageIndex,
                pageSize
            })))
        ),
    );


}
