import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { ProfileActions } from '../actions';
import { ProfileService } from '../services/profile.service';
import { of } from 'rxjs';
import { IProfile } from '../models/profile.model';

@Injectable()
export class ProfileEffects {

    constructor(
        private actions$: Actions,
        private profileService: ProfileService,
    ) { }

    fetch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.fetchProfile),
            switchMap(() => {
                return this.profileService.fetch()
                    .pipe(
                        map((profile) => ProfileActions.fetchProfileSuccess({ profile })),
                        catchError((error) => of(ProfileActions.fetchProfileFailure({ error })))
                    );
            })
        )
    );

    update$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.updateProfile),
            map((action) => action.profile),
            switchMap((profilePayload: IProfile) => {
                return this.profileService.update(profilePayload)
                    .pipe(
                        map((profile) => {
                            return ProfileActions.updateProfileSuccess({ profile });
                        }),
                        catchError((error) => of(ProfileActions.updateProfileFailure({ error })))
                    );
            })
        )
    );


    updateAvatar$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileActions.updateAvatar),
            map((action) => action.avatar),
            switchMap((avatarPayload: File) => {
                return this.profileService.updateAvatar(avatarPayload)
                    .pipe(
                        map((photoUrl: string) => {
                            return ProfileActions.updateAvatarSuccess({ photoUrl });
                        }),
                        catchError((error) => of(ProfileActions.updateAvatarFailure({ error })))
                    );
            })
        )
    );






}
