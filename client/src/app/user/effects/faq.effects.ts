import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { switchMap, map, catchError, withLatestFrom } from 'rxjs/operators';
import { FaqActions } from '../actions';
import { FaqService } from '../services/faq.service';
import { of } from 'rxjs';
import { IFaq } from '../models/faq.model';
import { IUserState } from '../reducers/user.reducer';
import { Store } from '@ngrx/store';
import { getFaqPagination } from '../selectors/meeting.selector';

@Injectable()
export class FaqEffects {

    constructor(
        private actions$: Actions,
        private faqService: FaqService,
        private _store$: Store<IUserState>
    ) { }

    fetch$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FaqActions.fetchFaqs),
            switchMap((pagination: { pageIndex: number, pageSize: number }) => {
                return this.faqService.fetch(pagination)
                    .pipe(
                        map((paginatedFaqs: {
                            data: Array<IFaq>,
                            totalCount: number,
                            pageIndex: number,
                            pageSize: number
                        }) => FaqActions.fetchFaqsSuccess({ paginatedFaqs })),
                        catchError((error) => of(FaqActions.fetchFaqsFailure({ error })))
                    );
            })
        )
    );



    postUpdateSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(FaqActions.updateFaqSuccess),
            withLatestFrom(this._store$.select(getFaqPagination)),
            map(([action, meetingPagination]) => meetingPagination),
            switchMap(({ pageIndex, pageSize }) => of(FaqActions.fetchFaqs({
                pageIndex,
                pageSize
            })))
        ),
    );


}
