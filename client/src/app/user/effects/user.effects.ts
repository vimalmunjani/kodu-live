import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { UserService } from '../services/user.service';

@Injectable()
export class UserEffects {

    constructor(
        private actions$: Actions,
        private userService: UserService,
    ) { }

}
