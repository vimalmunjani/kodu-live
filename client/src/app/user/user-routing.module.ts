import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserBaseComponent, ProfileComponent, MeetingsComponent, FaqsComponent } from './components';


const routes: Routes = [
  {
    path: '',
    component: UserBaseComponent,
    children: [
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'meeting',
        component: MeetingsComponent
      },
      {
        path: 'faq',
        component: FaqsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
