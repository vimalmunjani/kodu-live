export const BASE_USER_URL = `api/user`;


export const PROFILE_URL = `${BASE_USER_URL}/profile`;
export const AVATAR_URL = `${BASE_USER_URL}/avatar`;
export const FETCH_MEETING_URL = `${BASE_USER_URL}/meetings`;
export const START_MEETING_URL = `${BASE_USER_URL}/meeting`;
export const JOIN_MEETING_URL = `${BASE_USER_URL}/join-meeting`;
export const UPDATE_MEETING_URL = `${BASE_USER_URL}/meeting`;
export const CANCEL_MEETING_URL = `${BASE_USER_URL}/cancel-meeting`;

export const BASE_SUBSCRIPTION_URL = `api/subscription`;

export const UPDATE_PLAN_URL = `${BASE_SUBSCRIPTION_URL}/create`;
export const CANCEL_PLAN_URL = `${BASE_SUBSCRIPTION_URL}/cancel`;
export const REACTIVATE_PLAN_URL = `${BASE_SUBSCRIPTION_URL}/reactivate`;
export const RESET_PLAN_URL = `${BASE_SUBSCRIPTION_URL}/reset`;

export const BASE_SUPPORT_URL = `api/support`;
export const FETCH_FAQS_URL = `${BASE_SUPPORT_URL}/faqs`;