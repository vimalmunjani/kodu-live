import { FormGroup } from '@angular/forms';

export function dateRangeValidator(startControlName: string, endControlName: string): any {
    return (formGroup: FormGroup) => {
        const startControl = formGroup.controls[startControlName];
        const endControl = formGroup.controls[endControlName];
        if (endControl.errors && !endControl.errors.dateRangeValidator) {
            return;
        }
        if (startControl.value.valueOf() >= endControl.value.valueOf()) {
            endControl.setErrors({ dateRangeValidator: true });
        } else {
            endControl.setErrors(null);
        }
    };
}