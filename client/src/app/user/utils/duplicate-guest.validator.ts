
import { FormGroup } from '@angular/forms';

export function duplicateGuestValidator(controlName: string, guests: Array<{ email: string, bgColor: string }>): any {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const emails = guests.map((guest) => guest.email);
        if (control.errors && !control.errors.duplicateGuestValidator) {
            return;
        }
        if (emails.includes(control.value)) {
            control.setErrors({ duplicateGuestValidator: true });
        } else {
            control.setErrors(null);
        }
    };
}