import { createReducer, on } from '@ngrx/store';
import {
    ProfileActions, MeetingActions, FaqActions
} from '../actions';

import { IProfile } from '../models/profile.model';
import { IMeeting } from '../models/meeting.model';
import { IFaq } from '../models/faq.model';
import { IError } from '../../core/models/error.model';


export interface IUserState {
    error: IError | null;
    pending: boolean;
    profile: IProfile | null;
    meetings: Array<IMeeting> | [];
    meetingPagination: {
        totalCount: number,
        pageIndex: number,
        pageSize: number
    };
    meeting: IMeeting | null;
    faqs: Array<IFaq> | [];
    faqPagination: {
        totalCount: number,
        pageIndex: number,
        pageSize: number
    };
    faq: IFaq | null;
}

const initialUserState: IUserState = {
    error: null,
    pending: false,
    profile: null,
    meetings: [],
    meetingPagination: {
        totalCount: 0,
        pageIndex: 1,
        pageSize: 10
    },
    faqs: [],
    meeting: null,
    faqPagination: {
        totalCount: 0,
        pageIndex: 1,
        pageSize: 10
    },
    faq: null,
};

export const userReducer = createReducer(
    initialUserState,
    on(ProfileActions.fetchProfile, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(ProfileActions.fetchProfileSuccess, (state, { profile }) => ({
        ...state,
        error: null,
        pending: false,
        profile
    })),
    on(ProfileActions.fetchProfileFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false,
        profile: null
    })),
    on(ProfileActions.updateProfile, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(ProfileActions.updateProfileSuccess, (state, { profile }) => ({
        ...state,
        error: null,
        pending: false,
        profile
    })),
    on(ProfileActions.updateProfileFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false
    })),
    on(MeetingActions.updateMeeting, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(MeetingActions.updateMeetingSuccess, (state, { meeting }) => ({
        ...state,
        error: null,
        pending: false
    })),
    on(MeetingActions.updateMeetingFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false
    })),
    on(ProfileActions.updateAvatar, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(ProfileActions.updateAvatarSuccess, (state, { photoUrl }) => ({
        ...state,
        error: null,
        pending: false,
        profile: { ...state.profile, photoUrl } as IProfile
    })),
    on(ProfileActions.updateAvatarFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false
    })),
    on(MeetingActions.fetchMeetings, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(MeetingActions.fetchMeetingsSuccess, (state, { paginatedMeetings }) => ({
        ...state,
        error: null,
        pending: false,
        meetings: paginatedMeetings.data,
        meetingPagination: {
            totalCount: paginatedMeetings.totalCount,
            pageIndex: paginatedMeetings.pageIndex,
            pageSize: paginatedMeetings.pageSize
        }
    })),
    on(MeetingActions.fetchMeetingsFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false,
        meetings: []
    })),
    on(MeetingActions.startMeeting, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(MeetingActions.startMeetingSuccess, (state, { meeting }) => ({
        ...state,
        error: null,
        pending: false,
        meeting
    })),
    on(MeetingActions.startMeetingFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false
    })),
    on(MeetingActions.joinMeeting, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(MeetingActions.joinMeetingSuccess, (state, { meeting }) => ({
        ...state,
        error: null,
        pending: false,
        meeting
    })),
    on(MeetingActions.joinMeetingFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false
    })),
    on(FaqActions.fetchFaqs, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(FaqActions.fetchFaqsSuccess, (state, { paginatedFaqs }) => ({
        ...state,
        error: null,
        pending: false,
        faqs: paginatedFaqs.data,
        faqPaginationPagination: {
            totalCount: paginatedFaqs.totalCount,
            pageIndex: paginatedFaqs.pageIndex,
            pageSize: paginatedFaqs.pageSize
        }
    })),
    on(FaqActions.fetchFaqsFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false,
        meetings: []
    }))
);

export const getError = (state: IUserState) => state.error;
export const getPending = (state: IUserState) => state.pending;

