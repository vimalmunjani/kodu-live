import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { userReducer } from './reducers';
import { UserEffects, ProfileEffects, MeetingEffects, FaqEffects } from './effects';
import { UserService, ProfileService, SubscriptionService } from './services';

import { UserRoutingModule } from './user-routing.module';
import { UserBaseComponent, ProfileComponent, MeetingsComponent, FaqsComponent, FaqFormComponent, ChangePasswordComponent } from './components';
import { MaterialModule } from '../material/material.module';
import { PersonalInfoComponent } from './components/personal-info/personal-info.component';
import { JoinMeetComponent } from './components/join-meet/join-meet.component';
import { CopyInvitationComponent } from './components/copy-invitation/copy-invitation.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { StartMeetComponent } from './components/start-meet/start-meet.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatNativeDateModule } from '@angular-material-components/datetime-picker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';

export const COMPONENTS = [
  UserBaseComponent,
  ProfileComponent,
  MeetingsComponent,
  FaqsComponent,
  PersonalInfoComponent,
  JoinMeetComponent,
  CopyInvitationComponent,
  ConfirmDialogComponent,
  StartMeetComponent,
  FaqFormComponent,
  ChangePasswordComponent
];

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    NgxMatNativeDateModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatMomentModule,
    StoreModule.forFeature('user', userReducer),
    EffectsModule.forFeature([UserEffects, ProfileEffects, MeetingEffects, FaqEffects]),
  ],
  declarations: COMPONENTS,
  providers: [
    UserService,
    ProfileService,
    SubscriptionService,
    DatePipe
  ],
  entryComponents: [
    PersonalInfoComponent,
    JoinMeetComponent,
    CopyInvitationComponent,
    StartMeetComponent,
    ConfirmDialogComponent,
    FaqFormComponent
  ]
})
export class UserModule { }
