import { IUserState } from '../reducers/user.reducer';
import { createSelector } from '@ngrx/store';

export const selectUser = (state: any) => state.user;
export const selectProfile = (state: IUserState) => state.profile;

export const getProfile = createSelector(selectUser, selectProfile);
