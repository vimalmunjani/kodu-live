import { IUserState } from '../reducers/user.reducer';
import { createSelector } from '@ngrx/store';

export const selectUser = (state: any) => state.user;

export const selectFaqs = (state: IUserState) => state.faqs;
export const selectFaqPagination = (state: IUserState) => state.faqPagination;
export const selectFaq = (state: IUserState) => state.faq;


export const getFaqs = createSelector(selectUser, selectFaqs);
export const getFaq = createSelector(selectUser, selectFaq);
export const getFaqPagination = createSelector(selectUser, selectFaqPagination);
