import { IUserState } from '../reducers/user.reducer';
import { createSelector } from '@ngrx/store';

export const selectUser = (state: any) => state.user;

export const selectMeetings = (state: IUserState) => state.meetings;
export const selectMeetingPagination = (state: IUserState) => state.meetingPagination;
export const selectMeeting = (state: IUserState) => state.meeting;

export const selectFaqs = (state: IUserState) => state.faqs;
export const selectFaqPagination = (state: IUserState) => state.faqPagination;
export const selectFaq = (state: IUserState) => state.faq;

export const getMeetings = createSelector(selectUser, selectMeetings);
export const getMeeting = createSelector(selectUser, selectMeeting);
export const getMeetingPagination = createSelector(selectUser, selectMeetingPagination);

export const getFaqs = createSelector(selectUser, selectFaqs);
export const getFaq = createSelector(selectUser, selectFaq);
export const getFaqPagination = createSelector(selectUser, selectFaqPagination);
