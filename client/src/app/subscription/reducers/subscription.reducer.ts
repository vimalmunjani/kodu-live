import { createReducer, on, createFeatureSelector, State, createSelector } from "@ngrx/store";
import { SubscriptionActions } from "../actions";

export interface SubscriptionState {
    plans: {
        error: string | null;
        plans: Array<any>
    }
}

const initialSubscriptionState: SubscriptionState = {
    plans: {
        error: null,
        plans: []
    }
}

export const subscriptionReducer = createReducer(
    initialSubscriptionState,
    on(SubscriptionActions.fetchPlansSuccess, (state, { plans }) => ({
        ...state,
        plans: {
            ...state.plans,
            error: null,
            plans
        }
    })),
    on(SubscriptionActions.fetchPlansFailure, (state, { error }) => ({
        ...state,
        plans: {
            ...state.plans,
            error: error,
            plans: []
        }
    })),
);

export const selectSubscriptionState = createFeatureSelector<State<any>, SubscriptionState>(<any>'subscription');

export const selectPlans = createSelector(selectSubscriptionState, (state) => state.plans.plans);
export const selectPlansError = createSelector(selectSubscriptionState, (state) => state.plans.error);
