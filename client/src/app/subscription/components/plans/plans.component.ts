import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { selectPlans } from '../../reducers';
import { SubscriptionActions } from '../../actions';

@Component({
  selector: 'kodu-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {

  plans$: any;

  constructor(private _store$: Store<any>) { }

  ngOnInit(): void {
    // this._store$.dispatch(SubscriptionActions.fetchPlans());
    // this.plans$ = this._store$.pipe(select(selectPlans));
  }

}
