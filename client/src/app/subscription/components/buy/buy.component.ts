import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SubscriptionService } from '../../services';
import * as moment from 'moment';
import { Router } from '@angular/router';
declare var paypal: any;

@Component({
  selector: 'kodu-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.scss']
})
export class BuyComponent implements OnInit {

  @ViewChild('paypal', { static: true }) paypalElementRef: ElementRef;
  planId: string;
  tenureType: string;
  nextBillingDate: string;

  constructor(protected subscriptionService: SubscriptionService, protected router: Router) {
    this.paypalElementRef = <any>null;
    this.planId = 'P-0EC29197TB739541EL5UWR4A';
    this.tenureType = 'Annually';
    this.nextBillingDate = '';
  }

  ngOnInit(): void {
    paypal.Buttons({
      createSubscription: (data: any, actions: any) => {
        return actions.subscription.create({
          'plan_id': this.planId
        });
      },
      onApprove: (data: { subscriptionID: string; }, actions: any) => {
        console.log('Subscription complete', data);
        this.subscriptionService.updatePlan(data.subscriptionID).subscribe(() => {
          this.router.navigate(['user', 'profile']);
        });
      }
    }).render(this.paypalElementRef.nativeElement)
  }

  changeTenure(planId: string, tenure: string) {
    this.planId = planId.trim();
    this.tenureType = tenure;
    if (tenure === 'Monthly') {
      this.nextBillingDate = moment().add(1, 'months').format('LL');
    } else {
      this.nextBillingDate = moment().add(1, 'years').format('LL');
    }
  }

}
