import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PlanService } from '../services';
import { SubscriptionActions } from '../actions';
import { map, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class PlanEffects {

    constructor(
        private actions$: Actions,
        private planService: PlanService,
    ) { }

    getPlans$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SubscriptionActions.fetchPlans),
            switchMap(() =>
                this.planService.fetchPlans().pipe(
                    map(({ plans }: any) => SubscriptionActions.fetchPlansSuccess({ plans })),
                    catchError((error) => of(SubscriptionActions.fetchPlansFailure({ error })))
                )
            )
        )
    );

}
