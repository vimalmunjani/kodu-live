export const BASE_SUBSCRIPTION_URL = `api/subscription`;

export const PLANS_URL = `${BASE_SUBSCRIPTION_URL}/plans`;
export const UPDATE_PLAN_URL = `${BASE_SUBSCRIPTION_URL}/create`;
