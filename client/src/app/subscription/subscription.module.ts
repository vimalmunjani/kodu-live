import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubscriptionRoutingModule } from './subscription-routing.module';
import { SubscriptionComponent, PlansComponent, BuyComponent } from './components';
import { MaterialModule } from '../material';
import { PlanService, SubscriptionService } from './services';
import { PlanEffects } from './effects';
import { subscriptionReducer } from './reducers';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CoreModule } from '../core';

export const COMPONENTS = [
  SubscriptionComponent,
  PlansComponent,
  BuyComponent
]

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    MaterialModule,
    SubscriptionRoutingModule,
    StoreModule.forFeature('subscription', subscriptionReducer),
    EffectsModule.forFeature([PlanEffects]),
  ],
  declarations: COMPONENTS,
  providers: [
    PlanService,
    SubscriptionService
  ]
})
export class SubscriptionModule { }
