import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor(private _http: HttpClient) { }

  fetchPlans() {
    return this._http.get(Constants.PLANS_URL);
  }

}
