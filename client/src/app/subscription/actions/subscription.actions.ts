import { createAction, props } from '@ngrx/store';

export const fetchPlans = createAction('[Subscription] Fetch Plans');

export const fetchPlansSuccess = createAction(
    '[Subscription] Fetch Plans Success',
    props<{ plans: any }>()
);

export const fetchPlansFailure = createAction(
    '[Subscription] Fetch Plans Failure',
    props<{ error: any }>()
);
