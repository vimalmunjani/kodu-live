import { createReducer, on, createFeatureSelector, State, createSelector } from "@ngrx/store";
import { AuthActions } from "../actions";

export interface AuthState {
    error: string | null;
    pending: boolean;
    user: any   // this user object will store the token and related data when login is successful
}

const initialAuthState: AuthState = {
    error: null,
    pending: false,
    user: null
}

export const authReducer = createReducer(
    initialAuthState,
    on(AuthActions.login, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(AuthActions.loginSuccess, (state, { user }) => ({
        ...state,
        error: null,
        pending: false,
        user
    })),
    on(AuthActions.loginFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false,
        user: null
    })),
    on(AuthActions.signUp, (state) => ({
        ...state,
        error: null,
        pending: true,
    })),
    on(AuthActions.signUpSuccess, (state, { user }) => ({
        ...state,
        error: null,
        pending: false,
        user
    })),
    on(AuthActions.signUpFailure, (state, { error }) => ({
        ...state,
        error,
        pending: false,
        user: null
    })),
    on(AuthActions.logout, (state) => initialAuthState)
);

export const selectAuthState = createFeatureSelector<State<any>, AuthState>(<any>'auth');

export const selectAuthLoading = createSelector(selectAuthState, (state) => state.pending);
export const selectAuthError = createSelector(selectAuthState, (state) => state.error);
export const selectUser = createSelector(selectAuthState, (state) => state.user);
export const selectLoggedIn = createSelector(selectUser, (user) => !!user);