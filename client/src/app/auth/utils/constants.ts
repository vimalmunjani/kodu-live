export const BASE_AUTH_URL = `api/auth`;

export const SIGN_UP_URL = `${BASE_AUTH_URL}/signup`;
export const SIGN_IN_URL = `${BASE_AUTH_URL}/signin`;
export const VALIDATE_TOKEN_URL = `${BASE_AUTH_URL}/validate-token`;
export const GOOGLE_AUTH = `${BASE_AUTH_URL}/oauth/google`;
export const FACEBOOK_AUTH = `${BASE_AUTH_URL}/oauth/facebook`;
export const RESET_PASSWORD = `${BASE_AUTH_URL}/password/reset`;
export const UPDATE_PASSWORD = `${BASE_AUTH_URL}/password/update`;
export const EMAIL_PATTERN = `^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$`;
