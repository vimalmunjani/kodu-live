import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AuthRoutingModule } from './auth-routing.module';
import { MaterialModule } from '../material';

import { SignInComponent, SignUpComponent, AuthBaseComponent, ToolbarComponent, ResetComponent } from './components';

import { AuthEffects } from './effects';
import { authReducer } from './reducers';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services';

import {
  SocialLoginModule,
  SocialAuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';

export const COMPONENTS = [
  SignInComponent,
  SignUpComponent,
  ToolbarComponent,
  AuthBaseComponent,
  ResetComponent
];

@NgModule({
  imports: [
    CommonModule,
    SocialLoginModule,
    HttpClientModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    MaterialModule,
    StoreModule.forFeature('auth', authReducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
  providers: [
    AuthService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '582096045453-536rkuuind2g3si916ltdtglkde0bh3g.apps.googleusercontent.com'
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('324635705390398'),
          },
        ],
      } as SocialAuthServiceConfig,
    }
  ],
  declarations: COMPONENTS,
})
export class AuthModule { }
