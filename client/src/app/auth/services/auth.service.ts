import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICredentials } from '../models/credentials.model';
import { Constants } from '../utils';
import { SocialAuthService, GoogleLoginProvider } from "angularx-social-login";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _http: HttpClient, private _socialAuthService: SocialAuthService) { }

  signUp(credentials: ICredentials) {
    return this._http.post(Constants.SIGN_UP_URL, {
      email: credentials.email,
      password: credentials.password,
    });
  }

  signIn(credentials: ICredentials) {
    return this._http.post(Constants.SIGN_IN_URL, {
      email: credentials.email,
      password: credentials.password,
    });
  }

  validateToken(token: string) {
    return this._http.post(Constants.VALIDATE_TOKEN_URL, { token });
  }

  signInWithGoogle(accessToken: string) {
    return this._http.post(Constants.GOOGLE_AUTH, {
      'access_token': accessToken
    });
  }

  signInWithFacebook(accessToken: string) {
    return this._http.post(Constants.FACEBOOK_AUTH, {
      'access_token': accessToken
    });
  }

  resetPassword(email: string) {
    return this._http.post(Constants.RESET_PASSWORD, {
      email: email,
    });
  }

  updatePassword(credentials: ICredentials) {
    return this._http.post(Constants.UPDATE_PASSWORD, {
      email: credentials.email,
      password: credentials.password,
    });
  }

}
