import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { AuthService } from '../services';
import { map, catchError, exhaustMap, tap, switchMap } from 'rxjs/operators';
import { AuthActions } from '../actions';
import { Credentials } from '../models';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { SocialAuthService, GoogleLoginProvider } from 'angularx-social-login';
import { JwtService } from 'src/app/core/service/jwt.service';

@Injectable()
export class AuthEffects {

    constructor(
        private actions$: Actions,
        private _authService: AuthService,
        private _jwtService: JwtService,
        private _socialAuthService: SocialAuthService,
        private router: Router,
    ) { }

    signup$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.signUp),
            map((action) => action.credentials),
            exhaustMap((auth: Credentials) =>
                this._authService.signUp(auth).pipe(
                    map((user) => AuthActions.signUpSuccess({ user })),
                    catchError((error) => of(AuthActions.signUpFailure({ error })))
                )
            )
        )
    );

    signin$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.login),
            map((action) => action.credentials),
            exhaustMap((auth: Credentials) =>
                this._authService.signIn(auth).pipe(
                    map((user) => AuthActions.loginSuccess({ user })),
                    catchError((error) => of(AuthActions.loginFailure({ error })))
                )
            )
        )
    );

    validateToken$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.validateToken),
            map((action) => action.token),
            exhaustMap((token: string) =>
                this._authService.validateToken(token).pipe(
                    map((user) => AuthActions.loginSuccess({ user })),
                    catchError((error) => of(AuthActions.validateTokenFailure({ error })))
                )
            )
        )
    );


    postValidateTokenFailure$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.validateTokenFailure),
            map((action) => action.error),
            tap((user) => {
                this._jwtService.destroyToken();
                this.router.navigate(['auth', 'signin'], { queryParamsHandling: 'preserve' });
            })
        ), { dispatch: false });

    postAuthSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.signUpSuccess, AuthActions.loginSuccess),
            map((action) => action.user),
            tap((user) => {
                this._jwtService.saveToken(user.authToken);
                this.router.navigate(['user', 'profile'], { queryParamsHandling: 'preserve' });
            })
        ), { dispatch: false });

    logOut$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.logout),
            tap(() => {
                this._jwtService.destroyToken();
                this.router.navigate(['auth', 'signin'], { queryParamsHandling: 'preserve' });
            }),
        ), { dispatch: false });

    // Some confirmation logic to be implemented
    logOutConfirmation$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.logoutConfirmation),
            map(() => AuthActions.logout())
        )
    );

    // signInWithGoogle$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(AuthActions.signInGoogle),
    //         exhaustMap(() => {
    //             return of(this._socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID))
    //                 .map((auth) => {
    //                     return this._authService.signInWithGoogle(auth.idToken).pipe(
    //                         map((user) => AuthActions.loginSuccess({ user })),
    //                         catchError((error) => of(AuthActions.loginFailure({ error })))
    //                     )
    //                 })
    //         })
    //     )
    // );

}