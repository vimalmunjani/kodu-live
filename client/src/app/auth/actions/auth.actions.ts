import { createAction, props } from '@ngrx/store';
import { ICredentials } from '../models/credentials.model';

export const login = createAction(
    '[Auth] Login',
    props<{ credentials: ICredentials }>()
);

export const loginSuccess = createAction(
    '[Auth] Login Success',
    props<{ user: any }>()
);

export const loginFailure = createAction(
    '[Auth] Login Failure',
    props<{ error: any }>()
);

export const signUp = createAction(
    '[Auth] SignUp',
    props<{ credentials: ICredentials }>()
);

export const signUpSuccess = createAction(
    '[Auth] SignUp Success',
    props<{ user: any }>()
);

export const signUpFailure = createAction(
    '[Auth] SignUp Failure',
    props<{ error: any }>()
);

export const validateToken = createAction(
    '[Auth] Validate Token',
    props<{ token: string }>()
);

export const validateTokenFailure = createAction(
    '[Auth] Validate Token Failure',
    props<{ error: any }>()
);

export const logout = createAction('[Auth] Logout');

export const logoutConfirmation = createAction('[Auth] Logout Confirmation');

export const signInGoogle = createAction('[Auth] Google SignIn');
