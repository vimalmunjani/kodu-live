import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  FormControl
} from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { AuthActions } from '../../actions';
import { Credentials } from '../../models';
import { selectAuthLoading } from '../../reducers';
import { Observable, Subject, merge } from 'rxjs';
import { AuthService } from '../../services';
import { SocialAuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { ActivatedRoute, Router } from '@angular/router';
import { EMAIL_PATTERN } from '../../utils/constants';
import { JwtService } from 'src/app/core/service/jwt.service';

@Component({
  selector: 'kodu-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  public signInForm: FormGroup;
  public submitted: boolean;
  public loading$: Observable<boolean>;
  public selfLoading$: Subject<boolean>;
  public hidePassword: boolean;
  public emailField: FormControl;
  public passwordField: FormControl;

  constructor(
    private _formBuilder: FormBuilder,
    private _store$: Store<any>,
    private _socialAuthService: SocialAuthService,
    private _authService: AuthService,
    private _jwtService: JwtService,
    private _router: Router,
    private route: ActivatedRoute) {
    this.hidePassword = true;
    this.submitted = false;
    this.emailField = new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]);
    this.passwordField = new FormControl('', Validators.required);
    this.signInForm = this._formBuilder.group({
      email: this.emailField,
      password: this.passwordField
    });

    this.selfLoading$ = new Subject();
    this.loading$ = merge(this._store$.pipe(select(selectAuthLoading)), this.selfLoading$);

    this.autoLogin();

  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params: any) => {
      const password = params && params.password;
      const email = params && params.email;

      if (email && password) {
        const credentials = new Credentials(email, password);
        this._store$.dispatch(AuthActions.login({ credentials }));
      }
    });
  }

  public get controls(): { [key: string]: AbstractControl } {
    return this.signInForm.controls;
  }

  public autoLogin(): void {
    const token = this._jwtService.getToken();
    if (token) {
      this._store$.dispatch(AuthActions.validateToken({ token }));
    }
  }

  public onSubmit(): void {
    if (this.signInForm.invalid) {
      return;
    }
    const credentials = new Credentials(this.emailField.value, this.passwordField.value);
    this._store$.dispatch(AuthActions.login({ credentials }));
  }

  signInWithGoogle(): void {
    this.selfLoading$.next(true);
    this._socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((auth) => {
        console.log('G AUTH SUCCESS', auth);
        this._authService.signInWithGoogle(auth.idToken).subscribe((user: any) => {
          console.log('FROM SERVER', user);
          this.selfLoading$.next(false);
          this._router.navigate(['/']);
          this._store$.dispatch(AuthActions.loginSuccess({ user }));
        }, () => {
          this.selfLoading$.next(false);
        });
      })
      .catch((err) => {
        console.log('G AUTH ERROR', err);
        this.selfLoading$.next(false);
      });
  }

  signInWithFacebook(): void {
    this.selfLoading$.next(true);
    this._socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID)
      .then((auth) => {
        console.log('FB AUTH SUCCESS', auth);
        this._authService.signInWithFacebook(auth.authToken).subscribe((user: any) => {
          console.log('FROM SERVER', user);
          this.selfLoading$.next(false);
          this._router.navigate(['/']);
          this._store$.dispatch(AuthActions.loginSuccess({ user }));
        }, () => {
          this.selfLoading$.next(false);
        });
      })
      .catch((err) => {
        console.log('FB AUTH ERROR', err);
        this.selfLoading$.next(false);
      });
  }

}
