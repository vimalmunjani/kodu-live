import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kodu-auth-base',
  templateUrl: './auth-base.component.html',
  styleUrls: ['./auth-base.component.scss']
})
export class AuthBaseComponent implements OnInit {

  links: Array<string> = ['English', 'Francais', 'Bahasa', 'Indonesia', 'Espanol', 'Portugue', 'Deutsh'];


  constructor() { }

  ngOnInit(): void {
  }

}
