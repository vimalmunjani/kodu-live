import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { selectLoggedIn } from '../../../auth/reducers';
import { AuthActions } from 'src/app/auth/actions';

@Component({
  selector: 'kodu-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  public isHandset$: Observable<boolean>;
  public loggedIn$: Observable<boolean>;
  public navs: Array<any> = [{
    title: 'Home',
    link: 'home'
  }, {
    title: 'Features',
    link: 'features'
  }, {
    title: 'Plans & Pricing',
    link: 'pricing'
  }, {
    title: 'Contact',
    link: 'contact'
  }];

  constructor(private breakpointObserver: BreakpointObserver, private _store$: Store<any>) {
    this.isHandset$ = this.breakpointObserver.observe(Breakpoints.Handset)
      .pipe(
        map(result => result.matches)
      );
    // this.loggedIn$ = of(false);
    this.loggedIn$ = this._store$.pipe(select(selectLoggedIn));
  }

  ngOnInit(): void {
    const logo_toolbar = document.getElementById('logo_toolbar') as HTMLVideoElement;
    if (logo_toolbar) {
      logo_toolbar.oncanplaythrough = () => {
        logo_toolbar.muted = true;
        logo_toolbar.play();
      };
    }
  }

  logOut(): void {
    this._store$.dispatch(AuthActions.logoutConfirmation());
  }

}
