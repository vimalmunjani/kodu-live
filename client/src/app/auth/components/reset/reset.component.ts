import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SnackService } from '../../../core/service/snack.service';
import { AuthService } from '../../services';
import { EMAIL_PATTERN } from '../../utils/constants';

@Component({
  selector: 'kodu-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent {

  public loading: boolean;
  public resetForm: FormGroup;
  public email: FormControl;

  constructor(private authService: AuthService,
    private snackService: SnackService,
    private router: Router) {
    this.loading = false;
    this.email = new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]);
    this.resetForm = new FormGroup({
      email: this.email
    })
  }

  onSubmit() {
    this.loading = true;
    this.authService.resetPassword(this.email.value)
      .subscribe((res) => {
        this.snackService.success('Password Reset Email Sent Successfully');
        setTimeout(() => {
          this.loading = false
          this.router.navigate(['auth', 'signin']);
        }, 2000);
      }, ({ error }) => {
        this.snackService.success(error);
        this.loading = false
      });
  }

}
