import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  FormControl,
  ValidationErrors
} from '@angular/forms';
import { Credentials } from '../../models';
import { AuthActions } from '../../actions';
import { Store, select } from '@ngrx/store';
import { debounceTime } from 'rxjs/operators';
import { Observable, Subject, merge } from 'rxjs';
import { selectAuthLoading } from '../../reducers';
import { AuthService } from '../../services';
import { Router } from '@angular/router';
import { SocialAuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { confirmPasswordValidator } from '../../utils/confirm-password.validator';
import { EMAIL_PATTERN } from '../../utils/constants';

@Component({
  selector: 'kodu-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  public signUpForm: FormGroup;
  public submitted: boolean;
  public loading$: Observable<boolean>;
  public selfLoading$: Subject<boolean>;
  public hidePassword: boolean;
  public emailField: FormControl;
  public confirmPasswordField: FormControl;
  public passwordField: FormControl;

  constructor(private _formBuilder: FormBuilder,
    private _store$: Store<any>,
    private _socialAuthService: SocialAuthService,
    private _authService: AuthService,
    private _router: Router) {
    this.hidePassword = true;
    this.submitted = false;
    this.emailField = new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]);
    this.passwordField = new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(16),
    ]));
    this.confirmPasswordField = new FormControl('', Validators.required);
    this.signUpForm = this._formBuilder.group({
      email: this.emailField,
      password: this.passwordField,
      confirmPassword: this.confirmPasswordField,
    }, {
      validator: confirmPasswordValidator('password', 'confirmPassword')
    });

    this.selfLoading$ = new Subject();
    this.loading$ = merge(this._store$.pipe(select(selectAuthLoading)), this.selfLoading$);

  }

  ngOnInit(): void { }

  public get controls(): { [key: string]: AbstractControl } {
    return this.signUpForm.controls;
  }

  public onSubmit(): void {
    if (this.signUpForm.invalid) {
      return;
    }
    const credentials = new Credentials(this.emailField.value, this.passwordField.value);
    this._store$.dispatch(AuthActions.signUp({ credentials }));
  }

  signInWithGoogle(): void {
    this.selfLoading$.next(true);
    this._socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((auth) => {
        console.log('G AUTH SUCCESS', auth);
        this._authService.signInWithGoogle(auth.idToken).subscribe((user: any) => {
          this.selfLoading$.next(false);
          console.log('FROM SERVER', user);
          this._router.navigate(['/']);
          this._store$.dispatch(AuthActions.loginSuccess({ user }));
        }, () => {
          this.selfLoading$.next(false);
        });
      })
      .catch((err) => {
        this.selfLoading$.next(false);
        console.log('G AUTH ERROR', err);
      });
  }

  signInWithFacebook(): void {
    this.selfLoading$.next(true);
    this._socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID)
      .then((auth) => {
        console.log('FB AUTH SUCCESS', auth);
        this._authService.signInWithFacebook(auth.authToken).subscribe((user: any) => {
          console.log('FROM SERVER', user);
          this.selfLoading$.next(false);
          this._router.navigate(['/']);
          this._store$.dispatch(AuthActions.loginSuccess({ user }));
        }, () => {
          this.selfLoading$.next(false);
        });
      })
      .catch((err) => {
        this.selfLoading$.next(false);
        console.log('FB AUTH ERROR', err);
      });
  }

}
