import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent, SignUpComponent, AuthBaseComponent, ResetComponent } from './components';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthBaseComponent,
    children: [
      {
        path: 'signin',
        component: SignInComponent
      },
      {
        path: 'signup',
        component: SignUpComponent
      },
      {
        path: 'reset',
        component: ResetComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'signin'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
