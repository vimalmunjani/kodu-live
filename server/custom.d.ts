declare namespace NodeJS {
    interface ProcessEnv {
        NODE_ENV: 'development' | 'production';
        PORT: string;
        JWT_SECRET: string;
        JWT_COOKIE_SECRET: string;
        G_CLIENT_ID: string;
        G_CLIENT_SECRET: string;
        FB_APP_ID: string;
        FB_APP_SECRET: string
        DISABLE_AUTH_COOKIE: string;
        MEETING_JWT_APP_SECRET: string;
        MEETING_JWT_APP_ID: string;
        MEETING_SUBJECT: string;
    }

}

declare module 'passport-google-plus-token';
declare module 'googleapis';