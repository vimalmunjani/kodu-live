import express from 'express';
import { createGuestQuery } from '../controllers/guest-quest.controller';

const router = express.Router();
router.post('/query', createGuestQuery);

export { router as guestRouter };
