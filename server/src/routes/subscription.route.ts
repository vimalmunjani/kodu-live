import express from 'express';

import { createSubscription, getSubscriptionDetails, cancelSubscription, reactivateSubscription, resetSubscription } from '../controllers';
import { validateAuthToken } from '../middlewares';

const router = express.Router();
router.get('/create/:id', validateAuthToken, createSubscription)
    .get('/detail/:id', validateAuthToken, getSubscriptionDetails)
    .get('/cancel/:id', validateAuthToken, cancelSubscription)
    .get('/reset/:id', validateAuthToken, resetSubscription)
    .get('/reactivate/:id', validateAuthToken, reactivateSubscription);

export { router as subscriptionRouter };
