import express from 'express';
import { createFaqCategory, getFAQs } from '../controllers/faq-category.controller';
import { createFaq } from '../controllers/faq.controller';

const router = express.Router();
router.post('/faq-category', createFaqCategory);
router.post('/faq', createFaq);
router.get('/faqs', getFAQs);

export { router as faqRouter };
