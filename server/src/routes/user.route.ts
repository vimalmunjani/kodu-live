import express from 'express';

import { validateAuthToken, extractAvatar } from '../middlewares';
import {
    getProfile,
    createMeeting,
    getMeetingsByUser,
    updateAvatar,
    updateProfile,
    markUserAsRegistered,
    getRegisteredUsers,
    joinMeeting,
    updateMeeting,
    cancelMeeting
} from '../controllers';

const router = express.Router();
router.get('/profile', validateAuthToken, getProfile);
router.put('/profile', validateAuthToken, updateProfile);
router.get('/meetings', validateAuthToken, getMeetingsByUser);
router.post('/meeting', validateAuthToken, createMeeting);
router.put('/meeting', validateAuthToken, updateMeeting);
router.put('/cancel-meeting', validateAuthToken, cancelMeeting);
router.post('/join-meeting', validateAuthToken, joinMeeting);
router.post('/meeting-mark-as-register', markUserAsRegistered);
router.get('/meeting-register', getRegisteredUsers);
router.post('/avatar', validateAuthToken, extractAvatar, updateAvatar);

export { router as userRouter };
