import express from 'express';

const router = express.Router();
router.post('/paypal', (req, res) => {
    console.log('paypal webhook', req.body);
    return res.status(200).send();
});

export { router as webhooksRouter };
