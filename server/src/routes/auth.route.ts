import express from 'express';
import passport from 'passport';

import { signin, signup, googleAuth, facebookAuth, loginWithToken, resetPassword, updatePassword } from '../controllers';
import { authCredentialsValidator, validateGoogleAuthToken, validateAuthToken } from '../middlewares';
const PF = require('../middlewares/facebook-auth.middleware');

const router = express.Router();
router.post('/signin', signin)
    .post('/validate-token', validateAuthToken, loginWithToken)
    .post('/signup', authCredentialsValidator, signup)
    .post('/oauth/google', validateGoogleAuthToken, googleAuth)
    .post('/oauth/facebook', passport.authenticate('facebook', { session: false }), facebookAuth)
    .post('/password/reset', resetPassword)
    .post('/password/update', validateAuthToken, authCredentialsValidator, updatePassword);

export { router as authRouter };
