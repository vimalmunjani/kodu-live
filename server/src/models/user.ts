// import mongoose, { Schema } from 'mongoose';

export interface IUser {
    id?: any;
    email: string;
    password: string;
}

export interface ISocialUser {
    id?: any;
    email: string;
    name: string;
    picture: string;
}


