export enum AuthStrategy {
    LOCAL,
    GOOGLE,
    FACEBOOK
}