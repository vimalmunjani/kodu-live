import { IUser } from "./user";


export interface IProfile {
    id: number;
    name: string;
    contact: string;
    address: string;
    photoUrl: string;
    user?: IUser;
}


