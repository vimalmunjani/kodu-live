export interface IPlan {
    id: string;
    name: string;
    description: string;
    features: string[];
    price: {
        monthly: number;
        quarterly: number;
        annually: number;
    }
}