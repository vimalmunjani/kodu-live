import { FaqCategory } from "../entity/FaqCategory";

export interface IFaq {
    id?: any;
    question: string;
    answer: string;
    faqCategory: FaqCategory;
}



