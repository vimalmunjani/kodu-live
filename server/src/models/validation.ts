export interface IValidation {
    error: boolean;
    message: string | null;
}

export class Validation implements IValidation {
    public error: boolean;
    public message: string | null;
    constructor(error: boolean, message: string | null) {
        this.error = error;
        this.message = message;
    }
}