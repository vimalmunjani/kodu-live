import { IUser } from "./user";


export interface IMeeting {
    id: number;
    title: string;
    startTime: string;
    endTime: string;
    meetingDate: Date;
    meetingCode?: string;
    organizer: IUser;
    guests?: Array<IUser>;
    guestEmailsString?: string;
    token?: string;
    cancelled?: boolean;
}


