
export interface IGuestQuery {
    id: number;
    message: string;
    email: string;
    name: string;
}
