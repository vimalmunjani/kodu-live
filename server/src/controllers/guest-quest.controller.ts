import { Request, Response } from "express";
import { GuestQueryService } from "../services";

export const createGuestQuery = async (request: Request, response: Response) => {

    const guestQuery = request.body.guestQuery;

    try {
        const newGuestQuery = await GuestQueryService.createGuestQuery(guestQuery);
        response.status(200).json(newGuestQuery);
    } catch (error) {
        return response.status(403).json('CREATE GUEST QUERY ERROR');
    }
}





