import { Request, Response } from "express";
import { FaqService } from "../services";

export const createFaq = async (request: Request, response: Response) => {

    const faq = request.body.faq;

    try {
        const newFaq = await FaqService.createFaq(faq);
        response.status(200).json(newFaq);
    } catch (error) {
        return response.status(403).json('CREATE FAQ ERROR');
    }
}


