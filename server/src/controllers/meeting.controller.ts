import { Request, Response } from "express";
import { MeetingService, AuthService, EmailService } from "../services";
import { MeetingUtils } from "../utils";
import { getInviteEmailTemplate } from "../utils/email/meeting-invite.tpl";
import { IMeeting } from "../models/meeting";
import { Meeting } from "../entity/Meeting";

export const getMeetingsByUser = async (request: Request, response: Response) => {
    try {
        const { email } = request.body.user;
        const pageIndex = Number(request.query.pageIndex);
        const pageSize = Number(request.query.pageSize);
        const user = await AuthService.findUser(email);
        const paginatedMeetings = await MeetingService.getMeetingsByUser(user, pageIndex, pageSize);
        response.status(200).json(paginatedMeetings);
    } catch (error) {
        return response.status(403).json('FETCH MEETINGS ERROR');
    }
}

export const createMeeting = async (request: Request, response: Response) => {

    const { title, meetingDate, startTime, endTime, guests } = request.body;

    try {
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        let options = {
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            month: "short",
            timeZoneName: "short",
            weekday: "short",
            year: "numeric"
        };
        const meeting = await MeetingService.createMeeting(user, title, meetingDate, startTime, endTime, guests);
        const payload = {
            user: {
                name: user.profile.name,
                email: user.email
            },
            room: meeting.meetingCode
        };
        const token = await MeetingUtils.getMeetingJWTToken(payload);
        let url = `https://kodulive.com`;
        let message = `You have been invited to meeting.`;
        guests.push(user.email);
        if (guests && guests.length) {
            const toPayload: Array<{ to: string, from: string, subject: string, text: string, html: string }> = [];
            await MeetingUtils.asyncForEach(guests, async (email: string) => {
                const isOrganizer = meeting?.organizer?.email === email;
                let token = null;
                if (isOrganizer) {
                    const payload = {
                        user: {
                            name: user?.profile?.name,
                            email: user?.email
                        },
                        room: meeting.meetingCode
                    };
                    token = await MeetingUtils.getMeetingJWTToken(payload);
                    const meetingTitleParam = meeting.title.split(' ').join('-');
                    url = `https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${token}#config.subject="${meetingTitleParam}"`;
                    message = `New Kodulive Meeting`
                } else {
                    let displayName = 'Fellow Kodu';
                    try {
                        const invitee = await AuthService.findUser(email);
                        displayName = invitee?.profile?.name;
                        if (displayName) {
                            displayName = displayName.split(' ')[0];
                        } const payload = {
                            user: {
                                name: invitee?.profile?.name,
                                email: invitee?.email
                            },
                            room: meeting.meetingCode
                        };
                        token = await MeetingUtils.getMeetingJWTToken(payload);
                        url = `https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${token}#userInfo.displayName="${displayName}"`;
                    } catch {
                        url = `https://meeting.kodulive.com/${meeting.meetingCode}#config.prejoinPageEnabled=true`;
                    }

                }
                toPayload.push({
                    to: email,
                    from: 'live.kodu@gmail.com',
                    subject: 'KoduLive Meeting Invitation',
                    text: message,
                    html: getInviteEmailTemplate(meeting, encodeURI(url), options, message)
                });
            })
            EmailService.sendEmailMultiple(toPayload);
        }
        meeting.token = token;
        response.status(200).json(meeting)
    } catch (error) {
        return response.status(403).json('CREATE MEETING ERROR');
    }
}

export const joinMeeting = async (request: Request, response: Response) => {

    const { meetingCode } = request.body;

    try {
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        const meeting = await MeetingService.findMeeting(meetingCode);
        if (meeting) {
            const payload = {
                user: {
                    name: user.profile.name,
                    email: user.email
                },
                room: meeting.meetingCode
            };
            const token = await MeetingUtils.getMeetingJWTToken(payload);
            meeting.token = token;
            response.status(200).json(meeting);
        } else {
            response.status(403).json('MEETING NOT FOUND');
        }

    } catch (error) {
        return response.status(403).json('MEETING NOT FOUND');
    }
}

export const updateMeeting = async (request: Request, response: Response) => {

    const { id, title, meetingDate, startTime, endTime, guests } = request.body;

    try {
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        let options = {
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            month: "short",
            timeZoneName: "short",
            weekday: "short",
            year: "numeric"
        };
        let meetingData = await MeetingService.findMeetingById(id);
        let meeting = { ...meetingData, title, meetingDate, startTime, endTime, guests };
        if (meetingData) {
            if (meetingData.organizer && meetingData.organizer.email === user.email) {
                const updatedMeeting = await MeetingService.updateMeeting(meeting);
                response.status(200).json(updatedMeeting);
            }


            const payload = {
                user: {
                    name: user.profile.name,
                    email: user.email
                },
                room: meeting.meetingCode
            };
            const token = await MeetingUtils.getMeetingJWTToken(payload);
            let url = `https://kodulive.com`;
            let message = `Update: You have been invited to meeting.`;
            guests.push(user.email);
            if (guests && guests.length) {
                const toPayload: Array<{ to: string, from: string, subject: string, text: string, html: string }> = [];
                await MeetingUtils.asyncForEach(guests, async (email: string) => {
                    const isOrganizer = meeting?.organizer?.email === email;
                    let token = null;
                    if (isOrganizer) {
                        const payload = {
                            user: {
                                name: user?.profile?.name,
                                email: user?.email
                            },
                            room: meeting.meetingCode
                        };
                        token = await MeetingUtils.getMeetingJWTToken(payload);
                        const meetingTitleParam = meeting.title.split(' ').join('-');
                        url = `https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${token}#config.subject="${meetingTitleParam}"`;
                        message = `Update: Kodulive Meeting`;
                    } else {
                        let displayName = 'Fellow Kodu';
                        try {
                            const invitee = await AuthService.findUser(email);
                            displayName = invitee?.profile?.name;
                            if (displayName) {
                                displayName = displayName.split(' ')[0];
                            } const payload = {
                                user: {
                                    name: invitee?.profile?.name,
                                    email: invitee?.email
                                },
                                room: meeting.meetingCode
                            };
                            token = await MeetingUtils.getMeetingJWTToken(payload);
                            url = `https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${token}#userInfo.displayName="${displayName}"`;
                        } catch {
                            url = `https://meeting.kodulive.com/${meeting.meetingCode}#config.prejoinPageEnabled=true`;
                        }

                    }
                    toPayload.push({
                        to: email,
                        from: 'live.kodu@gmail.com',
                        subject: 'Update: KoduLive Meeting Invitation',
                        text: message,
                        html: getInviteEmailTemplate(meeting, encodeURI(url), options, message)
                    });
                })
                EmailService.sendEmailMultiple(toPayload);
            }
            meeting.token = token;
            response.status(200).json(meeting)
        } else {
            return response.status(403).json('UPDATE MEETING ERROR');
        }
    } catch (error) {
        return response.status(403).json('UPDATE MEETING ERROR');
    }
}

export const cancelMeeting = async (request: Request, response: Response) => {

    const { id } = request.body;

    try {
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        let meeting = await MeetingService.findMeetingById(id);
        if (meeting) {
            if (meeting.organizer && meeting.organizer.email === user.email) {
                meeting = { ...meeting, cancelled: true };
                const updatedMeeting = await MeetingService.updateMeeting(meeting);
                response.status(200).json(updatedMeeting);
            } else {
                response.status(403).json('UNAUTHORIZED UPDATE');
            }
        } else {
            response.status(403).json('MEETING NOT FOUND');
        }

    } catch (error) {
        return response.status(403).json('MEETING NOT FOUND');
    }
}
