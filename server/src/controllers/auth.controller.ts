import { Request, Response, NextFunction } from "express";

import { AuthService, EmailService, ProfileService } from "../services";
import { AuthUtils } from "../utils";
import { getEmailTemplate } from "../utils/email/password-reset.tpl";

/**
 * SIGN IN
 */
export const signin = async (request: Request, response: Response) => {

    const { email, password } = request.body;
    const { error, message } = AuthUtils.validateEmail(email);
    if (error) {
        return response.status(403).json({ message });
    }

    try {
        const user = await AuthService.findUser(email);
        await AuthUtils.matchPassword(password, user.password);
        const [authToken, authCookieToken] = await Promise.all([AuthUtils.signJWTToken({ email: user.email }), AuthUtils.signJWTCookieToken({ email: user.email })])
        if (!process.env.DISABLE_AUTH_COOKIE) {
            response.cookie('authToken', authCookieToken, {
                httpOnly: true,
            });
        }
        return response.status(200).json({ authToken });
    } catch (error) {
        return response.status(403).json({ message: `Invalid Email/password` });
    }

}

export const loginWithToken = async (request: Request, response: Response) => {

    try {
        const user = request.body.user;
        const [authToken, authCookieToken] = await Promise.all([AuthUtils.signJWTToken({ email: user.email }), AuthUtils.signJWTCookieToken({ email: user.email })])
        if (!process.env.DISABLE_AUTH_COOKIE) {
            response.cookie('authToken', authCookieToken, {
                httpOnly: true,
            });
        }
        return response.status(200).json({ authToken });
    } catch (error) {
        return response.status(403).json({ message: `Relogin required` });
    }

}

/**
 * SIGN UP
 */
export const signup = async (request: Request, response: Response) => {

    const { email, password } = request.body;

    const ifUserExists: boolean = await AuthService.ifUserExists(email);
    if (ifUserExists) {
        return response.status(403).json({ message: `Email already in use` });
    }

    try {
        const user = await AuthService.createUser(email, password);
        const basicSubscription = await ProfileService.createSubscription();
        if (basicSubscription) {
            const profile = await ProfileService.createProfileByLocalStrategy({ email, password }, user, basicSubscription);
            const [authToken, authCookieToken] = await Promise.all([AuthUtils.signJWTToken({ email: user.email }), AuthUtils.signJWTCookieToken({ email: user.email })]);
            if (!process.env.DISABLE_AUTH_COOKIE) {
                response.cookie('authToken', authCookieToken, {
                    httpOnly: true,
                });
            }
            return response.status(200).json({ authToken });
        } else {
            throw new Error('Subscription failed');
        }

    } catch (error) {
        return response.status(403).json();
    }

}

/**
 * GOOGLE AUTH
 */
export const googleAuth = async (request: Request, response: Response) => {
    try {
        const { email } = request.body.user;
        const hashPassword = await AuthUtils.hashPassword(email);
        const user = await AuthService.findOrCreateUser(email, hashPassword);
        const basicSubscription = await ProfileService.createSubscription();
        if (basicSubscription) {
            const profile = await ProfileService.createProfileByGoogleStrategy(request.body.user, user, basicSubscription);
            const [authToken, authCookieToken] = await Promise.all([AuthUtils.signJWTToken({ email: user.email }), AuthUtils.signJWTCookieToken({ email: user.email })]);
            if (!process.env.DISABLE_AUTH_COOKIE) {
                response.cookie('authToken', authCookieToken, {
                    httpOnly: true,
                });
            }
            return response.status(200).json({ authToken });
        } else {
            throw new Error('Subscription failed');
        }

    } catch (error) {
        return response.status(403).json('GOOGLE AUTH ERROR');
    }
}

/**
 * FACEBOOK AUTH
 */
export const facebookAuth = async (request: Request, response: Response) => {
    try {
        const { email } = <any>request.user;
        const hashPassword = await AuthUtils.hashPassword(email);
        const user = await AuthService.findOrCreateUser(email, hashPassword);
        const basicSubscription = await ProfileService.createSubscription();
        if (basicSubscription) {
            const profile = await ProfileService.createProfileByFacebookStrategy(<any>request.user, user, basicSubscription);
            const [authToken, authCookieToken] = await Promise.all([AuthUtils.signJWTToken({ email: user.email }), AuthUtils.signJWTCookieToken({ email: user.email })]);
            if (!process.env.DISABLE_AUTH_COOKIE) {
                response.cookie('authToken', authCookieToken, {
                    httpOnly: true,
                });
            }
            return response.status(200).json({ authToken });
        } else {
            throw new Error('Subscription failed');
        }

    } catch (error) {
        return response.status(403).json('FB AUTH ERROR');
    }
}

/**
 * RESET PASSWORD
 */
export const resetPassword = async (request: Request, response: Response) => {
    try {
        const { email } = request.body;
        if (!email) {
            throw new Error("Error resetting password");
        }
        try {
            const user = await AuthService.findUser(email);
            const password = AuthUtils.generateRandomString(10);
            const hashPassword = await AuthUtils.hashPassword(password);
            const updatedUser = AuthService.updateUser({ ...user, password: hashPassword });
            EmailService.sendEmail(email, 'KoduLive: Password Reset', `KoduLive new password: ${password}`, getEmailTemplate(password, email));
            return response.status(200).json('OK');
        } catch (error) {
            return response.status(404).json('No user found with this email');
        }
    } catch (error) {
        return response.status(400).json('Error resetting password');
    }
}

/**
 * UPDATE PASSWORD
 */
export const updatePassword = async (request: Request, response: Response) => {
    try {
        const { email } = <any>request.user
        const { password } = request.body;
        const user = await AuthService.findUser(email);
        const updatedUser = AuthService.updateUser({ ...user, password });
        return response.status(200).json('OK');
    } catch (error) {
        return response.status(400).json({ message: `Error updating Password` });
    }
}
