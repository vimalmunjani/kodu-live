import { Request, Response, NextFunction } from "express";
import { ProfileService, AuthService } from "../services";

export const getProfile = async (request: Request, response: Response, next: NextFunction) => {
    try {
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        const profile = await ProfileService.getProfile(user);
        response.status(200).json({ ...profile, email: user?.email })
    } catch (error) {
        return response.status(403).json('FETCH PROFILE ERROR');
    }

}

export const updateProfile = async (request: Request, response: Response, next: NextFunction) => {

    try {
        const { email } = request.body.user
        const url = request.protocol + "://" + request.get("host");
        const user = await AuthService.findUser(email);
        const oldProfile = await ProfileService.getProfile(user);
        const newProfile = await ProfileService.update({ ...oldProfile, ...request.body.profile });
        response.status(200).json({ ...newProfile, email: user?.email })
    } catch (error) {
        return response.status(403).json('UPADTE PROFILE ERROR');
    }

}

// export const updatePlan = async (request: Request, response: Response, next: NextFunction) => {

//     try {
//         const token = 'A21AAEonDqCkaE4WZLJhvHC7SIRiEZfRXvCPO2TL4KeKEnnPcqD2AS6kT3OnMYThxbodpIQSRDD739JPSpB8S74ofEEsFP8ZA';
//         const { email } = request.body.user;
//         // const { subscriptionId } = request.body;
//         const subscriptionId = 'I-M0203AHHTSD9';
//         const user = await AuthService.findUser(email);
//         const x = await axios.get(`https://api.sandbox.paypal.com/v1/billing/subscriptions/${subscriptionId}`, {
//             headers: { Authorization: `Bearer ${token}` }
//         });
//         console.log('SUBSCRIPTION', x.data);
//         const oldProfile = await ProfileService.getProfile(user);
//         const newProfile = await ProfileService.update({ ...oldProfile, ...request.body.subscriptionId });
//         response.status(200).json({ ...newProfile, email: user?.email })
//     } catch (error) {
//         return response.status(403).json('UPADTE PROFILE ERROR');
//     }

// }

export const updateAvatar = async (request: Request, response: Response, next: NextFunction) => {

    try {
        const { email } = <any>request.user;
        const url = `${request.protocol}://${request.get("host")}/images/${request.file?.filename}`;
        const user = await AuthService.findUser(email);
        const oldProfile = await ProfileService.getProfile(user);
        if (oldProfile) {
            oldProfile.photoUrl = url;
            const newProfile = await ProfileService.update(oldProfile);
            return response.status(200).json({ ...newProfile, email: user?.email })
        } else {
            throw 'UPADTE PROFILE ERROR';
        }

    } catch (error) {
        return response.status(403).json(request);
    }

}
