import { Request, Response, NextFunction } from "express";
import { AuthService } from "../services";

export const getUser = async (request: Request, response: Response, next: NextFunction) => {
    const { email } = request.body.user
    response.status(200).json('OK');
}

export const getRegisteredUsers = async (request: Request, response: Response, next: NextFunction) => {
    try {
        const isRegistered = !!request.params.isRegistered;
        const users = await AuthService.getRegisteredUsers(isRegistered);
        response.status(200).json(users);
    } catch (error) {
        return response.status(403).json('FETCH REGISTERED USERS ERROR');
    }

}

export const markUserAsRegistered = async (request: Request, response: Response, next: NextFunction) => {

    const markUserAsRegistered = request.body.markUserAsRegistered;
    let emails = request.body.emails;
    const responseMap: any = {};

    for (const email of emails) {
        try {
            const user = await AuthService.markUserAsRegistered(email, markUserAsRegistered);
            responseMap[email] = true;
        } catch{
            responseMap[email] = false;
        }
    }

    response.status(200).json(responseMap);


}