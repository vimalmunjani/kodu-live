import { Request, Response } from "express";
import { FaqCategoryService, FaqService } from "../services";

export const createFaqCategory = async (request: Request, response: Response) => {
    const faqCategory = request.body.faqCategory;
    try {
        const newFaqCategory = await FaqCategoryService.createFaqCategory(faqCategory);
        response.status(200).json(newFaqCategory);
    } catch (error) {
        return response.status(403).json('CREATE FAQ ERROR');
    }
}

export const getFAQs = async (request: Request, response: Response) => {
    try {
        const pageIndex = Number(request.query.pageIndex);
        const pageSize = Number(request.query.pageSize);
        const paginatedFAQs = await FaqService.getFAQs(pageIndex, pageSize);
        response.status(200).json(paginatedFAQs);
    } catch (error) {
        return response.status(403).json('FETCH MEETINGS ERROR');
    }
}





