import { Request, Response, NextFunction } from "express";

import { PlanService, PaypalService, AuthService, ProfileService, SubscriptionService } from "../services";
import { Subscription } from "../entity/Subscription";
import { Profile } from "../entity/Profile";

export const getPlans = async (request: Request, response: Response) => {
    try {
        const plans = await PlanService.getAllPlans();
        return response.status(200).json({ plans });
    } catch (error) {
        return response.status(404).json();
    }
}

export const getPlanById = async (request: Request, response: Response) => {
    try {
        const planId = request.params.id
        const plan = await PlanService.getPlanById(planId);
        return response.status(200).json(plan);
    } catch (error) {
        return response.status(404).json({ error });
    }
}

export const createPlan = async (request: Request, response: Response) => {
    try {
        const { plan } = request.body;
        const createdPlan = await PlanService.createPlan(plan);
        return response.status(201).json({ plan: createdPlan });
    } catch (error) {
        return response.status(404).json();
    }
}

export const updatePlan = async (request: Request, response: Response) => {
    try {
        const planId = request.params.id;
        const { plan } = request.body;
        const updatedPlan = await PlanService.updatePlanById(planId, plan);
        return response.status(201).json({ plan: updatedPlan });
    } catch (error) {
        return response.status(404).json();
    }
}

export const createSubscription = async (request: Request, response: Response) => {
    try {
        const subscriptionId = request.params.id;
        const updatedSubscription = await PaypalService.createSubscription(subscriptionId);
        const { id, agreement_details, start_date, state, description } = updatedSubscription;
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        const profile: Profile = await ProfileService.getProfile(user) as any;
        const oldSubscription = profile.subscription;
        const newSubscription: any = {
            subscriptionId: id,
            nextBillingDate: agreement_details.next_billing_date,
            planType: 'Pro',
            state: state,
            tenureType: (<string>description).includes('M2') ? 'Annually' : 'Monthly',
            startDate: start_date
        }
        const subscription = await SubscriptionService.update({ ...oldSubscription, ...newSubscription });
        return response.status(200).json(subscription);
    } catch (error) {
        return response.status(400).json(error);
    }
}

export const getSubscriptionDetails = async (request: Request, response: Response) => {
    try {
        const subscriptionId = request.params.id;
        const updatedSubscription = await PaypalService.createSubscription(subscriptionId);
        return response.status(200).json(updatedSubscription);
    } catch (error) {
        return response.status(400).json(error);
    }
}

export const resetSubscription = async (request: Request, response: Response) => {
    try {
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        const profile: Profile = await ProfileService.getProfile(user) as any;
        const oldSubscription = profile.subscription;
        const newSubscription: any = {
            id: oldSubscription.id,
            subscriptionId: null,
            nextBillingDate: null,
            planType: 'Basic',
            state: null,
            tenureType: null,
            startDate: null
        }
        const subscription = await SubscriptionService.update(newSubscription);
        return response.status(200).json(subscription);
    } catch (error) {
        return response.status(400).json(error);
    }
}

export const cancelSubscription = async (request: Request, response: Response) => {
    try {
        const subscriptionId = request.params.id;
        const updatedSubscription = await PaypalService.cancelSubscription(subscriptionId);
        const { id, agreement_details, start_date, state, description } = updatedSubscription;
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        const profile: Profile = await ProfileService.getProfile(user) as any;
        const oldSubscription = profile.subscription;
        const newSubscription: any = {
            subscriptionId: id,
            nextBillingDate: agreement_details.next_billing_date,
            planType: 'Pro',
            state: state,
            tenureType: (<string>description).includes('M2') ? 'Annually' : 'Monthly',
            startDate: start_date
        }
        const subscription = await SubscriptionService.update({ ...oldSubscription, ...newSubscription });
        return response.status(200).json(subscription);
    } catch (error) {
        return response.status(400).json(error);
    }
}

export const reactivateSubscription = async (request: Request, response: Response) => {
    try {
        const subscriptionId = request.params.id;
        const updatedSubscription = await PaypalService.reactivateSubscription(subscriptionId);
        const { id, agreement_details, start_date, state, description } = updatedSubscription;
        const { email } = request.body.user
        const user = await AuthService.findUser(email);
        const profile: Profile = await ProfileService.getProfile(user) as any;
        const oldSubscription = profile.subscription;
        const newSubscription: any = {
            subscriptionId: id,
            nextBillingDate: agreement_details.next_billing_date,
            planType: 'Pro',
            state: state,
            tenureType: (<string>description).includes('M2') ? 'Annually' : 'Monthly',
            startDate: start_date
        }
        const subscription = await SubscriptionService.update({ ...oldSubscription, ...newSubscription });
        return response.status(200).json(subscription);
    } catch (error) {
        return response.status(400).json(error);
    }
}
