import validator from 'validator';
import bcrypt from 'bcrypt';
import JWT from 'jsonwebtoken';

import { IValidation, Validation, IUser } from '../models';

export const validateEmail = (email: string): IValidation => {
    if (!email || validator.isEmpty(email)) {
        return new Validation(true, `Please provide email`);
    } else if (!validator.isEmail(email)) {
        return new Validation(true, `Please provide a valid email`);
    } else {
        return new Validation(false, null);
    }
}

export const validateAuthCredentials = (email: string, password: string): IValidation => {
    if (!email || !password) {
        return new Validation(true, `Please provide email/password`);
    } else if (validator.isEmpty(email) || validator.isEmpty(password)) {
        return new Validation(true, `Please provide email/password`);
    } else if (!validator.isEmail(email)) {
        return new Validation(true, `Please provide a valid email`);
    } else if (!validator.isLength(password, { min: 8, max: 16 })) {
        return new Validation(true, `Password should be between 8 to 16 characters`);
    } else {
        return new Validation(false, null);
    }
}

export const hashPassword = async (password: string): Promise<string> => {
    try {
        const hash = await bcrypt.hash(password, 10);
        return hash;
    } catch (error) {
        throw new Error("Unable to hash password");
    }
}

export const matchPassword = async (password: string, passwordHash: string): Promise<boolean> => {
    try {
        const isMatch = await bcrypt.compare(password, passwordHash);
        if (!isMatch) {
            throw new Error("passwords do not match");
        }
        return isMatch;
    } catch (error) {
        throw new Error("Unable to match password");
    }
}

export const signJWTToken = async (payload: Partial<IUser>): Promise<string> => {
    return new Promise((resolve, reject) => {
        JWT.sign(payload, process.env.JWT_SECRET, {
            expiresIn: '1d'
        }, (error: any, token: any) => {
            if (error) {
                reject(`Unable to sign JWT Token`);
            }
            resolve(token);
        });
    });
}

export const signJWTCookieToken = async (payload: Partial<IUser>): Promise<string | null> => {
    return new Promise((resolve, reject) => {
        if (process.env.DISABLE_AUTH_COOKIE) {
            resolve(null);
        }
        JWT.sign(payload, process.env.JWT_COOKIE_SECRET, {
            expiresIn: '1d'
        }, (error, token) => {
            if (error) {
                reject(`Unable to sign JWT Token`);
            }
            resolve(token);
        });
    });
}

export const verifyJWTToken = async (token: string): Promise<any> => {
    return new Promise((resolve, reject) => {
        JWT.verify(token, process.env.JWT_SECRET, {
        }, (error, payload) => {
            if (error) {
                reject(error);
            }
            resolve(payload);
        });
    });
}

export const generateRandomString = (length: number): string => {
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (var i = length; i > 0; --i) {
        result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
}
