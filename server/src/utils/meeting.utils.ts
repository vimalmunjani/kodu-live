import JWT from 'jsonwebtoken';

export const getMeetingJWTToken = async (payload: { user: { name: string, email: string }, room: string | undefined }): Promise<string> => {
    return new Promise((resolve, reject) => {
        JWT.sign(payload, process.env.MEETING_JWT_APP_SECRET, {
            algorithm: "HS256",
            audience: process.env.MEETING_JWT_APP_ID,
            subject: process.env.MEETING_SUBJECT,
            issuer: process.env.MEETING_JWT_APP_ID,
        }, (error: any, token: any) => {
            if (error) {
                reject(`Unable to generate JWT Token`);
            }
            resolve(token);
        });
    });
}

export const asyncForEach = async (array: any, callback: any) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}