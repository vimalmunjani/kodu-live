import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from "typeorm";
import { User } from "./User";
import { LogEntity } from "./Log";
import { Profile } from "./Profile";

@Entity()
export class Subscription extends LogEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true,
        default: null
    })
    subscriptionId: string;

    @Column({
        nullable: true,
        default: 'Basic'
    })
    planType: string;

    @Column({
        nullable: true,
        default: null
    })
    tenureType: string;

    @Column({
        nullable: true,
        default: null
    })
    startDate: string;

    @Column({
        nullable: true,
        default: null
    })
    nextBillingDate: string;

    @Column({
        nullable: true,
        default: null
    })
    state: string;

    @OneToOne(type => Profile, profile => profile.user)
    profile: Profile;

}