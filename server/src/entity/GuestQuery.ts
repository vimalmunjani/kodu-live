import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class GuestQuery {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true,
        default: null
    })
    message: string;
    @Column({
        nullable: true,
        default: null
    })
    email: string;
    @Column({
        nullable: true,
        default: null
    })
    name: string;

}