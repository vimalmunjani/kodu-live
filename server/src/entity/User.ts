
import { Entity, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToMany } from "typeorm";
import { IUser, AuthStrategy } from "../models";
import { Profile } from "./Profile";
import { Meeting } from "./Meeting";
import { LogEntity } from "./Log";
import { Subscription } from "./Subscription";



@Entity()
export class User extends LogEntity implements IUser {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    isActive: boolean;

    @Column({
        default: false
    })
    isRegistered: boolean;

    @Column()
    strategy: AuthStrategy;

    @OneToOne(type => Profile, profile => profile.user, {
        eager: true
    })
    profile: Profile;

    @OneToMany(type => Meeting, meeting => meeting.organizer)
    meetingsAsOwner: Meeting[];

    @ManyToMany(type => Meeting, meeting => meeting.guests)
    meetingsAsGuest: Meeting[];

}