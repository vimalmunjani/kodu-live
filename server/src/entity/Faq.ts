import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne } from "typeorm";
import { FaqCategory } from "./FaqCategory";
import { LogEntity } from "./Log";

@Entity()
export class Faq extends LogEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true,
        default: null
    })
    question: string;
    @Column({
        nullable: true,
        default: null
    })
    answer: string;

    @ManyToOne(type => FaqCategory, {
        eager: true
    })
    @JoinColumn()
    faqCategory: FaqCategory;

}