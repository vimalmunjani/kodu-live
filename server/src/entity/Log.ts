import { Column } from "typeorm";

export class LogEntity {

    @Column({
        nullable: true,
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP'
    })
    createdOn: string;

    @Column({
        nullable: true,
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP'
    })
    lastModifiedOn: string;

    @Column({
        nullable: true
    })
    createdBy: number;

    @Column({
        nullable: true
    })
    lastModifiedBy: number;

}