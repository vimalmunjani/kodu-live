import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { LogEntity } from "./Log";
import { IPlan } from "../models";

@Entity()
export class Plan extends LogEntity implements IPlan {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({
        nullable: true,
        default: null
    })

    @Column({
        nullable: false,
        default: ''
    })
    name: string;

    @Column({
        nullable: true,
        default: null
    })
    description: string;

    @Column({
        nullable: true,
        default: null,
        type: 'simple-array'
    })
    features: string[];

    @Column({
        nullable: true,
        default: null,
        type: 'json'
    })
    price: {
        monthly: number;
        quarterly: number;
        annually: number;
    }

}