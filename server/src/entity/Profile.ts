import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from "typeorm";
import { User } from "./User";
import { LogEntity } from "./Log";
import { Subscription } from "./Subscription";

export enum UserRole {
    SUPERADMIN = "SUPERADMIN",
    ADMIN = "ADMIN",
    ENDUSER = "ENDUSER",
    SUPPORTADMIN = "SUPPORTADMIN",
}

@Entity()
export class Profile extends LogEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true,
        default: null
    })
    @Column()
    name: string;

    @Column({
        nullable: true,
        default: null
    })
    contact: string;

    @Column({
        nullable: true,
        default: null
    })
    address: string;

    @Column({
        nullable: true,
        default: null
    })
    photoUrl: string;

    @Column({
        type: "enum",
        enum: UserRole,
        default: UserRole.ENDUSER
    })
    role: UserRole;

    @OneToOne(type => User, user => user.profile)
    @JoinColumn()
    user: User;

    @OneToOne(type => Subscription, subscription => subscription.profile, {
        eager: true
    })
    @JoinColumn()
    subscription: Subscription;

}