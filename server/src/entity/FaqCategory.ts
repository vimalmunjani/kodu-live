import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { LogEntity } from "./Log";

@Entity()
export class FaqCategory extends LogEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true,
        default: null
    })
    name: string;
    @Column({
        nullable: true,
        default: false
    })
    disabled: boolean;

}