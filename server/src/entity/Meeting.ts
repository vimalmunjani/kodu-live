import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable, Generated } from "typeorm";
import { User } from "./User";
import { LogEntity } from "./Log";

@Entity()
export class Meeting extends LogEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false,
        unique: true
    })
    @Generated('uuid')
    meetingCode: string;

    @Column({
        nullable: true,
        default: null
    })
    title: string;

    @Column({
        type: 'timestamp',
        precision: 6,
        default: () => 'CURRENT_TIMESTAMP(6)',
        update: false,
    })
    meetingDate: Date;

    @Column({
        default: null
    })
    startTime: string;

    @Column({
        default: null
    })
    endTime: string;

    @Column({ nullable: true })
    guestEmailsString: string;

    @ManyToOne(type => User, user => user.meetingsAsOwner, {
        eager: true
    })
    organizer: User;

    @ManyToMany(type => User, user => user.meetingsAsGuest)
    @JoinTable()
    guests: Array<User>;

    @Column({
        nullable: true,
        default: false
    })
    cancelled: boolean;

}