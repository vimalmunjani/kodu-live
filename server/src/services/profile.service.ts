import { getRepository } from "typeorm";

import { Profile } from "../entity/Profile";
import { IUser, ISocialUser } from "../models";
import { IProfile } from "../models/profile";
import { User } from "../entity/User";
import { Subscription } from "../entity/Subscription";



export const getProfile = async (user: User): Promise<IProfile | undefined> => {
    const profileRepository = getRepository(Profile);
    const profile = await profileRepository.findOne({ user });
    return profile;
};

export const createProfileByLocalStrategy = async (user: IUser, localUser: User, basicSubscription?: Subscription): Promise<IProfile> => {
    const { email } = user;
    const profileRepository = getRepository(Profile);
    const newProfile = profileRepository.create({ name: email });
    newProfile.user = localUser;
    if (basicSubscription) {
        newProfile.subscription = basicSubscription;
    }
    await profileRepository.save(newProfile);
    return newProfile;
};

export const createProfileByGoogleStrategy = async (user: ISocialUser, localUser: User, basicSubscription?: Subscription): Promise<IProfile | undefined> => {
    const { email, name, picture } = user;
    const profileRepository = getRepository(Profile);
    const profile = await getProfile(localUser);
    if (profile) {
        return profile;
    } else {
        const newProfile = profileRepository.create({ name, photoUrl: picture });
        newProfile.user = localUser;
        if (basicSubscription) {
            newProfile.subscription = basicSubscription;
        }

        await profileRepository.save(newProfile);
        return newProfile;
    }

};

export const createSubscription = async (): Promise<Subscription | undefined> => {
    const subscriptionRepository = getRepository(Subscription);
    const basicSubscription = subscriptionRepository.create();
    await subscriptionRepository.save(basicSubscription);
    return basicSubscription;
};

export const createProfileByFacebookStrategy = async (user: ISocialUser, localUser: User, basicSubscription?: Subscription): Promise<IProfile | undefined> => {
    const { email, name, picture } = user;
    const profileRepository = getRepository(Profile);
    const profile = getProfile(localUser);
    if (profile) {
        return profile;
    } else {
        const newProfile = profileRepository.create({ name, photoUrl: picture });
        newProfile.user = localUser;
        if (basicSubscription) {
            newProfile.subscription = basicSubscription;
        }
        await profileRepository.save(newProfile);
        return newProfile;
    }

};

export const update = async (profile: IProfile): Promise<IProfile> => {
    const profileRepository = getRepository(Profile);
    const updatedProfile = await profileRepository.save(profile);
    return updatedProfile;
};


