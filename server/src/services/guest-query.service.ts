import { getRepository } from "typeorm";
import { GuestQuery } from "../entity/GuestQuery";
import { IGuestQuery } from "../models/guest-query";


export const createGuestQuery = async (guestQuery: IGuestQuery): Promise<IGuestQuery> => {
    const queryRepository = getRepository(GuestQuery);
    const newQuery = queryRepository.create(guestQuery);
    await queryRepository.save(newQuery);
    return newQuery;
};