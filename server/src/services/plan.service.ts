import { getRepository } from "typeorm";

import { Plan } from "../entity/Plan";
import { IPlan } from "../models";

export const getAllPlans = async (): Promise<any> => {
    try {
        const planRepository = getRepository(Plan);
        const plans = planRepository.find();
        return plans;
    } catch (error) {
        throw new Error("ERROR FETCHING PLAN");
    }
};

export const getPlanById = async (planId: string): Promise<any> => {
    try {
        const planRepository = getRepository(Plan);
        const plan = planRepository.findOneOrFail({ id: planId });
        return plan;
    } catch (error) {
        throw new Error("ERROR FETCHING PLAN");
    }
};

export const createPlan = async (plan: IPlan): Promise<IPlan> => {
    try {
        const planRepository = getRepository(Plan);
        const newPlan = planRepository.create(plan);
        await planRepository.save(newPlan);
        return newPlan;
    } catch (error) {
        throw new Error("ERROR CREATING PLAN");
    }
};

export const updatePlanById = async (planId: string, plan: any): Promise<any> => {
    try {
        const planRepository = getRepository(Plan);
        await planRepository.update({ id: planId }, plan);
        const updatedPlan = planRepository.findOne({ id: planId });
        return updatedPlan;
    } catch (error) {
        throw new Error("ERROR UPDATING PLAN");
    }
};
