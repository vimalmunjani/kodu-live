import { getRepository } from "typeorm";
import { FaqCategory } from "../entity/FaqCategory";
import { IFaqCategory } from "../models/faq-category";


export const createFaqCategory = async (faqCategory: IFaqCategory): Promise<IFaqCategory> => {
    const categoryRepository = getRepository(FaqCategory);
    const newCategory = categoryRepository.create(faqCategory);
    await categoryRepository.save(newCategory);
    return newCategory;
};
export const updateFaqCategory = async (faqCategory: IFaqCategory): Promise<IFaqCategory> => {
    const categoryRepository = getRepository(FaqCategory);
    const newCategory = categoryRepository.save(faqCategory);
    return newCategory;
};


export const getFaqCategory = async (): Promise<IFaqCategory[] | undefined> => {
    const categoryRepository = getRepository(FaqCategory);
    const categories = await categoryRepository.find();
    return categories;
};
