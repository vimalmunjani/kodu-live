import { Subscription } from "../entity/Subscription";
import { getRepository } from "typeorm";
import { Profile } from "../entity/Profile";

export const getSubscription = async (profile: Profile): Promise<Subscription | undefined> => {
    const subscriptionRepository = getRepository(Subscription);
    const subscription = await subscriptionRepository.findOne(profile);
    return subscription;
};

export const update = async (subscription: Subscription): Promise<Subscription> => {
    try {
        const subscriptionRepository = getRepository(Subscription);
        const updatedSubscription = await subscriptionRepository.save(subscription);
        return updatedSubscription;
    } catch (error) {
        return error;
    }
};
