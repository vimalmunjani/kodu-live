import { IMeeting } from "../models/meeting";
import { getRepository } from "typeorm";
import { Meeting } from "../entity/Meeting";
import { User } from "../entity/User";

export const getMeetingsByUser = async (user: User, pageIndex: number, pageSize: number): Promise<{
    totalCount: number,
    pageIndex: number,
    pageSize: number,
    data: Array<IMeeting>,
} | undefined> => {
    const meetingRepository = getRepository(Meeting);
    const skippedItems = (pageIndex) * pageSize;

    const totalMeetings = await meetingRepository.find({ organizer: user, cancelled: false });
    const meetings = await meetingRepository.createQueryBuilder().where({ organizer: user, cancelled: false })
        .orderBy('createdOn', "DESC")
        .offset(skippedItems)
        .limit(pageSize)
        .getMany()
    let totalCount = totalMeetings && totalMeetings.length;
    if (!totalCount) {
        totalCount = 0;
    }
    // meetings.forEach((meeting) => meeting.organizer = user);
    return {
        totalCount,
        pageIndex,
        pageSize,
        data: meetings,
    }
};

export const createMeeting = async (user: User, title: string, meetingDate: Date, startTime: string, endTime: string, guestEmails: Array<string>): Promise<IMeeting> => {
    const meetingRepository = getRepository(Meeting);
    if (!guestEmails) {
        guestEmails = [];
    }
    const guestEmailsString = guestEmails.join(',');
    const meeting = {
        title, meetingDate, startTime, endTime, guestEmailsString
    };
    const newMeeting = meetingRepository.create(meeting);
    newMeeting.organizer = user;
    await meetingRepository.save(newMeeting);
    return newMeeting;
};

export const findMeeting = async (meetingCode: string): Promise<IMeeting> => {
    const meetingRepository = getRepository(Meeting);
    const meeting = await meetingRepository.findOne({ meetingCode });
    if (!meeting) {
        throw new Error(`Meeting not found`)
    }
    return meeting
};

export const findMeetingById = async (id: number): Promise<IMeeting> => {
    const meetingRepository = getRepository(Meeting);
    const meeting = await meetingRepository.findOne({ id });
    if (!meeting) {
        throw new Error(`Meeting not found`)
    }
    return meeting
};


export const updateMeeting = async (meeting: IMeeting): Promise<IMeeting | undefined> => {
    try {
        let { title, meetingDate, startTime, endTime, guests } = meeting;
        if (!guests) {
            guests = [];
        }
        const guestEmailsString = guests.join(',');
        const meetingRepository = getRepository(Meeting);
        const updatedMeetingRecord = await meetingRepository.update(meeting.id, { title, meetingDate, startTime, endTime, guestEmailsString });
        const updatedMeeting = await meetingRepository.findOne({ id: meeting.id });
        return updatedMeeting;
    } catch (error) {
        return error;
    }
};