import * as paypal from 'paypal-rest-sdk';
import '../utils/paypal.config';

export const createSubscription = async (subscriptionId: string): Promise<any> => {
    return new Promise((resolve, reject) => {
        paypal.billingAgreement.get(subscriptionId, function (error: any, billingAgreement: any) {
            if (error) {
                reject(error);
            } else {
                resolve(billingAgreement);
            }
        });
    })
};

export const cancelSubscription = async (subscriptionId: string): Promise<any> => {
    const cancel_note = {
        "note": "Cancel Subscription"
    };
    return new Promise((resolve, reject) => {
        paypal.billingAgreement.cancel(subscriptionId, cancel_note, function (error: any, billingAgreement: any) {
            if (error) {
                reject(error);
            } else {
                resolve(billingAgreement);
            }
        });
    })
};

export const reactivateSubscription = async (subscriptionId: string): Promise<any> => {
    const resume_note = {
        "note": "Resume Subscription"
    };
    return new Promise((resolve, reject) => {
        paypal.billingAgreement.reactivate(subscriptionId, resume_note, function (error: any, billingAgreement: any) {
            if (error) {
                reject(error);
            } else {
                resolve(billingAgreement);
            }
        });
    })
};
