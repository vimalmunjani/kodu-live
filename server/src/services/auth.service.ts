import { getRepository } from "typeorm";

import { User } from "../entity/User";

export const ifUserExists = async (email: string): Promise<boolean> => {
    const userRepository = getRepository(User);
    const user = await userRepository.findOne({ email });
    if (!user) {
        return false;
    }
    return true
};

export const createUser = async (email: string, password: string): Promise<User> => {
    const userRepository = getRepository(User);
    const newUser = userRepository.create({ email, password });
    newUser.isActive = true;
    newUser.isRegistered = false;
    await userRepository.save(newUser);
    return newUser;
};

export const findUser = async (email: string): Promise<User> => {
    const userRepository = getRepository(User);
    const user = await userRepository.findOne({ email });
    if (!user) {
        throw new Error(`User not found`)
    }
    return user
};


export const markUserAsRegistered = async (email: string, isRegistered: boolean = true): Promise<User> => {
    const userRepository = getRepository(User);
    const user = await userRepository.findOne({ email });
    if (!user) {
        throw new Error(`User not found`)
    }
    user.isRegistered = !!isRegistered;
    await userRepository.save(user);
    return user
};

export const getRegisteredUsers = async (isRegistered: boolean = true): Promise<Array<User>> => {
    const userRepository = getRepository(User);
    let users = await userRepository.find({ isRegistered });
    if (!users) {
        users = [];
    }
    return users
};

export const findOrCreateUser = async (email: string, password: string): Promise<User> => {
    try {
        const user = await findUser(email);
        return user;
    } catch (error) {
        const user = await createUser(email, password);
        return user;
    }
};

export const updateUser = async (user: User): Promise<User> => {
    try {
        const userRepository = getRepository(User);
        const updatedUser = await userRepository.save(user);
        return updatedUser;
    } catch (error) {
        throw new Error("Unable to update user");
    }
};
