import { getRepository } from "typeorm";
import { Faq } from "../entity/Faq";
import { IFaq } from "../models/faq";


export const createFaq = async (faq: IFaq): Promise<IFaq> => {
    const Repository = getRepository(Faq);
    const newFaq = Repository.create(faq);
    await Repository.save(newFaq);
    return newFaq;
};
export const updateFaq = async (faq: IFaq): Promise<IFaq> => {
    const Repository = getRepository(Faq);
    const newFaq = Repository.save(faq);
    return newFaq;
};



export const getFAQs = async (pageIndex: number, pageSize: number): Promise<{
    totalCount: number,
    pageIndex: number,
    pageSize: number,
    data: Array<IFaq>,
} | undefined> => {
    const faqRepository = getRepository(Faq);
    const skippedItems = (pageIndex) * pageSize;

    const totalFaqs = await faqRepository.find();
    const faqs = await faqRepository.createQueryBuilder().where({})
        .orderBy('createdOn', "DESC")
        .offset(skippedItems)
        .limit(pageSize)
        .getMany()
    let totalCount = totalFaqs && totalFaqs.length;
    if (!totalCount) {
        totalCount = 0;
    }
    return {
        totalCount,
        pageIndex,
        pageSize,
        data: faqs,
    }
};
