import { Request, Response, NextFunction } from "express";
import { AuthUtils } from "../utils";

export const authCredentialsValidator = async (request: Request, response: Response, next: NextFunction) => {

    const { email, password } = request.body;

    const { error, message } = AuthUtils.validateAuthCredentials(email, password);
    if (error) {
        return response.status(403).json({ message });
    }

    try {
        const passwordHash = await AuthUtils.hashPassword(password);
        request.body = { ...request.body, password: passwordHash };
        next();
    } catch (error) {
        return response.status(403).json({ message: `Something went wrong, Try Again!` });
    }

}

export const validateAuthToken = async (request: Request, response: Response, next: NextFunction) => {
    let token = request.headers.authorization ? request.headers.authorization.split(' ')[1] : null;
    let cookieToken = request.cookies;
    if (!token) {
        return response.status(403).json({ message: `Token not present` });
    }
    try {
        const user = await AuthUtils.verifyJWTToken(token);
        request.body = { ...request.body, user }
        request.user = user;
        next();
    } catch (error) {
        return response.status(403).json({ message: `Invalid Token` });
    }
}
