import { Response, Request, NextFunction } from "express";
import { OAuth2Client } from "google-auth-library";

export const validateGoogleAuthToken = async (request: Request, response: Response, next: NextFunction) => {

    let token = request.body.access_token || null;
    if (!token) {
        return response.status(403).json({ message: `Token not present` });
    }

    try {
        const client = new OAuth2Client(process.env.G_CLIENT_ID);
        const ticket = await client.verifyIdToken({
            idToken: token,
        });
        const user = ticket.getPayload();
        request.body = { ...request.body, user };
        next();
    } catch (error) {
        console.log(error.message);
        return response.status(403).json({ message: `Invalid Token` });
    }

}