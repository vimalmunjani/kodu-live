import multer from "multer";

const MIME_TYPE_MAP: { [key: string]: string } = {
    "image/png": "png",
    "image/jpeg": "jpg",
    "image/jpg": "jpg"
};

const storage = multer.diskStorage({
    destination: (request: any, file: any, cb: any) => {
        const isValid = MIME_TYPE_MAP[file.mimetype];
        let error = null;
        if (!isValid) {
            error = new Error("Invalid mime type");
        }
        cb(error, "src/images");
    },
    filename: (request: any, file: any, cb: any) => {
        const name = file.originalname
            .toLowerCase()
            .split(" ")
            .join("-");
        const ext = MIME_TYPE_MAP[file.mimetype];
        cb(null, name + "-" + Date.now() + "." + ext);
    }
});

export const extractAvatar = multer({ storage: storage }).single("image");
