import passport from 'passport';
import FacebookTokenStrategy from 'passport-facebook-token';

passport.use('facebook', new FacebookTokenStrategy({
    clientID: '324635705390398',
    clientSecret: '57725fff2f72163e1bfff8ad7154fcf1',
}, async (accessToken, refreshToken, profile, done) => {
    try {
        return done(null, profile._json);
    } catch (error) {
        return done(error, false, error.message);
    }
}));
