import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import * as env from 'dotenv';
import compression from 'compression';
import helmet from 'helmet';
import cookieParser from 'cookie-parser';
import path from 'path';
import { createConnection } from "typeorm";
import "reflect-metadata";

import { authRouter, userRouter, guestRouter, subscriptionRouter, webhooksRouter } from './routes';
import { faqRouter } from './routes/faq.route';

// load the env config
env.config();

createConnection()
    .then(() => console.log('Connected to DB'))
    .catch((error) => {
        console.log('Error creating DB connection', error.message);
        process.exit();
    });

// creating the express app
const app = express();

// using the middlewares
app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors({}));
app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, '../../client/dist')));
app.use(express.static(path.join(__dirname, './images')));

app.use('/api/auth', authRouter);
app.use('/api/user', userRouter);
app.use('/api/support', faqRouter);
app.use('/api/guest', guestRouter);
app.use('/api/subscription', subscriptionRouter);
app.use('/api/webhooks', webhooksRouter);

app.use('/ping', (req, res) => {
    return res.status(200).json('OK');
});
app.use('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../client/dist/index.html'));
});

module.exports = app;
