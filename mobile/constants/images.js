
const avatar_4 = require("../assets/images/avatar-4.jpg");
const logo = require("../assets/images/logo.png");
const meetingBg = require("../assets/images/meeting-bg-photo-3.jpg");
const calendar = require("../assets/images/calendar.png");
const pending = require("../assets/images/booking-pending.png");
const confirmed = require("../assets/images/booking-confirmed.png");

export default {
    avatar_4,
    logo,
    meetingBg,
    calendar,
    pending,
    confirmed
}