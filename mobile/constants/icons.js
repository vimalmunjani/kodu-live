export const back = require("../assets/icons/back.png");
export const list = require("../assets/icons/list.png");
export const search = require("../assets/icons/search.png");
export const user = require("../assets/icons/user.png");
export const home = require("../assets/icons/home.png");
export const video = require("../assets/icons/video.png");
export const fast = require("../assets/icons/fast.png");
export const eye = require("../assets/icons/eye.png");
export const envelope = require("../assets/icons/envelope.png");
export const phone = require("../assets/icons/phone.png");
export const plan = require("../assets/icons/plan.png");
export const shutdown = require("../assets/icons/shutdown.png");
export const edit = require("../assets/icons/edit.png");
export const calendar = require("../assets/icons/calendar.png");
export const plus = require("../assets/icons/plus.png");
export const datetime = require("../assets/icons/datetime.png");
export const close = require("../assets/icons/close.png");

export default {
    back,
    list,
    search,
    user,
    home,
    video,
    fast,
    eye,
    envelope,
    phone,
    plan,
    shutdown,
    edit,
    calendar,
    plus,
    datetime,
    close
}