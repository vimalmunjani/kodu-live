import Constants from '../utils/constants';
import HttpClient from './http.service';

const MeetingService = {

    fetch: (pagination) => {
        const { pageIndex, pageSize, token } = pagination;
        return HttpClient.get(`${Constants.FETCH_MEETING_URL}?pageIndex=${pageIndex}&pageSize=${pageSize}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
    },

    start: (payload, token) => {
        const meetingMetaData = {
            title: payload.title,
            meetingDate: payload.startDate,
            startTime: (new Date(payload.startTime)).getTime(),
            endTime: (new Date(payload.endTime)).getTime(),
            guests: ['vikaspawale.v@gmail.com']
        };

        return HttpClient.post(Constants.START_MEETING_URL,
            meetingMetaData,
            {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
    },

    join: (meetingCode, token) => {

        return HttpClient.post(Constants.JOIN_MEETING_URL, {
            meetingCode
        }, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
    },

    delete: (meeting, token) => {

        return HttpClient.put(Constants.CANCEL_MEETING_URL,
            meeting
            , {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
    },



};

export default MeetingService;
