import Constants from '../utils/constants';
import HttpClient from './http.service';

const ProfileService = {

    fetch: (token) => {
        return HttpClient.get(Constants.PROFILE_URL, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
    },



};

export default ProfileService;
