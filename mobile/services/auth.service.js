import Constants from '../utils/constants';
import HttpClient from './http.service';

const AuthService = {

    signUp: (credentials) => {
        return HttpClient.post(Constants.SIGN_UP_URL, {
            email: credentials.email,
            password: credentials.password,
        });
    },

    signIn: (credentials) => {
        return HttpClient.post(Constants.SIGN_IN_URL, {
            email: credentials.email,
            password: credentials.password,
        });
    },

    validateToken: (token) => {
        return HttpClient.post(Constants.VALIDATE_TOKEN_URL, { token });
    },

    signInWithGoogle: (accessToken) => {
        return HttpClient.post(Constants.GOOGLE_AUTH, {
            'access_token': accessToken
        });
    },

    signInWithFacebook: (accessToken) => {
        return HttpClient.post(Constants.FACEBOOK_AUTH, {
            'access_token': accessToken
        });
    },

    resetPassword: (userEmail) => {
        return HttpClient.post(Constants.RESET_PASSWORD, {
            email: userEmail
        });
    },

    updatePassword: (credentials) => {
        return HttpClient.post(Constants.UPDATE_PASSWORD, {
            email: credentials.email,
            password: credentials.password,
        });
    }

};

export default AuthService;
