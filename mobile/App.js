import React from 'react';

import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'react-native';

import Tabs from './navigation/tabs'
import { ForgotPassword, Login, Register, Splash, Welcome, Live } from './screens'
import { COLORS } from './constants';

import { Provider as AuthProvider } from './context/AuthContext';
import { Provider as ProfileProvider } from './context/ProfileContext';
import { Provider as MeetingProvider } from './context/MeetingContext';
import { setNavigator } from './navigators/navigatorRef';

const Stack = createStackNavigator();

const App = () => {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor(COLORS.primary);
    return (
        <AuthProvider>
            <ProfileProvider>
                <MeetingProvider>
                    <NavigationContainer ref={(navigator) => setNavigator(navigator)}>
                        <Stack.Navigator
                            screenOptions={{
                                headerShown: false
                            }}
                            initialRouteName={'Splash'}
                        >
                            <Stack.Screen name="Splash" component={Splash} />
                            <Stack.Screen name="Welcome" component={Welcome} />
                            <Stack.Screen name="Login" component={Login} />
                            <Stack.Screen name="Register" component={Register} />
                            <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
                            <Stack.Screen name="Home" component={Tabs} />
                            <Stack.Screen name="Live" component={Live} />
                        </Stack.Navigator>
                    </NavigationContainer>
                </MeetingProvider>
            </ProfileProvider>
        </AuthProvider>
    )
}

export default App;