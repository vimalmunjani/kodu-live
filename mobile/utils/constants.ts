const BASE_URL = `https://kodulive.com/api`;
const BASE_AUTH_URL = `${BASE_URL}/auth`;
const BASE_USER_URL = `${BASE_URL}/user`;

const Constants = {
    /**
     * Auth
     */
    SIGN_UP_URL: `${BASE_AUTH_URL}/signup`,
    SIGN_IN_URL: `${BASE_AUTH_URL}/signin`,
    VALIDATE_TOKEN_URL: `${BASE_AUTH_URL}/validate-token`,
    GOOGLE_AUTH: `${BASE_AUTH_URL}/oauth/google`,
    FACEBOOK_AUTH: `${BASE_AUTH_URL}/oauth/facebook`,
    RESET_PASSWORD: `${BASE_AUTH_URL}/password/reset`,
    UPDATE_PASSWORD: `${BASE_AUTH_URL}/password/update`,
    EMAIL_PATTERN: `^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$`,
    /**
     * User/ Profile
     */
    PROFILE_URL: `${BASE_USER_URL}/profile`,
    AVATAR_URL: `${BASE_USER_URL}/avatar`,
    /**
     * Meeting
     */
    FETCH_MEETING_URL: `${BASE_USER_URL}/meetings`,
    START_MEETING_URL: `${BASE_USER_URL}/meeting`,
    JOIN_MEETING_URL: `${BASE_USER_URL}/join-meeting`,
    UPDATE_MEETING_URL: `${BASE_USER_URL}/meeting`,
    CANCEL_MEETING_URL: `${BASE_USER_URL}/cancel-meeting`
};

export default Constants;