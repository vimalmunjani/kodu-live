import ProfileService from '../services/profile.service';
import createDataContext from './createDataContext';


const profileReducer = (state, action) => {
    switch (action.type) {
        case 'set_profile':
            return { errorMessage: '', profile: action.payload };
        case 'set_error':
            return { ...state, errorMessage: action.payload };
        case 'set_loading':
            return { ...state, loading: action.payload };
        case 'clear_error_message':
            return { ...state, errorMessage: '' };
        default:
            return state;

    }
}

const getProfile = dispatch => {
    return (token: string) => {
        dispatch({ type: 'set_loading', payload: true });
        ProfileService.fetch(token)
            .then(((response) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: null });
                dispatch({ type: 'set_profile', payload: response.data });
            }))
            .catch((error) => {
                dispatch({ type: 'set_error', payload: error });
            })
    }

}



export const { Context, Provider } = createDataContext(
    profileReducer,
    { getProfile },
    {
        profile: null,
        errorMessage: null,
        loading: false
    });