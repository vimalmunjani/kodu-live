import MeetingService from '../services/meeting.service';
import createDataContext from './createDataContext';
import { navigate } from '../navigators/navigatorRef'


const meetingReducer = (state, action) => {
    switch (action.type) {
        case 'set_meetings':
            const paginatedMeetings = action.payload;
            return {
                ...state,
                loading: false,
                errorMessage: null,
                meetings: paginatedMeetings.data,
                meetingPagination: {
                    totalCount: paginatedMeetings.totalCount,
                    pageIndex: paginatedMeetings.pageIndex,
                    pageSize: paginatedMeetings.pageSize
                }
            };
        case 'set_meeting':
            return {
                ...state,
                meeting: Object.assign({}, action.payload)
            };
        case 'set_ask_confirmation':
            const meetings = [...state.meetings];
            const item = action.payload;
            meetings.forEach((meeting) => {
                if (meeting.id === item.id) {
                    meeting.askForConfrimation = !meeting.askForConfrimation;
                }
            })
            return {
                ...state,
                meetings: meetings
            };
        case 'delete_meeting':
            let meetingList = [...state.meetings];
            const payloadMeeting = action.payload;
            meetingList.forEach((meeting) => {
                if (meeting.id === payloadMeeting.id) {
                    meeting.askForConfrimation = !meeting.askForConfrimation;
                }
            });
            meetingList = meetingList.filter((meeting) => meeting.id !== payloadMeeting.id);
            return {
                ...state,
                meetings: meetingList
            };
        case 'get_meeting':
            return state;
        case 'update_meeting':
            return state;
        case 'set_error':
            return { ...state, errorMessage: action.payload };
        case 'set_loading':
            return { ...state, loading: action.payload };
        case 'clear_error_message':
            return { ...state, errorMessage: '' };

        default:
            return state;

    }
}

const getMeetings = dispatch => {
    return (token) => {
        dispatch({ type: 'set_loading', payload: true });
        MeetingService.fetch({ pageIndex: 0, pageSize: 10, token })
            .then(((response) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: null });
                dispatch({ type: 'set_meetings', payload: response.data });
            }))
            .catch((error) => {
                dispatch({ type: 'set_error', payload: error });
            })
    }

}

const toJoinMeeting = dispatch => {
    return (meeting) => {
        dispatch({ type: 'set_meeting', payload: meeting });
        navigate('Join');
    }

}

const askForConfrimation = dispatch => {
    return (meeting) => {
        dispatch({ type: 'set_ask_confirmation', payload: meeting });
    }

}

const joinMeeting = dispatch => {
    return (meeting, token) => {
        const meetingCode = meeting.meetingCode;
        MeetingService.join(meetingCode, token)
            .then(({ data }) => {
                dispatch({ type: 'set_meeting', payload: data });
                navigate('Live');
            })
            .catch((error) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: 'Something went wrong' });
            });

    }

}

const deleteMeeting = dispatch => {

    return (meeting, token) => {
        dispatch({ type: 'set_loading', payload: true });
        MeetingService.delete(meeting, token)
            .then(({ data }) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'delete_meeting', payload: meeting });
            })
            .catch((error) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: 'Something went wrong' });
            });

    }

}

const setMeetingByMeetingCode = dispatch => {
    return (meeting, token) => {
        const meetingCode = meeting.meetingCode;
        MeetingService.join(meetingCode, token)
            .then(({ data }) => {
                dispatch({ type: 'set_meeting', payload: data });
            })
            .catch((error) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: 'Something went wrong.Invalid Meeting Code' });
            });

    }

}


const startMeeting = dispatch => {
    return (payload, token) => {
        MeetingService.start(payload, token)
            .then(({ data }) => {
                getMeetings(token);
                navigate('Home');
            })
            .catch((error) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: 'Something went wrong' });
            });
    }
}



export const { Context, Provider } = createDataContext(
    meetingReducer,
    {
        getMeetings,
        toJoinMeeting,
        joinMeeting,
        startMeeting,
        setMeetingByMeetingCode,
        askForConfrimation,
        deleteMeeting
    }, {
    loading: false,
    errorMessage: null,
    meetings: [],
    meetingPagination: {
        totalCount: 0,
        pageIndex: 1,
        pageSize: 10
    },
    meeting: null
});