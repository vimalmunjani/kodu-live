import AuthService from '../services/auth.service';
import createDataContext from './createDataContext';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { navigate } from '../navigators/navigatorRef';
import { GoogleSignin } from '@react-native-community/google-signin';

GoogleSignin.configure({
    webClientId: '582096045453-536rkuuind2g3si916ltdtglkde0bh3g.apps.googleusercontent.com',
    offlineAccess: true,
    forceCodeForRefreshToken: true,
    iosClientId: '',
});

const authReducer = (state, action) => {
    switch (action.type) {
        case 'sign_in':
            return { errorMessage: '', token: action.payload };
        case 'set_error':
            return { ...state, errorMessage: action.payload };
        case 'set_info':
            return { ...state, infoMessage: action.payload };
        case 'set_loading':
            return { ...state, loading: action.payload };
        case 'clear_error_message':
            return { ...state, errorMessage: null, infoMessage: null };
        case 'sign_out':
            return { errorMessage: null, infoMessage: null, token: null, loading: false };
        default:
            return state;

    }
}

const signIn = dispatch => {
    return (credentials) => {
        if (credentials && credentials.email && credentials.password) {
            dispatch({ type: 'set_loading', payload: true });
            clearErrorMessage();
        } else {
            const errorMessage = !credentials.email ? 'Enter email id' : 'Enter password';
            dispatch({ type: 'set_error', payload: errorMessage });
            return;
        }

        AuthService.signIn(credentials)
            .then(({ data }) => {
                if (data.authToken) {
                    AsyncStorage.setItem('auth_token', data.authToken);
                    dispatch({ type: 'sign_in', payload: data.authToken });
                    navigate('Home');
                } else {
                    dispatch({ type: 'set_error', payload: 'Please check your email id or password' });
                }
            })
            .catch((error) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: 'Please check your email id or password' });
            });
    }

}

const signUp = dispatch => {
    return ({ email, password, confirmPassword }) => {
        if (email && password && confirmPassword) {
            dispatch({ type: 'set_loading', payload: true });
            clearErrorMessage();
        } else {
            const errorMessage = !email ? 'Enter email id' : 'Enter password';
            dispatch({ type: 'set_error', payload: errorMessage });
            return;
        }
        if (password !== confirmPassword) {
            dispatch({ type: 'set_error', payload: 'Password not matching' });
            return;
        }

        AuthService.signUp({ email, password })
            .then(({ data }) => {
                if (data.authToken) {
                    AsyncStorage.setItem('auth_token', data.authToken);
                    dispatch({ type: 'sign_in', payload: data.authToken });
                    navigate('Home');
                } else {
                    dispatch({ type: 'set_error', payload: 'Please check your email id or password' });
                }
            })
            .catch((error) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: 'Please check your email id or password' });
            });
    }

}

const signOut = dispatch => {
    return () => {
        AsyncStorage.removeItem('auth_token', () => {
            navigate('Welcome');
            dispatch({ type: 'sign_out' });
        });

    }
}


const clearErrorMessage = dispatch => {
    return () => {
        dispatch({ type: 'clear_error_message' });
    }

}

const tryLocalSignIn = dispatch => {
    return () => {
        AsyncStorage.getItem('auth_token')
            .then((token) => {
                if (token) {
                    dispatch({ type: 'sign_in', payload: token });
                    navigate('Home');
                } else {
                    navigate('Welcome');
                }
            }
            )
            .catch((error) => {
                navigate('Welcome');
            });

    }

}

const signInWithGoogle = dispatch => {
    return async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            AuthService.signInWithGoogle(userInfo.idToken)
                .then(({ data }) => {
                    dispatch({ type: 'set_loading', payload: false });
                    if (data.authToken) {
                        AsyncStorage.setItem('auth_token', data.authToken);
                        dispatch({ type: 'sign_in', payload: data.authToken });
                        navigate('Home');
                    } else {
                        dispatch({ type: 'set_error', payload: 'signInWithGoogle error' });
                    }
                })
                .catch((error) => {
                    dispatch({ type: 'set_loading', payload: false });
                    dispatch({ type: 'set_error', payload: 'Error Signing In, Please check your google account' });
                    console.error(error);
                });
        } catch (e) {
            dispatch({ type: 'set_loading', payload: false });
            dispatch({ type: 'set_error', payload: 'Error Signing In, Please check your google account' });
            console.error('signInWithGoogle error', e);
        }
    }
}

const resetPassword = dispatch => {
    return (email) => {
        if (email) {
            dispatch({ type: 'set_loading', payload: true });
            clearErrorMessage();
        } else {
            dispatch({ type: 'set_error', payload: 'Enter email id' });
            return;
        }

        AuthService.resetPassword(email)
            .then(({ data }) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_info', payload: 'Password sent to registered email' });
            })
            .catch((error) => {
                dispatch({ type: 'set_loading', payload: false });
                dispatch({ type: 'set_error', payload: 'Please check your email id' });
            });
    }

}


export const { Context, Provider } = createDataContext(
    authReducer,
    {
        signIn,
        signUp,
        signOut,
        clearErrorMessage,
        tryLocalSignIn,
        resetPassword,
        signInWithGoogle
    }, {
    token: null,
    errorMessage: null,
    loading: false,
    infoMessage: null
});