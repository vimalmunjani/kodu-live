import React, { useEffect, useState, useContext } from "react";
import {
    SafeAreaView,
    View,
    Text,
    StyleSheet,
    Image,
    Animated
} from "react-native";

import { images } from '../constants';
import { Context as AuthContext } from '../context/AuthContext';

const Splash = () => {

    const [logoAnime, setLogoAnime] = useState(new Animated.Value(0));
    const [logoTextAmine, setLogoTextAmine] = useState(new Animated.Value(0));
    const { tryLocalSignIn } = useContext(AuthContext);

    useEffect(() => {

        Animated.parallel([
            Animated.spring(logoAnime, {
                toValue: 1,
                tension: 10,
                friction: 2,
                duration: 1000
            }).start(),
            Animated.timing(logoTextAmine, {
                toValue: 1,
                duration: 1200
            }),
        ]).start(() => {
            setTimeout(() => {
                tryLocalSignIn();
            }, 500);
        });
    }, []);

    function renderSplash() {
        return (

            <View style={styles.container}>
                <Animated.View
                    style={{
                        opacity: logoAnime,
                        top: logoAnime.interpolate({
                            inputRange: [0, 1],
                            outputRange: [80, 0],
                        }),
                    }}>
                    <Image source={images.logo} resizeMode="contain" style={{
                        height: 120,
                        width: 220
                    }} />

                </Animated.View>
                <Animated.View style={{ opacity: logoTextAmine }}>
                    <Text style={styles.logoTextAmine}></Text>
                </Animated.View>
            </View>
        )
    }



    return (
        <SafeAreaView style={styles.safeContainer}>
            {renderSplash()}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    safeContainer: {
        flex: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3,
        elevation: 1,
    }
})

export default Splash;