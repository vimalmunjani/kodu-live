import React, { useContext, useState, useEffect } from "react";
import {
    ActivityIndicator,
    Keyboard,
    KeyboardAvoidingView,
    StyleSheet,
    View,
    TouchableOpacity,
    Image
} from "react-native";
import { GoogleSigninButton } from '@react-native-community/google-signin';

import { Button, Block, Input, Text } from "../components";
import { COLORS, icons, SIZES, FONTS } from "../constants";

import { Context as AuthContext } from '../context/AuthContext';



const Register = ({ navigation }) => {

    const { state, signUp, clearErrorMessage, signInWithGoogle } = useContext(AuthContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const { loading } = state;

    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            clearErrorMessage();
        });

        return unsubscribe;
    }, [navigation]);

    const renderHeader = () => {
        return (
            <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingLeft: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}

                    onPress={() => navigation.navigate('Welcome')}
                >
                    <Image
                        source={icons.back}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                </TouchableOpacity>

                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View
                        style={{
                            width: '70%',
                            height: "100%",
                            backgroundColor: COLORS.lightGray3,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: SIZES.radius
                        }}
                    >
                        <Text style={{ ...FONTS.h3 }}>Signup</Text>
                    </View>
                </View>

                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingRight: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}
                >

                </TouchableOpacity>
            </View>
        )
    }


    return (
        <KeyboardAvoidingView style={styles.register}>
            <Block padding={[0, SIZES.base * 2]}>
                {renderHeader()}
                {state.errorMessage ? <Text style={styles.errorMessageStyle}>{state.errorMessage}</Text> : null}
                <Block middle>
                    <Input
                        label="Email"
                        style={[styles.input]}
                        defaultValue={email}
                        onChangeText={(email) => setEmail(email)}
                    />
                    <Input
                        secure
                        label="Password"
                        style={[styles.input]}
                        defaultValue={password}
                        onChangeText={(password) => setPassword(password)}
                    />
                    <Input
                        secure
                        label="Confirm Password"
                        style={[styles.input]}
                        defaultValue={confirmPassword}
                        onChangeText={(confirmPassword) => setConfirmPassword(confirmPassword)}
                    />
                    <Button gradient onPress={() => signUp({ email, password, confirmPassword })}>
                        {loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                                <Text bold white center>
                                    Signup
                                </Text>
                            )}
                    </Button>

                    <Button color={'black'} onPress={() => navigation.navigate("ForgotPassword")}>
                        <Text
                            white
                            caption
                            center
                            style={{ textDecorationLine: "underline" }}
                        >
                            Forgot your password?
              </Text>
                    </Button>
                    <Button color={COLORS.googleblue} onPress={signInWithGoogle}>
                        {loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                    <GoogleSigninButton
                                        style={{
                                            height: 40
                                        }}
                                        size={GoogleSigninButton.Size.Icon}
                                        color={GoogleSigninButton.Color.Dark}
                                        onPress={signInWithGoogle} />
                                    <Text
                                        white
                                        caption
                                        center
                                        style={{ textDecorationLine: "underline" }}
                                    >
                                        Sign in with Google
                                </Text>
                                </View>

                            )}
                    </Button>
                </Block>
            </Block>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    register: {
        flex: 1,
    },
    input: {
        borderRadius: 0,
        borderWidth: 0,
        borderBottomColor: COLORS.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    hasErrors: {
        borderBottomColor: COLORS.red
    },
    errorMessageStyle: {
        backgroundColor: COLORS.red,
        color: COLORS.white,
        padding: SIZES.padding * 0.5,
        paddingHorizontal: SIZES.padding,
        marginVertical: 30,
        borderRadius: SIZES.radius * 0.2
    }
});

export default Register;