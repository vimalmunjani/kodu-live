import React, { useEffect, useContext } from 'react';
import JitsiMeet, { JitsiMeetView } from 'react-native-jitsi-meet';
import { View, StyleSheet } from 'react-native';

import { Context as MeetingContext } from '../context/MeetingContext';
import { Context as ProfileContext } from '../context/ProfileContext';

function Live({ navigation }) {

    const { state: meetingState } = useContext(MeetingContext);
    const { state: profileState, getProfile } = useContext(ProfileContext);
    const { profile, loading } = profileState;
    const { meeting } = meetingState;
    useEffect(() => {
        setTimeout(() => {
            const isOrganizer = meeting?.organizer?.email === profile?.email;
            let url = `https://meeting.kodulive.com/${meeting.meetingCode}`;
            const displayName = profile && profile.name && profile.name.split(' ')[0] || '';
            const meetingTitleParam = meeting.title.split(' ').join('-');
            if (meeting && meeting.meetingCode && !isOrganizer) {
                url = `https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${meeting.token}#userInfo.displayName="${displayName}"`;
            }
            if (meeting && meeting.meetingCode && isOrganizer) {
                url = `https://meeting.kodulive.com/${meeting.meetingCode}?jwt=${meeting.token}#config.subject="${meetingTitleParam}"`;
            }

            const userInfo = {
                displayName: `${displayName}`,
                email: `${profile.email}`,
                avatar: 'https:/gravatar.com/avatar/abc123',
            };
            JitsiMeet.call(url, userInfo);
            /* Você também pode usar o JitsiMeet.audioCall (url) para chamadas apenas de áudio */
            /* Você pode terminar programaticamente a chamada com JitsiMeet.endCall () */
        }, 1000);

        return () => {
            JitsiMeet.endCall();
        };
    }, [])


    function onConferenceTerminated(nativeEvent) {
        /* Conference terminated event */
        setTimeout(() => {
            navigation.navigate('Home');
        }, 500);

    }

    function onConferenceJoined(nativeEvent) {
        /* Conference joined event */
    }

    function onConferenceWillJoin(nativeEvent) {
        /* Conference will join event */
    }
    return (
        <View style={StyleSheet.absoluteFillObject}>
            <JitsiMeetView
                onConferenceTerminated={e => onConferenceTerminated(e)}
                onConferenceJoined={e => onConferenceJoined(e)}
                onConferenceWillJoin={e => onConferenceWillJoin(e)}
                style={{
                    flex: 1,
                    height: '100%',
                    width: '100%',
                }}
            />
        </View>


    )
}
export default Live;