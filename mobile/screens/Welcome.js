import React, { useState, useContext } from "react";
import {
    Animated,
    Dimensions,
    Image,
    FlatList,
    StyleSheet,
    SafeAreaView,
    View
} from "react-native";
import { GoogleSigninButton } from '@react-native-community/google-signin';
import { Button, Block, Text } from "../components";
import { COLORS, SIZES } from "../constants";
import { Context as AuthContext } from '../context/AuthContext';

const { width, height } = Dimensions.get("window");

const Welcome = ({ navigation }) => {
    const { signInWithGoogle, loading } = useContext(AuthContext);
    const illustrationsData = [
        { id: 1, source: require("../assets/images/invite-header.png") }
    ]

    const [scrollX, setScrollX] = useState(new Animated.Value(0));
    const [illustrations, setIllustrations] = useState(illustrationsData);

    const renderIllustrations = () => {

        return (
            <FlatList
                horizontal
                pagingEnabled
                scrollEnabled
                showsHorizontalScrollIndicator={false}
                scrollEventThrottle={16}
                snapToAlignment="center"
                data={illustrations}
                keyExtractor={(item, index) => `${item.id}`}
                renderItem={({ item }) => (
                    <Image
                        source={item.source}
                        resizeMode="contain"
                        style={{ width, height: height / 2, overflow: "visible" }}
                    />
                )}
                onScroll={Animated.event([
                    {
                        nativeEvent: { contentOffset: { x: scrollX } }
                    }
                ])}
            />
        );
    }

    const renderWelcome = () => {
        return (
            <Block>
                <Block center bottom flex={0.4}>
                    <Text h1 center bold>
                        Kodu Live.
            <Text h1 primary>
                            {" "}
                Meet.
            </Text>
                    </Text>
                    <Text h3 gray2 style={{ marginTop: SIZES.padding / 2 }}>
                        Enjoy the experience.
            </Text>
                </Block>
                <Block center middle>
                    {renderIllustrations()}
                </Block>
                <Block middle flex={0.5} margin={[0, SIZES.padding * 2]}>
                    <Button gradient onPress={() => navigation.navigate("Login")}>
                        <Text center semibold white>
                            Login
                </Text>
                    </Button>
                    <Button color={'black'} shadow onPress={() => navigation.navigate("Register")}>
                        <Text center semibold white>
                            Signup
                </Text>
                    </Button>
                    <Button color={COLORS.googleblue} onPress={signInWithGoogle}>
                        {loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                    <GoogleSigninButton
                                        style={{
                                            height: 40
                                        }}
                                        size={GoogleSigninButton.Size.Icon}
                                        color={GoogleSigninButton.Color.Dark}
                                        onPress={signInWithGoogle} />
                                    <Text
                                        white
                                        caption
                                        center
                                        style={{ textDecorationLine: "underline" }}
                                    >
                                        Sign in with Google
                                </Text>
                                </View>

                            )}
                    </Button>
                </Block>
            </Block>
        );
    }

    return (
        <SafeAreaView style={styles.safeContainer}>
            {renderWelcome()}
        </SafeAreaView>
    )


}


export default Welcome;

const styles = StyleSheet.create({
    safeContainer: {
        flex: 1,
        backgroundColor: COLORS.lightGray4
    },
    stepsContainer: {
        position: "absolute",
        bottom: SIZES.base * 3,
        right: 0,
        left: 0
    },
    steps: {
        width: 5,
        height: 5,
        borderRadius: 5,
        marginHorizontal: 2.5
    }
});