import React, { useState, useContext, useEffect } from "react";
import {
    SafeAreaView,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
    ActivityIndicator
} from "react-native";

import { icons, images, SIZES, COLORS, FONTS } from '../constants';
import { Button, Text } from "../components";
import { Context as AuthContext } from '../context/AuthContext';

import { Context as MeetingContext } from '../context/MeetingContext';
import { Context as ProfileContext } from '../context/ProfileContext';

const Home = ({ navigation }) => {
    const { state: authState, clearErrorMessage } = useContext(AuthContext);
    const { state, getMeetings, toJoinMeeting, askForConfrimation, deleteMeeting } = useContext(MeetingContext);
    const { getProfile } = useContext(ProfileContext);
    const { meetings, loading } = state;

    useEffect(() => {
        if (!authState.token) {
            navigation.navigate('Welcome')
        }
        getProfile(authState.token);
        getMeetings(authState.token);
    }, []);

    useEffect(() => {
        const unsubscribeBlur = navigation.addListener('blur', () => {
            clearErrorMessage();
        });
        const unsubscribeFocus = navigation.addListener('focus', () => {
            if (!authState.token) {
                navigation.navigate('Welcome')
            }
            clearErrorMessage();
            getMeetings(authState.token);
        });

        return () => {
            unsubscribeBlur();
            unsubscribeFocus();
        };
    }, [navigation]);

    const categoryData = [
        {
            id: 1,
            name: "Mon",
            icon: images.confirmed,
        },
        {
            id: 2,
            name: "Tue",
            icon: images.pending,
        },
        {
            id: 3,
            name: "Wed",
            icon: images.pending,
        },
        {
            id: 4,
            name: "Thr",
            icon: images.pending,
        },
        {
            id: 5,
            name: "Fri",
            icon: images.pending,
        },
        {
            id: 6,
            name: "Sat",
            icon: images.pending,
        }

    ]

    const [categories, setCategories] = useState(categoryData);
    const [selectedCategory, setSelectedCategory] = useState(categoryData[(new Date()).getDay() - 1]);



    const getTimeString = (ms) => {
        const time = new Date(parseInt(ms, 10));
        let hours = time.getHours();
        let minutes = time.getMinutes();

        // Check whether AM or PM 
        const newformat = hours >= 12 ? 'PM' : 'AM';

        // Find current hour in AM-PM Format 
        hours = hours % 12;

        // To display "0" as "12" 
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        return `${hours}:${minutes} ${newformat}`;
    }
    const getMonthName = (dateString) => {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return monthNames[new Date(dateString).getMonth()];
    }

    const getDate = (date) => {
        const dateCount = new Date(date).getDate();
        return dateCount >= 10 ? dateCount : `0${dateCount}`;
    }

    const onSelectCategory = (category) => {
        setSelectedCategory(category)
    }


    const renderHeader = () => {
        return (
            <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingLeft: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}
                    onPress={() => navigation.navigate('Join')}
                >
                    <Image
                        source={icons.video}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                </TouchableOpacity>

                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View
                        style={{
                            width: '70%',
                            height: "100%",
                            backgroundColor: COLORS.lightGray3,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: SIZES.radius
                        }}
                    >
                        <Text style={{ ...FONTS.h3 }}>Meetings</Text>
                    </View>
                </View>

                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingRight: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}
                    onPress={() => navigation.navigate('InstantMeeting')}
                >
                    <Image
                        source={icons.fast}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    const renderMainCategories = () => {
        const renderItem = ({ item }) => {
            return (
                <TouchableOpacity
                    style={{
                        padding: SIZES.padding,
                        paddingBottom: SIZES.padding * 2,
                        backgroundColor: (selectedCategory?.id == item.id) ? COLORS.primary : COLORS.white,
                        borderRadius: SIZES.radius,
                        alignItems: "center",
                        justifyContent: "center",
                        marginRight: SIZES.padding,
                        ...styles.shadow
                    }}
                    onPress={() => onSelectCategory(item)}
                >
                    <View
                        style={{
                            width: 50,
                            height: 50,
                            borderRadius: 25,
                            alignItems: "center",
                            justifyContent: "center",
                            backgroundColor: (selectedCategory?.id == item.id) ? COLORS.white : COLORS.lightGray3
                        }}
                    >
                        <Image
                            source={item.icon}
                            resizeMode="contain"
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                    </View>

                    <Text
                        style={{
                            marginTop: SIZES.padding,
                            color: (selectedCategory?.id == item.id) ? COLORS.white : COLORS.black,
                            ...FONTS.body5
                        }}
                    >
                        {item.name}
                    </Text>
                </TouchableOpacity>
            )
        }

        return (
            <View style={{ padding: SIZES.padding * 2 }}>
                <Text style={{ ...FONTS.h1 }}>Upcoming</Text>
                <Text style={{ ...FONTS.h1 }}>Events</Text>

                <FlatList
                    data={categories}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={item => `${item.id}`}
                    renderItem={renderItem}
                    contentContainerStyle={{ paddingVertical: SIZES.padding * 2 }}
                />
            </View>
        )
    }

    const renderMeetingList = () => {
        const renderItem = ({ item }) => (
            <TouchableOpacity
                style={{ marginBottom: SIZES.padding * 2 }}
                onPress={() => { }}
            >
                {/* Image */}
                <View
                    style={{
                        marginBottom: SIZES.padding
                    }}
                >
                    <Image
                        source={images.meetingBg}
                        resizeMode="cover"
                        style={{
                            width: "100%",
                            height: 200,
                            borderRadius: SIZES.radius
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => toJoinMeeting(item)}
                        style={{
                            position: 'absolute',
                            height: 50,
                            top: 55,
                            width: SIZES.width * 0.7,
                            backgroundColor: COLORS.white,
                            borderRadius: SIZES.radius,
                            alignItems: 'center',
                            justifyContent: 'center',
                            ...styles.shadow,
                            alignSelf: 'center',
                            flexDirection: 'row'
                        }}
                    >

                        <TouchableOpacity
                            style={{
                                width: 30,
                                marginHorizontal: SIZES.padding * 0.8,
                                justifyContent: 'center'
                            }}
                        >
                            <Image
                                source={icons.video}
                                resizeMode="contain"
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                            />
                        </TouchableOpacity>
                        <Text style={{ ...FONTS.h4 }}>{getTimeString(item.startTime)} - {getTimeString(item.endTime)}</Text>
                    </TouchableOpacity>

                    <View
                        style={{
                            position: 'absolute',
                            bottom: 0,
                            height: 50,
                            width: SIZES.width * 0.3,
                            backgroundColor: COLORS.white,
                            borderTopRightRadius: SIZES.radius,
                            borderBottomLeftRadius: SIZES.radius,
                            alignItems: 'center',
                            justifyContent: 'center',
                            ...styles.shadow
                        }}
                    >

                        <Text style={{ ...FONTS.h4 }}>{getDate(item.meetingDate)} {getMonthName(item.meetingDate)}</Text>
                    </View>

                    <View
                        style={item.askForConfrimation ? {
                            position: 'absolute',
                            right: 0,
                            top: 0,
                            left: 0,
                            bottom: 0,
                            backgroundColor: COLORS.black,
                            color: COLORS.white,
                            borderTopRightRadius: SIZES.radius,
                            borderBottomLeftRadius: SIZES.radius,
                            alignItems: 'center',
                            justifyContent: 'center',
                            ...styles.shadow
                        } : {
                                position: 'absolute',
                                right: 0,
                                top: 0,
                                height: 50,
                                width: SIZES.width * 0.12,
                                backgroundColor: COLORS.white,
                                borderTopRightRadius: SIZES.radius,
                                borderBottomLeftRadius: SIZES.radius,
                                alignItems: 'center',
                                justifyContent: 'center',
                                ...styles.shadow
                            }}
                    >
                        {

                            item.askForConfrimation ?
                                <View>
                                    <View style={{ flexDirection: 'row', alignSelf: 'flex-start' }}>
                                        <TouchableOpacity
                                            style={{
                                                width: 40,
                                                paddingLeft: SIZES.padding,
                                                justifyContent: 'center'
                                            }}
                                            onPress={() => askForConfrimation(item)}
                                        >
                                            <Image
                                                source={icons.back}
                                                resizeMode="contain"
                                                style={{
                                                    width: 22,
                                                    height: 22,
                                                    tintColor: COLORS.white
                                                }}
                                            />
                                        </TouchableOpacity>
                                        <Text style={{ ...FONTS.body3, color: COLORS.white }}>Cancel Meeting</Text>
                                    </View>
                                    <View style={{ marginHorizontal: 15 }}>
                                        <Text style={{ ...FONTS.body4, color: COLORS.primary }}>
                                            {item.title}
                                        </Text>
                                        <Text style={{ ...FONTS.body5, color: COLORS.white }}>
                                            Cancelled meeting cannot be  recoverd.
                                    </Text>
                                        <Text style={{ ...FONTS.body3, color: COLORS.white }}>
                                            Are you sure ?
                                    </Text>
                                    </View>


                                    <Button gradient onPress={() => {
                                        deleteMeeting(item, authState.token);
                                    }}>
                                        {loading ? (
                                            <ActivityIndicator size="small" color="white" />
                                        ) : (
                                                <Text bold white center>
                                                    Delete Meeting
                                                </Text>
                                            )}
                                    </Button>


                                </View>
                                :
                                <TouchableOpacity onPress={() => {
                                    askForConfrimation(item);
                                }}>

                                    <Image
                                        source={icons.close}
                                        resizeMode="contain"
                                        style={{
                                            width: 18,
                                            height: 18
                                        }}
                                    />
                                </TouchableOpacity>


                        }

                    </View>




                </View>

                {/* Meeting Info */}

                <Text style={{ ...FONTS.body2 }}>{item.title}</Text>

                <View
                    style={{
                        marginTop: SIZES.padding,
                        flexDirection: 'row'
                    }}
                >
                </View>
            </TouchableOpacity>
        )

        return (
            <FlatList
                data={meetings}
                keyExtractor={item => `${item.id}`}
                renderItem={renderItem}
                contentContainerStyle={{
                    paddingHorizontal: SIZES.padding * 2,
                    paddingBottom: 30
                }}
            />
        )
    }

    const renderNoMeetingFound = () => {
        return (
            <TouchableOpacity onPress={() => {
                navigation.navigate('InstantMeeting');
            }} style={{
                flexDirection: 'column', paddingHorizontal: SIZES.padding * 2,
                paddingBottom: 30, justifyContent: 'center', alignItems: 'center'
            }}>

                <Image
                    source={icons.fast}
                    resizeMode="contain"
                    style={{
                        width: 60,
                        height: 60
                    }}
                />
                <Text style={{ ...FONTS.body4 }}> No meeting scheduled.</Text>
                <Text style={{ ...FONTS.body4 }}> Click here to organize.</Text>
            </TouchableOpacity>
        )
    }



    return (
        <SafeAreaView style={styles.container}>
            {renderHeader()}
            {renderMainCategories()}
            {meetings && meetings.length ? renderMeetingList() : renderNoMeetingFound()}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.lightGray4
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3,
        elevation: 1,
    }
})

export default Home;