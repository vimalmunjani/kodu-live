import Home from './Home';
import Splash from './Splash';
import Welcome from './Welcome';
import Login from './Login';
import Register from './Register';
import ForgotPassword from './ForgotPassword';
import Profile from './Profile';
import Live from './Live';
import Join from './Join';
import InstantMeeting from './InstantMeeting';

export {
    Splash,
    Welcome,
    Login,
    Register,
    ForgotPassword,
    Profile,
    Home,
    Live,
    Join,
    InstantMeeting
}