import React, { useContext, useState, useEffect } from "react";
import {
    ActivityIndicator,
    KeyboardAvoidingView,
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    FlatList,
    ScrollView
} from "react-native";

import { Button, Block, Input, Text } from "../components";
import { COLORS, icons, SIZES, FONTS, images } from "../constants";

import { Context as AuthContext } from '../context/AuthContext';
import { Context as MeetingContext } from '../context/MeetingContext';


const Join = ({ navigation }) => {

    const { state: authState } = useContext(AuthContext);
    let { state, joinMeeting, setMeetingByMeetingCode, askForConfrimation, deleteMeeting } = useContext(MeetingContext);
    let { meeting } = state;
    let meetingCode = meeting && meeting.meetingCode;
    let [meetingId, setMeetingId] = useState(meetingCode || '');
    let [guestEmails, setGuestEmails] = useState([]);
    let { loading, errorMessage: error } = state;

    useEffect(() => {

        const unsubscribeFocus = navigation.addListener('focus', () => {
            if (!authState.token) {
                navigation.navigate('Welcome')
            }



        });

        return () => {
            unsubscribeFocus();
        };
    }, [navigation]);

    const getTimeString = (ms) => {
        const time = new Date(parseInt(ms, 10));
        let hours = time.getHours();
        let minutes = time.getMinutes();

        // Check whether AM or PM 
        const newformat = hours >= 12 ? 'PM' : 'AM';

        // Find current hour in AM-PM Format 
        hours = hours % 12;

        // To display "0" as "12" 
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        return `${hours}:${minutes} ${newformat}`;
    }
    const getMonthName = (dateString) => {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return monthNames[new Date(dateString).getMonth()];
    }

    const getDate = (date) => {
        const dateCount = new Date(date).getDate();
        return dateCount >= 10 ? dateCount : `0${dateCount}`;
    }

    const renderHeader = () => {
        return (
            <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingLeft: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}

                    onPress={() => navigation.navigate('Home')}
                >
                    <Image
                        source={icons.back}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                </TouchableOpacity>

                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View
                        style={{
                            width: '70%',
                            height: "100%",
                            backgroundColor: COLORS.lightGray3,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: SIZES.radius
                        }}
                    >
                        <Text style={{ ...FONTS.h3 }}>Join</Text>
                    </View>
                </View>

                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingRight: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}
                >

                </TouchableOpacity>
            </View>
        )
    }

    const renderMeeting = () => {
        const meetingItem = () => (
            <TouchableOpacity

                onPress={() => { }}
            >
                <Text style={{ ...FONTS.body2 }}>{meeting.title}</Text>
                {/* Image */}
                <View
                    style={{
                        marginBottom: SIZES.padding
                    }}
                >
                    <Image
                        source={images.meetingBg}
                        resizeMode="cover"
                        style={{
                            width: "100%",
                            height: 200,
                            borderRadius: SIZES.radius
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => { }}
                        style={{
                            position: 'absolute',
                            height: 50,
                            top: 55,
                            width: SIZES.width * 0.7,
                            backgroundColor: COLORS.white,
                            borderRadius: SIZES.radius,
                            alignItems: 'center',
                            justifyContent: 'center',
                            ...styles.shadow,
                            alignSelf: 'center',
                            flexDirection: 'row'
                        }}
                    >

                        <TouchableOpacity
                            style={{
                                width: 30,
                                marginHorizontal: SIZES.padding * 0.8,
                                justifyContent: 'center'
                            }}
                        >
                            <Image
                                source={icons.edit}
                                resizeMode="contain"
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                            />
                        </TouchableOpacity>
                        <Text style={{ ...FONTS.h4 }}>{getTimeString(meeting.startTime)} - {getTimeString(meeting.endTime)}</Text>
                    </TouchableOpacity>

                    <View
                        style={{
                            position: 'absolute',
                            bottom: 0,
                            height: 50,
                            width: SIZES.width * 0.3,
                            backgroundColor: COLORS.white,
                            borderTopRightRadius: SIZES.radius,
                            borderBottomLeftRadius: SIZES.radius,
                            alignItems: 'center',
                            justifyContent: 'center',
                            ...styles.shadow
                        }}
                    >

                        <Text style={{ ...FONTS.h4 }}>{getDate(meeting.meetingDate)} {getMonthName(meeting.meetingDate)}</Text>
                    </View>

                    <View
                        style={meeting.askForConfrimation ? {
                            position: 'absolute',
                            right: 0,
                            top: 0,
                            left: 0,
                            bottom: 0,
                            backgroundColor: COLORS.black,
                            color: COLORS.white,
                            borderTopRightRadius: SIZES.radius,
                            borderBottomLeftRadius: SIZES.radius,
                            alignItems: 'center',
                            justifyContent: 'center',
                            ...styles.shadow
                        } : {
                                position: 'absolute',
                                right: 0,
                                top: 0,
                                height: 50,
                                width: SIZES.width * 0.12,
                                backgroundColor: COLORS.white,
                                borderTopRightRadius: SIZES.radius,
                                borderBottomLeftRadius: SIZES.radius,
                                alignItems: 'center',
                                justifyContent: 'center',
                                ...styles.shadow
                            }}
                    >
                        {

                            meeting.askForConfrimation ?
                                <View>
                                    <View style={{ flexDirection: 'row', alignSelf: 'flex-start' }}>
                                        <TouchableOpacity
                                            style={{
                                                width: 40,
                                                paddingLeft: SIZES.padding,
                                                justifyContent: 'center'
                                            }}
                                            onPress={() => askForConfrimation(meeting)}
                                        >
                                            <Image
                                                source={icons.back}
                                                resizeMode="contain"
                                                style={{
                                                    width: 22,
                                                    height: 22,
                                                    tintColor: COLORS.white
                                                }}
                                            />
                                        </TouchableOpacity>
                                        <Text style={{ ...FONTS.body3, color: COLORS.white }}>Cancel Meeting</Text>
                                    </View>
                                    <View style={{ marginHorizontal: 15 }}>
                                        <Text style={{ ...FONTS.body4, color: COLORS.primary }}>
                                            {meeting.title}
                                        </Text>
                                        <Text style={{ ...FONTS.body5, color: COLORS.white }}>
                                            Cancelled meeting cannot be  recoverd.
                                    </Text>
                                        <Text style={{ ...FONTS.body3, color: COLORS.white }}>
                                            Are you sure ?
                                    </Text>
                                    </View>


                                    <Button gradient onPress={() => {
                                        deleteMeeting(meeting, authState.token);
                                    }}>
                                        {loading ? (
                                            <ActivityIndicator size="small" color="white" />
                                        ) : (
                                                <Text bold white center>
                                                    Delete Meeting
                                                </Text>
                                            )}
                                    </Button>


                                </View>
                                :
                                <TouchableOpacity onPress={() => {
                                    askForConfrimation(meeting);
                                }}>

                                    <Image
                                        source={icons.close}
                                        resizeMode="contain"
                                        style={{
                                            width: 18,
                                            height: 18
                                        }}
                                    />
                                </TouchableOpacity>


                        }

                    </View>




                </View>

                {/* Meeting Info */}



                <View
                    style={{
                        marginTop: SIZES.padding,
                        flexDirection: 'row'
                    }}
                >
                </View>
            </TouchableOpacity>
        )

        return (
            <View style={styles.meeting}>
                {meeting ? meetingItem() : null}
            </View>
        )
    }

    const renderGuestList = () => {

        return (
            <FlatList
                style={{ marginBottom: 60 }}
                data={guestEmails}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.guestListStyle}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    style={{ marginHorizontal: 10 }}
                                    source={images.avatar_4}
                                    resizeMode="contain"
                                    style={{
                                        width: 30,
                                        height: 30,
                                        marginHorizontal: 10
                                    }}
                                />
                                <Text>{item.email}</Text>
                            </View>
                            <TouchableOpacity onPress={() => {
                                const filteredGuestEmails = guestEmails.filter((guest) => guest.email !== item.email);
                                setGuestEmails(filteredGuestEmails);
                            }}>
                                <Image
                                    style={{ alignSelf: 'flex-end' }}
                                    source={icons.close}
                                    resizeMode="contain"
                                    style={{
                                        width: 22,
                                        height: 22
                                    }}
                                />
                            </TouchableOpacity>
                        </View>

                    )
                }}
                keyExtractor={(item) => item.email}
            />
        )
    }


    return (
        <View style={styles.join}>
            {renderHeader()}
            <Block padding={[0, SIZES.base * 2]}>

                {state.errorMessage ? <Text style={styles.errorMessageStyle}>{state.errorMessage}</Text> : null}
                <Block middle >
                    <KeyboardAvoidingView>
                        <Input
                            label="Meeting Code"
                            style={[styles.input]}
                            defaultValue={meeting && meeting.meetingCode}
                            onChangeText={(meetingId) => {
                                setMeetingId(meetingId);
                                if (meetingId.length === 36) {
                                    setMeetingByMeetingCode({ meetingCode: meetingId }, authState.token);
                                }
                            }}
                        />

                        <Button gradient onPress={() => joinMeeting({ meetingCode: meetingId }, authState.token)}>
                            {loading ? (
                                <ActivityIndicator size="small" color="white" />
                            ) : (
                                    <Text bold white center>
                                        Join Meeting
                                    </Text>
                                )}
                        </Button>
                    </KeyboardAvoidingView>
                </Block>
            </Block>
            <Block padding={[0, SIZES.base * 2]}>
                {renderMeeting()}
                {renderGuestList()}
            </Block>
        </View>


    );
}

const styles = StyleSheet.create({
    join: {
        flex: 1,
    },
    input: {
        borderRadius: 0,
        borderWidth: 0,
        borderBottomColor: COLORS.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    hasErrors: {
        borderBottomColor: COLORS.red
    },
    guestListStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#fafafa'
    },
});

export default Join;