import React, { useContext, useState } from "react";
import {
    ActivityIndicator,
    Keyboard,
    KeyboardAvoidingView,
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    FlatList
} from "react-native";

import DateTimePicker from '@react-native-community/datetimepicker';
import { Button, Block, Input, Text } from "../components";
import { COLORS, icons, SIZES, FONTS, images } from "../constants";

import { Context as MeetingContext } from '../context/MeetingContext';
import { Context as AuthContext } from '../context/AuthContext';



const InstantMeeting = ({ navigation }) => {

    const { state: authState } = useContext(AuthContext);
    const { state, startMeeting } = useContext(MeetingContext);

    const [title, setTitle] = useState('');

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const [showStartDateInAndoridMode, setShowStartDateInAndoridMode] = useState(false);
    const [showEndDateInAndoridMode, setShowEndDateInAndoridMode] = useState(false);
    const [startPickerMode, setStartPickerMode] = useState('date');
    const [endPickerMode, setEndPickerMode] = useState('date');

    const [startTime, setStartTime] = useState(new Date());
    const [endTime, setEndTime] = useState(new Date());

    const [guestEmail, setGuestEmail] = useState('');
    const [guestEmails, setGuestEmails] = useState([]);

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    const handleSubmit = () => {
        setError('');
        if (!title) {
            alert('Please enter Meeting Title');
            return;
        }
        setLoading(true);
        const payload = {
            title, startDate, startTime, endDate, endTime, guestEmails
        };
        startMeeting(payload, authState.token);
    };

    const [show, setShow] = useState(false);

    const showStartDatepicker = () => {
        setShowStartDateInAndoridMode(true);
        setStartPickerMode('date');
    };

    const showStartTimepicker = () => {
        setShowStartDateInAndoridMode(true);
        setStartPickerMode('time');
    };

    const showEndDatepicker = () => {
        setShowEndDateInAndoridMode(true);
        setEndPickerMode('date');
    };

    const showEndTimepicker = () => {
        setShowEndDateInAndoridMode(true);
        setEndPickerMode('time');
    };



    const formatDate = (date, time) => {
        return `${date.getDate()}/${date.getMonth() +
            1}/${date.getFullYear()} ${time.getHours()}:${time.getMinutes()}`;
    };

    const onStartDateChange = (event, selectedValue) => {
        setShowStartDateInAndoridMode(Platform.OS === 'ios');
        if (startPickerMode == 'date') {
            const currentDate = selectedValue || new Date();
            setStartDate(currentDate);
            setStartPickerMode('time');
            setShowStartDateInAndoridMode(true); // to show the picker again in time mode
        } else {
            const selectedTime = selectedValue || new Date();
            setStartTime(selectedTime);
            setShowStartDateInAndoridMode(false);
            setStartPickerMode('date');
        }
    };

    const onEndDateChange = (event, selectedValue) => {
        setShowEndDateInAndoridMode(Platform.OS === 'ios');
        if (endPickerMode == 'date') {
            const currentDate = selectedValue || new Date();
            setEndDate(currentDate);
            setEndPickerMode('time');
            setShowEndDateInAndoridMode(true); // to show the picker again in time mode
        } else {
            const selectedTime = selectedValue || new Date();
            setEndTime(selectedTime);
            setShowEndDateInAndoridMode(false);
            setEndPickerMode('date');
        }
    };




    const renderHeader = () => {
        return (
            <View style={{ flexDirection: 'row', height: 50, marginTop: 10, marginBottom: 30 }}>
                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingLeft: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}

                    onPress={() => navigation.navigate('Home')}
                >
                    <Image
                        source={icons.back}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                </TouchableOpacity>

                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View
                        style={{
                            width: '70%',
                            height: "100%",
                            backgroundColor: COLORS.lightGray3,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: SIZES.radius
                        }}
                    >
                        <Text style={{ ...FONTS.h3 }}>Instant Meet</Text>
                    </View>
                </View>

                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingRight: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}
                >

                </TouchableOpacity>
            </View>
        )
    }


    return (
        <KeyboardAvoidingView style={styles.instantMeeting}>
            <Block padding={[0, SIZES.base * 2]}>
                {renderHeader()}
                {state.errorMessage ? <Text style={styles.errorMessageStyle}>{state.errorMessage}</Text> : null}
                <Block middle>
                    <Input
                        label="Meeting Title"
                        style={[styles.input]}
                        defaultValue={title}
                        onChangeText={(title) => setTitle(title)}
                    />
                    {/* Android date/time Control */}
                    {showStartDateInAndoridMode && (<DateTimePicker
                        style={styles.input}
                        value={startDate}
                        mode={startPickerMode}
                        is24Hour={true}
                        display="default"
                        onChange={onStartDateChange}
                    />)}

                    {/*   Android date/time label */}
                    {!showStartDateInAndoridMode && (
                        <TouchableOpacity style={styles.pseudoInput} onPress={() => showStartDatepicker()}>
                            <Text style={styles.link}> Start Date/Time: {formatDate(startDate, startTime)}</Text>
                            <Image
                                source={icons.calendar}
                                resizeMode="contain"
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                            />
                        </TouchableOpacity>
                    )}

                    {/* Android date/time Control */}
                    {showEndDateInAndoridMode && (<DateTimePicker
                        style={styles.input}
                        value={endDate}
                        mode={endPickerMode}
                        is24Hour={true}
                        display="default"
                        onChange={onEndDateChange}
                    />)}

                    {/*   Android date/time label */}
                    {!showEndDateInAndoridMode && (
                        <TouchableOpacity style={styles.pseudoInput} onPress={() => showEndDatepicker()}>
                            <Text style={styles.link}> End Date/Time: {formatDate(endDate, endTime)}</Text>
                            <Image
                                source={icons.calendar}
                                resizeMode="contain"
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                            />
                        </TouchableOpacity>
                    )}

                    {/* Guest Email Control */}

                    <Input
                        defaultValue={guestEmail}
                        style={[styles.input]}
                        placeholder='Enter guest email'
                        onChangeText={(guestEmail) => setGuestEmail(guestEmail)}
                        rightLabel={(
                            <Image
                                source={icons.plus}
                                resizeMode="contain"
                                style={{
                                    width: 22,
                                    height: 22
                                }}
                            />
                        )}
                        rightStyle={{
                            position: 'absolute',
                            top: 15
                        }}
                        onRightPress={() => {
                            if (guestEmail && !guestEmails.includes(guestEmail.toLowerCase())) {
                                setGuestEmails([...guestEmails, {
                                    email: guestEmail.toLowerCase()
                                }]);
                                setGuestEmail('');
                            }

                        }}
                    />


                    <FlatList
                        data={guestEmails}
                        renderItem={({ item }) => {
                            return (
                                <View style={styles.guestListStyle}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image
                                            style={{ marginHorizontal: 10 }}
                                            source={images.avatar_4}
                                            resizeMode="contain"
                                            style={{
                                                width: 30,
                                                height: 30,
                                                marginHorizontal: 10
                                            }}
                                        />
                                        <Text>{item.email}</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => {
                                        const filteredGuestEmails = guestEmails.filter((guest) => guest.email !== item.email);
                                        setGuestEmails(filteredGuestEmails);
                                    }}>
                                        <Image
                                            style={{ alignSelf: 'flex-end' }}
                                            source={icons.close}
                                            resizeMode="contain"
                                            style={{
                                                width: 22,
                                                height: 22
                                            }}
                                        />
                                    </TouchableOpacity>
                                </View>

                            )
                        }}
                        keyExtractor={(item) => item.email}
                    />

                    <Button gradient onPress={handleSubmit}>
                        {loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                                <Text bold white center>
                                    Start Instant Meeting
                                </Text>
                            )}
                    </Button>
                </Block>
            </Block>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    instantMeeting: {
        flex: 1,
        marginBottom: 75
    },
    pseudoInput: {
        flexDirection: 'row',
        marginVertical: 15,
        backgroundColor: '#ffffff',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    addGuestStyle: {
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    input: {
        borderRadius: 0,
        borderWidth: 0,
        borderBottomColor: COLORS.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    hasErrors: {
        borderBottomColor: COLORS.primary
    },
    guestListStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        backgroundColor: '#fafafa'
    },
});

export default InstantMeeting;