import React, { useEffect, useState, useContext } from "react";
import {
    SafeAreaView,
    View,
    StyleSheet,
    Image,
    Animated,
    TouchableOpacity
} from "react-native";

import { SIZES, icons, images, COLORS, FONTS } from '../constants';
import { Text } from '../components';

import { Context as AuthContext } from '../context/AuthContext';
import { Context as ProfileContext } from '../context/ProfileContext';

const Profile = ({ navigation }) => {
    const { state: authState, clearErrorMessage, signOut } = useContext(AuthContext);
    const { state, getProfile } = useContext(ProfileContext);
    const { profile, loading } = state;


    useEffect(() => {
        if (!authState.token) {
            navigation.navigate('Welcome')
        }
        getProfile(authState.token);
    }, []);

    useEffect(() => {
        const unsubscribeBlur = navigation.addListener('blur', () => {
            clearErrorMessage();
        });
        const unsubscribeFocus = navigation.addListener('focus', () => {
            clearErrorMessage();
            getProfile(authState.token);
        });
        const unsubscribe = () => {
            unsubscribeBlur();
            unsubscribeFocus();
        };
        return unsubscribe;
    }, [navigation]);

    const renderProfile = () => {
        return (
            <>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 30 }}>
                    <TouchableOpacity
                        style={{
                            width: 50,
                            paddingLeft: SIZES.padding * 2,
                            justifyContent: 'center'
                        }}
                    >
                        <Image
                            source={icons.user}
                            resizeMode="contain"
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                    </TouchableOpacity>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View
                            style={{
                                width: '90%',
                                height: "100%",
                                backgroundColor: COLORS.transparent,
                                justifyContent: 'center',
                                borderRadius: SIZES.radius * 0.2,
                                padding: 10
                            }}
                        >
                            <Text style={{ ...FONTS.h4 }}>{profile?.name}</Text>
                        </View>
                    </View>


                </View>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 30 }}>
                    <TouchableOpacity
                        style={{
                            width: 50,
                            paddingLeft: SIZES.padding * 2,
                            justifyContent: 'center'
                        }}
                    >
                        <Image
                            source={icons.envelope}
                            resizeMode="contain"
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                    </TouchableOpacity>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View
                            style={{
                                width: '90%',
                                height: "100%",
                                backgroundColor: COLORS.transparent,
                                justifyContent: 'center',
                                borderRadius: SIZES.radius * 0.2,
                                padding: 10
                            }}
                        >
                            <Text style={{ ...FONTS.body3 }}>Email</Text>
                            <Text style={{ ...FONTS.body4 }}>{profile?.email}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 30 }}>
                    <TouchableOpacity
                        style={{
                            width: 50,
                            paddingLeft: SIZES.padding * 2,
                            justifyContent: 'center'
                        }}
                    >
                        <Image
                            source={icons.phone}
                            resizeMode="contain"
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                    </TouchableOpacity>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View
                            style={{
                                width: '90%',
                                height: "100%",
                                backgroundColor: COLORS.transparent,
                                justifyContent: 'center',
                                borderRadius: SIZES.radius * 0.2,
                                padding: 10
                            }}
                        >
                            <Text style={{ ...FONTS.body3 }}>Contact</Text>
                            <Text style={{ ...FONTS.body4 }}>{profile?.contact}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 30 }}>
                    <TouchableOpacity
                        style={{
                            width: 50,
                            paddingLeft: SIZES.padding * 2,
                            justifyContent: 'center'
                        }}
                    >
                        <Image
                            source={icons.plan}
                            resizeMode="contain"
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                    </TouchableOpacity>

                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <View
                            style={{
                                width: '90%',
                                height: "100%",
                                backgroundColor: COLORS.transparent,
                                justifyContent: 'center',
                                borderRadius: SIZES.radius * 0.2,
                                padding: 10
                            }}
                        >
                            <Text style={{ ...FONTS.body3 }}>Subscription</Text>
                            <Text style={{ ...FONTS.body4 }}>{profile?.subscription?.planType}</Text>
                        </View>
                    </View>


                </View>
                <View style={{ flexDirection: 'row', height: 50, marginTop: 30, position: 'absolute', bottom: 100, width: '100%' }}>
                    <TouchableOpacity
                        style={{
                            width: 50,
                            paddingLeft: SIZES.padding * 2,
                            justifyContent: 'center'
                        }}
                    >
                        <Image
                            source={icons.shutdown}
                            resizeMode="contain"
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                        onPress={signOut}
                    >
                        <View
                            style={{
                                width: '90%',
                                height: "100%",
                                backgroundColor: COLORS.black,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: SIZES.radius * 0.2,
                                padding: 10
                            }}
                        >
                            <Text style={{ ...FONTS.h4 }} white >Logout</Text>
                        </View>
                    </TouchableOpacity>
                </View>


            </>
        )
    }

    const renderHeader = () => {
        return (
            <View style={{ flexDirection: 'row', height: 50, marginTop: 30 }}>
                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingLeft: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}
                    onPress={() => navigation.navigate('Home')}
                >
                    <Image
                        source={icons.back}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                </TouchableOpacity>

                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View
                        style={{
                            width: '70%',
                            height: "100%",
                            backgroundColor: COLORS.lightGray3,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: SIZES.radius
                        }}
                    >
                        <Text style={{ ...FONTS.h3 }}>Profile</Text>
                    </View>
                </View>

                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingRight: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}
                >
                    <Image
                        source={icons.edit}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />

                </TouchableOpacity>
            </View>
        )
    }



    return (
        <SafeAreaView style={styles.safeContainer}>
            {renderHeader()}
            {renderProfile()}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    safeContainer: {
        flex: 1,
        backgroundColor: COLORS.lightGray4
    },
    container: {
        flex: 1,
        backgroundColor: COLORS.lightGray4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3,
        elevation: 1,
    }
})

export default Profile;