import React, { useContext, useState, useEffect } from "react";
import {
    ActivityIndicator,
    Keyboard,
    KeyboardAvoidingView,
    StyleSheet,
    View,
    TouchableOpacity,
    Image
} from "react-native";

import { Button, Block, Input, Text } from "../components";
import { COLORS, icons, SIZES, FONTS } from "../constants";

import { Context as AuthContext } from '../context/AuthContext';



const ForgotPassword = ({ navigation }) => {

    const { state, resetPassword, clearErrorMessage } = useContext(AuthContext);
    const [email, setEmail] = useState('');
    const { loading, errorMessage, infoMessage } = state;

    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            clearErrorMessage();
        });

        return unsubscribe;
    }, [navigation]);

    const renderHeader = () => {
        return (
            <View style={{ flexDirection: 'row', height: 50, marginTop: 10 }}>
                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingLeft: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}

                    onPress={() => navigation.navigate('Login')}
                >
                    <Image
                        source={icons.back}
                        resizeMode="contain"
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                </TouchableOpacity>

                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View
                        style={{
                            width: '70%',
                            height: "100%",
                            backgroundColor: COLORS.lightGray3,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: SIZES.radius
                        }}
                    >
                        <Text style={{ ...FONTS.h3 }}>Forgot Password</Text>
                    </View>
                </View>

                <TouchableOpacity
                    style={{
                        width: 50,
                        paddingRight: SIZES.padding * 2,
                        justifyContent: 'center'
                    }}
                >

                </TouchableOpacity>
            </View>
        )
    }


    return (
        <KeyboardAvoidingView style={styles.passwordContainer}>
            <Block padding={[0, SIZES.base * 2]}>
                {renderHeader()}
                {errorMessage ? <Text style={styles.errorMessageStyle}>{errorMessage}</Text> : null}
                {infoMessage ? <Text style={{ ...styles.errorMessageStyle, backgroundColor: COLORS.green }}>{infoMessage}</Text> : null}
                <Block middle>
                    <Input
                        label="Email"
                        style={[styles.input]}
                        defaultValue={email}
                        onChangeText={(email) => setEmail(email)}
                    />

                    <Button gradient onPress={() => { resetPassword(email) }}>
                        {loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                                <Text bold white center>
                                    Reset Password
                                </Text>
                            )}
                    </Button>
                </Block>
            </Block>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    passwordContainer: {
        flex: 1,
    },
    input: {
        borderRadius: 0,
        borderWidth: 0,
        borderBottomColor: COLORS.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    hasErrors: {
        borderBottomColor: COLORS.red
    },
    errorMessageStyle: {
        backgroundColor: COLORS.red,
        color: COLORS.white,
        padding: SIZES.padding * 0.5,
        paddingHorizontal: SIZES.padding,
        marginVertical: 30,
        borderRadius: SIZES.radius * 0.2
    }
});

export default ForgotPassword;